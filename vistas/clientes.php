<?php 
  
  require '../archivo/cliente.php';
  require '../controlador/funciones.php';
  if(! haIniciadoSesion() )
  {
   header('Location: ../index.php');
  }
if ($_SESSION['usuario']=='admi' or $_SESSION['usuario']=='gerente' or $_SESSION['usuario']=='vanessa' or $_SESSION['usuario']=='alessandra' or $_SESSION['usuario']=='gianella' or $_SESSION['usuario']=='carmen' or $_SESSION['usuario']=='joe' or $_SESSION['usuario']=='karen' or $_SESSION['gianella']) {
include('header.php'); 
$conexion = new Conexion();
$cn = $conexion->getConexion();
$estado = 100;
if (isset( $_GET['estado'])) {
	$estado = $_GET['estado'];
}								
?>
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
    <div class="col-lg-13">
    	<div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>TABLA DE CLIENTES</h5> <span class="label label-primary">T-S|S</span>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
        	</div>
        </div>
        <div class="col-lg-13">
          <ol class="breadcrumb">
            <li>
                &nbsp &nbsp &nbsp &nbsp Selección
            </li>
            <li>
              Requerimientos
            </li>
            <li class="active">
                <strong>Clientes</strong>
            </li>
          </ol>
        </div>
        <div class="ibox-content">
          <div>
            <table class="footable table table-stripped toggle-arrow-tiny table-hover">
              <thead>
                <tr>
    							<td class="text-center"><strong>RUC</strong></td>
    							<td class="text-center"><strong>RAZÓN SOCIAL</strong></td>
    							<td class="text-center"><strong>CONTACTO</strong></td>
    							<td class="text-center"><strong>CORREO</strong></td>
    							<td class="text-center"><strong>CELULAR</strong></td>
    							<td class="text-center"><strong>OPCIONES</strong></td>
                </tr>
              </thead>
                <?php
                $cliente = new cliente($cn);
                $datos = $cliente -> listarcliente(); 
                foreach ($datos as $cliente) {
                	$ruc = $cliente[0];
                	$razonSocial = $cliente[1];
                	$contacto = $cliente[2];
                	$correo = $cliente[4];
                	$celular = $cliente[3];
                	$eliminado = $cliente[6];
    						//$rs=ejecutarQuery("SELECT * FROM clientes where eliminado=0 order by razonSocial");
    						//while($row=mysqli_fetch_assoc($rs)){
    						?>
              <tbody>
                <tr>
                  <td class="text-center"><?php echo $ruc; ?></td>
    							<td class="text-center"><?php echo $razonSocial;?></td>
    							<td class="text-center"><?php echo $contacto; ?></td>
    							<td class="text-center"><?php echo $correo;?></td>
    							<td class="text-center"><?php echo $celular; ?></td>
    							<td class="text-center">
    								<a href="#edit<?php echo $ruc;?>" data-toggle="modal"><button type='button' class='btn btn-warning btn-sm' title="EDITAR"><span class='glyphicon glyphicon-edit' aria-hidden='true'></span></button></a> 
    								<a href="#delete<?php echo $ruc;?>" data-toggle="modal"><button type='button' class='btn btn-danger btn-sm' title="ELIMINAR"><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></button></a>
    							</td>
                </tr>
              </tbody>
          		<!--MODAL DE EDITAR-->
              <div id="edit<?php echo $ruc; ?>" class="modal fade" role="dialog">
    						<form class="form-signin" action="../controlador/guardarCliente.php?ruc=<?php echo $ruc;?>" 
    								method="POST" enctype="multipart/form-data">	
      							<div class="modal-dialog" >
      								<div class="modal-content">
      									<div class="modal-header">
      										<button type="button" class="close" data-dismiss="modal">&times;</button>
      											<h4 class="modal-title">EDITAR CLIENTE</h4>
      									</div>
      									<div class="modal-body">
      											<?php 
      											$rs=ejecutarQuery("SELECT * FROM clientes where ruc=$ruc");
      											$row=mysqli_fetch_assoc($rs)
      											?>
      										<div class="row">
      							        <div class="form-group col-md-2">																				
      												<label for="txtcodigo" class="col-2 col-form-label">RUC:</label>													
      											</div>
      											<div class="col-md-4">
      												<div class="form-group">																		    
      													<input type="text" name='txtruc' id='txtruc' class='form-control' value="<?php echo $ruc; ?>" readonly="readonly" >
      												</div>
      											</div>
      								  	</div>
      										<div class="row">
      											<div class="col-md-12">
      												<div class="form-group">
      													<label>RAZÓN SOCIAL:</label>
      													<input type="text" name='txtrazonsocial' id='txtrazonsocial' class='form-control' value="<?php echo $razonSocial;?>" required>	
      												</div>
      											</div>					
      										</div>
      										<div class="row">
      											<div class="col-md-6">
      												<div class="form-group">
      													<label>CONTACTO:</label>
      													<input type="text" name='contacto' class='form-control' value="<?php echo $row['contacto']; ?>" >
      												</div>						
      											</div>
      						         	<div class="col-md-6">
      												<div class="form-group">
      													<label>CORREO:</label>
      														<input type="text" name='correo' class='form-control' value="<?php echo $row['correo']; ?>" >
      												</div>
      											</div>					
      										</div>
      										<div class="row">
      											<div class="col-md-6">
      												<div class="form-group">
      													<label>CELULAR</label>
      													<input type="number" name='celular' class='form-control' value="<?php echo $row['celular']; ?>" >
      												</div>						
      											</div>				
      										</div>
      									</div>	
      									<div class="modal-footer"> 
      										<button type="submit" class="btn btn-primary" name='btnAgregar'>Actualizar</button>
      										<button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
      									</div>
      								</div>							
      							</div>
    						</form>
    					</div>
    					<!-- Eliminar Modal -->
          		<div id="delete<?php echo $ruc; ?>" class="modal fade" role="dialog">
              	<div class="modal-dialog">
                  <form method="post" action="../controlador/eliminarCliente.php?ruc=<?php echo $ruc;?>">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">ELIMINAR CLIENTE</h4>
                      </div>
                  		<div class="modal-body">
                    		<input type="hidden" name="delete_id" value="<?php echo $ruc; ?>">
                    		<p>Esta seguro de Eliminar <strong><?php echo $razonSocial; ?>?</strong></p>
                      </div>
                    	<div class="modal-footer">
                      	<button type="submit" name="btnEliminar" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span>YES</button>
                      	<button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> NO</button>
                    	</div>
                  	</div>
                  </form>
                </div>
              </div>

    				 	<tfoot>	
    	          <?php
    						}
    						?>
    						<tr>
    	            <td colspan="6">
    	              <ul class="pagination pull-right"></ul>
    	            </td>
    	          </tr>
     					</tfoot>
              
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>REGISTRAR NUEVO CLIENTE</h5> <span class="label label-primary">T-S|S</span>
        <div class="ibox-tools">
          <a class="collapse-link">
            <i class="fa fa-chevron-up"></i>
          </a>
        </div>
    	</div>
      <div class="ibox-content">
        <div>
          <table class="footable table table-stripped toggle-arrow-tiny">
            <form class="form-signin" action="../controlador/nuevocliente.php" method="POST" enctype="multipart/form-data">
            	<thead>
              	<div class="row form-group">
               		<div class="col-md-3">
                   	<label for="cliente">RUC:</label>
                    	<input type="tel" class="form-control" rows="3" name="ruc" required>
               		</div>
                  <div class="col-md-3">
                    <label for="cliente">RAZÓN SOCIAL:</label>
                      <input type="text" class="form-control" rows="3" name="razonSocial" required>
                  </div>
                  <div class="col-md-3">
                    <label for="cliente">CONTACTO:</label>
                    <input type="text" class="form-control" rows="3" name="contacto">
                  </div>
                  
            		</div>
            		<div class="row form-group">
                  <div class="col-md-3">
                    <label for="cliente">CELULAR:</label>
                    <input type="number" class="form-control" rows="3" name="celular" >
                  </div>
                  <div class="col-md-3">
                    <label for="cliente">CORREO:</label>
                    <input type="email" class="form-control" rows="3" name="correo" >
                  </div>
                  <div class="col-md-3">
                  </div>
                  <div class="col-md-3">
                  	<br style="line-height: 23px">
                      <button class="btn btn-primary btn-block" type="submit">REGISTRAR</button>
                  </div>
                </div>
              </thead>
            </form>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>  
<?php include('footer.php'); }
else { ?> <script>
    alert("NO SE TE CONCEDIO PERMISO PARA ESTA VISTA");
    window.history.go(-1);
    </script> <?php } ?>