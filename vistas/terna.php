<?php 
	session_start();
	include('header.php'); 
	require '../controlador/funciones.php';
	require '../archivo/perfil.php';
	require '../archivo/cliente.php';
	require '../archivo/terna.php';
	require '../archivo/procesos.php';
$conexion = new Conexion();
$cn = $conexion->getConexion();
$estado = 100;
if (isset( $_GET['estado'])) {
	$estado = $_GET['estado'];
}	
$proceso = $_GET['codigo'];					
?>

	
    <div class="row ">
      <div class="col-lg-12">
      	<div class="ibox float-e-margins">
        	<div class="ibox-title">
          	<h5>DATOS DE PROCESO <?php echo $proceso; ?></h5> <span class="label label-primary">T-S|S</span>
            <div class="ibox-tools">
		          <a class="collapse-link">
		           	<i class="fa fa-chevron-up"></i>
		          </a>
            </div>
          </div>

          <div class="ibox-content">
                <?php
                $procesos = new procesos($cn);
                $datos = $procesos -> listarprocesocodigo($proceso); //en culminados le mando 0 
                foreach ($datos as $procesos) {
                	$codigo = $procesos[0];
                	$cliente = $procesos[12];
                	$cargo = $procesos[11];
                	$consultor = $procesos[10];
                	$cantidad = $procesos[2];
                	$ciudad = $procesos[3];
                	$fechapedido = $procesos[4];
                	$fechaentrega = $procesos[5];
                	$detalles = $procesos[6];
                	$estado = $procesos[7];
                	$eliminado = $procesos[8];
                	$grifo = $procesos[9];
							}?>
					<div class="wrapper wrapper-content animated fadeInRight">
	          <thead>
	          	<div class="row form-group">
	            	<div class="col-md-3">
	                  <label for="cliente">CARGO:</label>
	                  <input type="text" readonly class="form-control" rows="3" name="nombres"  value="<?php echo $cargo;?>">
	              </div>
	              <div class="col-md-3">
	                  <label for="cliente">CIUDAD:</label>
	                  <input type="text" class="form-control" rows="3"  readonly  value="<?php echo $ciudad;?>">
	              </div>
	              <div class="col-md-2">
	                  <label for="cliente">CANTIDAD:</label>
	                  <input type="text" class="form-control" rows="3" readonly value="<?php echo $cantidad;?>">
	              </div>
	              <div class="col-md-2">
	                  <label for="cliente">F. PEDIDO:</label>
	                  <input type="text" class="form-control" rows="3" readonly value="<?php echo date('d-m-Y', strtotime($fechapedido));?>">
	              </div>
	              <div class="col-md-2">
	                  <label for="cliente">F. ENTREGA:</label>
	                  <input type="text" class="form-control" rows="3" readonly value="<?php echo date('d-m-Y', strtotime($fechaentrega));?>">
	              </div>
	            </div>
	          </thead>
	       </div>

        </div>

                  </div>
              </div>
          </div>	
      	
        <div class="ibox float-e-margins">
          <div class="ibox-title">
            <h5>TABLA DE TERNA</h5> 
            <div class="ibox-tools">
            	<a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
              </a>
            </div>
          </div>
          <div class="col-lg-13">
          <ol class="breadcrumb">
            <li>
                &nbsp &nbsp &nbsp &nbsp Selección
            </li>
            <li>
              Procesos
            </li>
            <li class="active">
                <strong>Terna</strong>
            </li>
          </ol>
        </div>
          <div class="ibox-content">
          	<table class="footable table table-stripped toggle-arrow-tiny">
			        <thead>
			         	<tr >
									<td data-toggle="true" class="text-center"><strong>CÓDIGO</strong></td>
                  <td class="text-center"><strong>TERNA</strong></td>
                  <td class="text-center"><strong>OBSERVACIÓN</strong></td>
                  <th data-hide="all">DNI</th>
                  <th data-hide="all">Teléfono</th>
                  <th data-hide="all">Fecha Inicio</th>
                  <th data-hide="all">Fecha Ingreso</th>
                  <th data-hide="all">Entevista Virtual</th>
                  <th data-hide="all">Entrevista Personal</th>
                  <th data-hide="all">Verificación de Antecedentes</th>
                  <th data-hide="all">Verificación Laboral</th>
                  <th data-hide="all">Informe Psicológico</th>
                  <th data-hide="all">%</th>
                  <th data-hide="all">N° Orden de Servicio</th>
                  <th data-hide="all">N° Factura</th>
                  <td class="text-center"><strong>OPCIONES</strong></td>
                </tr>
            	</thead>
              <?php
                $postulante = new terna($cn);
                $datos = $postulante -> listarterna($proceso); 
                foreach ($datos as $postulante) {
                	$codigo = 	$postulante[0];
                	$proceso = 	$postulante[1];
                	$apellidos = 	$postulante[2];
                	$nombres = 	$postulante[3];
                	$rutaCV = 	$postulante[4];
                	$fechaEnvio = 	$postulante[5];
                	$rrhhCliente = 	$postulante[6];
                	$induccion = 	$postulante[7];
                	$entrevista = 	$postulante[8];
                	$informe = 	$postulante[9];
                	$fechaIngreso = 	$postulante[10];
                	$ordenServicio = 	$postulante[11];
                	$factura = 	$postulante[12];
                	$observacion = 	$postulante[13];
                	$eliminado = 	$postulante[14];
                	$terna = 	$postulante[15];
                	$dni = 	$postulante[16];
                	$telefono = 	$postulante[17];
                	$estacion = 	$postulante[18];
                	$porcentaje = 	$postulante[19];
                	$verLaboral = 	$postulante[20];
                	$elegido = 	$postulante[21];
                	
							?>
			        <tbody>
			        	<?php if ($elegido == 1) { ?>
			        		<tr style="background-color: #72bf91">
                  	<td class="text-center"><?php echo $codigo; ?></td>
										<td class="text-center"><?php echo $nombres; ?></td>
										<td class="text-center" style="max-width: 200px"><?php echo $observacion; ?></td>
										<td class="text-center"><?php echo $dni; ?></td>
										<td class="text-center"><?php echo $telefono; ?></td>
										<td class="text-center"><?php if ($fechaEnvio == '0000-00-00') {
																								echo 'FECHA NO REGISTRADA';
																					} else echo date('d-m-Y', strtotime($fechaEnvio));?></td> 
										<td class="text-center"><?php if ($fechaIngreso == '0000-00-00' OR $fechaIngreso === null) {
																								echo 'FECHA NO REGISTRADA';
																					} else echo date('d-m-Y', strtotime($fechaIngreso));?></td>
										<!--ENTREVISTA VIRTUAL -->												
										<td class="text-center">
											<?php if ($rrhhCliente=='0') {
													echo 'NN';
												} else{
														if ($rrhhCliente == '1') {
															echo 'APTO';
														} else echo 'NO APTO';
												} ?>
										</td>
										<!--ENTREVISTA PERSONAL -->
										<td class="text-center">
											<?php if ($induccion=='0') {
											echo 'NN';
											} else{
												if ($induccion == '1') {
													echo 'APTO';
												} else echo 'NO APTO';
											} ?>
										</td>
										<!--VERIFICACION DE ANTECEDENTES -->
										<td class="text-center">
											<?php if ($entrevista=='0') {
											echo 'NN';
											} else{
												if ($entrevista== '1') {
													echo 'APTO';
												} else echo 'NO APTO';
											} ?>
										</td>
										<!--VERIFICACION LABORAL -->
										<td class="text-center">
											<?php if ($verLaboral=='0') {
											echo 'NN';
										} else{
												if ($verLaboral == '1') {
													echo 'APTO';
												} else echo 'NO APTO';
										} ?>
										</td>
										<!--INFORME PSICOLÓGICO -->
										<td class="text-center">
											<?php if ($informe=='0') {
											echo 'NN';
										} else{
												if ($informe == '1') {
													echo 'APTO';
												} else echo 'NO APTO';
										} ?>
										</td>
										<td class="text-center">
												<?php 

													$count = 1;
													if ($rrhhCliente==1) {
														$count = $count +1;
													}
													if ($informe==1) {
														$count = $count +1;
													}
													if ($induccion==1) {
														$count = $count +1;
													}
													if ($entrevista==1) {
														$count = $count +1;
													}
													if ($fechaEnvio!=null) {
														$count = $count +1;
													}
													if ($fechaIngreso!=null) {
														$count = $count +1;
													}
													switch ($count) {
														case 1:
															echo "15%";
															break;
														case 2:
															echo "28%";
															break;
														case 3:
															echo "42%";
															break;
														case 4:
															echo "57%";
															break;
														case 5:
															echo "71%";
															break;
														case 6:
															echo "85%";
															break;
														case 7:
															echo "100%";
															break;
														default:
															echo "error en la matrix";
															break;
													}
													
												?>
											 	
										</td>
										<td class="text-center"><?php echo $ordenServicio; ?></td>
										<td class="text-center"><?php echo $factura; ?></td>
										<td class="text-center">
											<a href="">
												<button class="btn btn-primary btn-circle" style="background-color: #878787;" type="button" title="CV" onClick="window.open('<?php echo $rutaCV;?>');"><i class="fa fa-file"></i>
	                      </button>
											</a>
											<a href="#edit<?php echo $codigo;?>" data-toggle="modal">
												<button type='button' class='btn btn-warning btn-circle' title="EDITAR">
													<span class='glyphicon glyphicon-edit ' aria-hidden='true'></span>
												</button>
											</a>
											<?php if ($elegido == 1) { ?>
												<a href="../controlador/deseleccionar.php?codigo=<?php echo $codigo;?>">
												<button class="btn btn-primary btn-circle" type="button" title="DESELECCIONAR"><i class="fa fa-times"></i>
	                      </button>
												</a>
											<?php } else {	 ?>
												<a href="../controlador/seleccionar.php?codigo=<?php echo $codigo;?>">
												<button class="btn btn-primary btn-circle" type="button" title="SELECCIONAR"><i class="fa fa-check"></i>
	                      </button>
											</a>
											<?php } ?>
											<a href="#backup<?php echo $codigo;?>" data-toggle="modal">
												<button class="btn btn-primary btn-circle" style="background-color: #008CBA;" type="button" title="BACKUP"><i class="fa fa-cloud-upload"></i>
	                      </button>
											</a>
											<a href="#delete<?php echo $codigo;?>" data-toggle="modal">
												<button type='button' class='btn btn-danger btn-circle' title="ELIMINAR">
													<span class='glyphicon glyphicon-trash' aria-hidden='true'></span>
												</button>
											</a>
										</td>
	                </tr>
			        	<?php } else {?>
			        			<tr ">
                  	<td class="text-center"><?php echo $codigo; ?></td>
										<td class="text-center"><?php echo $nombres; ?></td>
										<td class="text-center" style="max-width: 200px"><?php echo $observacion; ?></td>
										<td class="text-center"><?php echo $dni; ?></td>
										<td class="text-center"><?php echo $telefono; ?></td>
										<td class="text-center"><?php if ($fechaEnvio == '0000-00-00') {
																								echo 'FECHA NO REGISTRADA';
																					} else echo date('d-m-Y', strtotime($fechaEnvio));?></td> 
										<td class="text-center"><?php if ($fechaIngreso == '0000-00-00') {
																								echo 'FECHA NO REGISTRADA';
																					} else echo date('d-m-Y', strtotime($fechaIngreso));?></td>
										<!--ENTREVISTA VIRTUAL -->												
										<td class="text-center">
											<?php if ($rrhhCliente=='0') {
													echo 'NN';
												} else{
														if ($rrhhCliente == '1') {
															echo 'APTO';
														} else echo 'NO APTO';
												} ?>
										</td>
										<!--ENTREVISTA PERSONAL -->
										<td class="text-center">
											<?php if ($induccion=='0') {
											echo 'NN';
											} else{
												if ($induccion == '1') {
													echo 'APTO';
												} else echo 'NO APTO';
											} ?>
										</td>
										<!--VERIFICACION DE ANTECEDENTES -->
										<td class="text-center">
											<?php if ($entrevista=='0') {
											echo 'NN';
											} else{
												if ($entrevista== '1') {
													echo 'APTO';
												} else echo 'NO APTO';
											} ?>
										</td>
										<!--VERIFICACION LABORAL -->
										<td class="text-center">
											<?php if ($verLaboral=='0') {
											echo 'NN';
										} else{
												if ($verLaboral == '1') {
													echo 'APTO';
												} else echo 'NO APTO';
										} ?>
										</td>
										<!--INFORME PSICOLÓGICO -->
										<td class="text-center">
											<?php if ($informe=='0') {
											echo 'NN';
										} else{
												if ($informe == '1') {
													echo 'APTO';
												} else echo 'NO APTO';
										} ?>
										</td>
										<td class="text-center">
												<?php 

													$count = 1;
													if ($rrhhCliente==1) {
														$count = $count +1;
													}
													if ($informe==1) {
														$count = $count +1;
													}
													if ($induccion==1) {
														$count = $count +1;
													}
													if ($entrevista==1) {
														$count = $count +1;
													}
													if ($fechaEnvio!=null) {
														$count = $count +1;
													}
													if ($fechaIngreso!=null) {
														$count = $count +1;
													}
													switch ($count) {
														case 1:
															echo "15%";
															break;
														case 2:
															echo "28%";
															break;
														case 3:
															echo "42%";
															break;
														case 4:
															echo "57%";
															break;
														case 5:
															echo "71%";
															break;
														case 6:
															echo "85%";
															break;
														case 7:
															echo "100%";
															break;
														default:
															echo "error en la matrix";
															break;
													}
													
												?>
											 	
										</td>
										<td class="text-center"><?php echo $ordenServicio; ?></td>
										<td class="text-center"><?php echo $factura; ?></td>
										<td class="text-center">
											<a >
												<button class="btn btn-primary btn-circle" style="background-color: #878787;" type="button" title="CV" onClick="window.open('<?php echo $rutaCV;?>');"><i class="fa fa-file"></i>
	                      </button>
											</a>
											<a href="#edit<?php echo $codigo;?>" data-toggle="modal">
												<button type='button' class='btn btn-warning btn-circle' title="EDITAR">
													<span class='glyphicon glyphicon-edit ' aria-hidden='true'></span>
												</button>
											</a>
											<?php if ($elegido == 1) { ?>
												<a href="../controlador/deseleccionar.php?codigo=<?php echo $codigo;?>">
												<button class="btn btn-primary btn-circle" type="button" title="DESELECCIONAR"><i class="fa fa-times"></i>
	                      </button>
												</a>
											<?php } else {	 ?>
												<a href="../controlador/seleccionar.php?codigo=<?php echo $codigo;?>">
												<button class="btn btn-primary btn-circle" type="button" title="SELECCIONAR"><i class="fa fa-check"></i>
	                      </button>
											</a>
											<?php } ?>
											
											<a href="#backup<?php echo $codigo;?>" data-toggle="modal">
												<button class="btn btn-primary btn-circle" style="background-color: #008CBA;" type="button" title="BACKUP"><i class="fa fa-cloud-upload"></i>
	                      </button>
											</a>
											<a href="#delete<?php echo $codigo;?>" data-toggle="modal">
												<button type='button' class='btn btn-danger btn-circle' title="ELIMINAR">
													<span class='glyphicon glyphicon-trash' aria-hidden='true'></span>
												</button>
											</a>
										</td>
	                </tr>
			        	<?php } ?>
                
              </tbody>
	            <!-- Back up Modal -->
					    <div id="backup<?php echo $codigo;?>" class="modal fade" role="dialog">
            		<div class="modal-dialog">
              		<form method="post" action="../controlador/backup.php?codigo=<?php echo $codigo; ?>">
                  	<div class="modal-content">
                    	<div class="modal-header">
                     		<button type="button" class="close" data-dismiss="modal">&times;</button>
                     		<h4 class="modal-title">BACK UP</h4>
                    	</div>
                    	<div class="modal-body">
                     		<input type="hidden" name="delete_id" value="<?php echo $codigo; ?>">
                     		<p>¿En que puesto consideraría a <strong><?php echo $nombres; ?></strong> como back up?</p><br>
                     		<div >
                     			<input type="text" name="puestoBackup" size="60">
                     		</div>
                     	</div>

                     	<div class="modal-footer">
                        <button type="submit" name="btnEliminar" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> ENVIAR</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> CANCELAR</button>
                      </div>
                    </div>
              		</form>
                </div>
					    </div>     
					              	
              <!--MODAL DE EDITAR-->
              <div id="edit<?php echo $codigo; ?>" class="modal fade" role="dialog">   
   										<form class="form-signin" action="../controlador/editarTerna.php?codigo=<?php echo $codigo;?>" method="POST" enctype="multipart/form-data">	
											<div class="modal-dialog" >
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
														<h4 class="modal-title">EDITAR TERNA</h4>
												</div>
												<div class="modal-body">
													<?php 
													$rs=ejecutarQuery("SELECT * FROM ternas where codigo=$codigo");
													$row=mysqli_fetch_assoc($rs)
													?>
													<div class="row">
											          	<div class="form-group col-md-2">																				
															<label for="txtcodigo" class="col-2 col-form-label">CODIGO:</label>													
														</div>
														<div class="col-md-4">
															<div class="form-group">																		    
																<input type="text" name='txtcodigo' id='txtcodigo' class='form-control' value="<?php echo $codigo; ?>" readonly="readonly" >
															</div>
														</div>
												  </div>
												<div class="row">
													<div class="col-md-12">
														<div class="form-group">
															<label>NOMBRES:</label>
															<input type="text" name='txtnombre' id='txtnombre' class='form-control' value="<?php echo $nombres." ".$apellidos;?>" readonly >	
														</div>
													</div>					
												</div>
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label>DNI:</label>
															<input type="text" name='dni' id='txtnombre' class='form-control' value="<?php echo $dni?>"  >	
														</div>
													</div>	
													<div class="col-md-6">
														<div class="form-group">
															<label>TELÉFONO:</label>
															<input type="text" name='telefono' id='txtnombre' class='form-control' value="<?php echo $telefono?>"  >	
														</div>
													</div>				
												</div>
															<div class="row">
																<div class="col-md-6">
																	<div class="form-group">
																		<label>FECHA INICIO:</label>
																		<input type="date" name='fechaEnvio' class='form-control' value="<?php echo $row['fechaEnvio']; ?>" >
																	</div>						
																</div>
												          		<div class="col-md-6">
																	<div class="form-group">
																		<label> ENTREVISTA VIRTUAL</label>
																			<?php
																			if ($row['rrhhCliente']=='0') {
																			?>
																				<select name="rrhh" class="form-control">
							                                                    	<option value="0" selected="selected">NN</option>
							                                                        <option value="1">APTO</option>
							                                                    	<option value="2">NO APTO</option>
							                                                    </select>
							                                                <?php
																			} else {
																				if ($row['rrhhCliente']=='1'){
																			?>
																					<select name="rrhh" class="form-control">
							                                                    	<option value="0" >NN</option>
							                                                        <option value="1" selected="selected">APTO</option>
							                                                    	<option value="2">NO APTO</option>
							                                                    </select>
							                                                <?php
																				} else {
																			?>
																					<select name="rrhh" class="form-control">
								                                                    	<option value="0" >NN</option>
								                                                        <option value="1" >APTO</option>
								                                                    	<option value="2" selected="selected">NO APTO</option>
							                                                    	</select>
							                                                <?php
																				}
																			}
																			?>

																	
																	</div>
																</div>					
															</div>
															<div class="row">
																<div class="col-md-6">
																	<div class="form-group">
																				<label>ENTREVISTA PERSONAL</label>
																				<?php
																			if ($row['induccion']=='0') {
																			?>
																				<select name="induccion" class="form-control">
							                                                    	<option value="0" selected="selected">NN</option>
							                                                        <option value="1">APTO</option>
							                                                    	<option value="2">NO APTO</option>
							                                                    </select>
							                                                <?php
																			} else {
																				if ($row['induccion']=='1'){
																			?>
																					<select name="induccion" class="form-control">
							                                                    	<option value="0" >NN</option>
							                                                        <option value="1" selected="selected">APTO</option>
							                                                    	<option value="2">NO APTO</option>
							                                                    </select>
							                                                <?php
																				} else {
																			?>
																					<select name="induccion" class="form-control">
								                                                    	<option value="0" >NN</option>
								                                                        <option value="1" >APTO</option>
								                                                    	<option value="2" selected="selected">NO APTO</option>
							                                                    	</select>
							                                                <?php
																				}
																			}
																			?>
																		
																	</div>						
																</div>
												          		<div class="col-md-6">
																	<div class="form-group">
																			<label> VERIFICACION DE ANTECEDENTES</label>
																		<?php
																		if ($row['entrevista']=='0') {
																		?>
																			<select name="entrevistas" class="form-control">
						                                                    	<option value="0" selected="selected">NN</option>
						                                                        <option value="1">APTO</option>
						                                                    	<option value="2">NO APTO</option>
						                                                    </select>
						                                                <?php
																		} else {
																			if ($row['entrevista']=='1'){
																		?>
																				<select name="entrevistas" class="form-control">
						                                                    	<option value="0" >NN</option>
						                                                        <option value="1" selected="selected">APTO</option>
						                                                    	<option value="2">NO APTO</option>
						                                                    </select>
						                                                <?php
																			} else {
																		?>
																				<select name="entrevistas" class="form-control">
							                                                    	<option value="0" >NN</option>
							                                                        <option value="1" >APTO</option>
							                                                    	<option value="2" selected="selected">NO APTO</option>
						                                                    	</select>
						                                                <?php
																			}
																		}
																		?>
																	</div>
																</div>					
															</div>
															<div class="row">
																<div class="col-md-6">
																	<div class="form-group">
																			<label>VERIFICACION LABORAL</label>
																			<?php
																		if ($row['verLaboral']=='0') {
																		?>
																			<select name="verLaboral" class="form-control">
						                                                    	<option value="0" selected="selected">NN</option>
						                                                        <option value="1">APTO</option>
						                                                    	<option value="2">NO APTO</option>
						                                                    </select>
						                                                <?php
																		} else {
																			if ($row['verLaboral']=='1'){
																		?>
																				<select name="verLaboral" class="form-control">
						                                                    	<option value="0" >NN</option>
						                                                        <option value="1" selected="selected">APTO</option>
						                                                    	<option value="2">NO APTO</option>
						                                                    </select>
						                                                <?php
																			} else {
																		?>
																				<select name="verLaboral" class="form-control">
							                                                    	<option value="0" >NN</option>
							                                                        <option value="1" >APTO</option>
							                                                    	<option value="2" selected="selected">NO APTO</option>
						                                                    	</select>
						                                                <?php
																			}
																		}
																		?>
																		
																	</div>						
																</div>
												          		<div class="col-md-6">
																	<div class="form-group">
																			<label> INFORME PSICOLÓGICO </label>
																		<?php
																		if ($row['informe']=='0') {
																		?>
																			<select name="informe" class="form-control">
						                                                    	<option value="0" selected="selected">NN</option>
						                                                        <option value="1">APTO</option>
						                                                    	<option value="2">NO APTO</option>
						                                                    </select>
						                                                <?php
																		} else {
																			if ($row['informe']=='1'){
																		?>
																				<select name="informe" class="form-control">
						                                                    	<option value="0" >NN</option>
						                                                        <option value="1" selected="selected">APTO</option>
						                                                    	<option value="2">NO APTO</option>
						                                                    </select>
						                                                <?php
																			} else {
																		?>
																				<select name="informe" class="form-control">
							                                                    	<option value="0" >NN</option>
							                                                        <option value="1" >APTO</option>
							                                                    	<option value="2" selected="selected">NO APTO</option>
						                                                    	</select>
						                                                <?php
																			}
																		}
																		?>
																	
																	</div>
																</div>					
															</div>
															<div class="row">
																<div class="col-md-6">
																	<div class="form-group">
																		<label>FECHA INGRESO:</label>
																		<input type="date" name='fechaIngreso' class='form-control' value="<?php echo $row['fechaIngreso']; ?>" >
																	</div>						
																</div>
												          		<div class="col-md-6">
																	<div class="form-group">
																		<label>N° ORDEN DE SERVICIO:</label>
																		<input type="text" name='ordenServicio' class='form-control' value="<?php echo $row['ordenServicio']; ?>" >
																	</div>
																</div>					
															</div>
															<div class="row">
																<div class="col-md-6">
																	<div class="form-group">
																		<label>N° FACTURA</label>
																		<input type="text" name='factura' class='form-control' value="<?php echo $row['factura']; ?>" >
																	</div>						
																</div>
												          		<div class="col-md-6">
																	<div class="form-group">
																		<label>OBSERVACION</label>
																		<textarea class="form-control" name="observacion" style="resize:vertical;"><?php echo $row['observacion']; ?></textarea>
																	</div>
																</div>					
															</div>


															
													</div>	
													<div class="modal-footer"> 
														<button type="submit" class="btn btn-primary" name='btnAgregar'>Actualizar</button>
														<button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
													</div>
												</div>							
											</div>
											</form>
										</div> 
							<!-- Eliminar Modal -->
					    <div id="delete<?php echo $codigo; ?>" class="modal fade" role="dialog">
            		<div class="modal-dialog">
              		<form method="post" action="../controlador/eliminarPostulante.php?codigo=<?php echo $codigo; ?>">
                  	<div class="modal-content">
                    	<div class="modal-header">
                     		<button type="button" class="close" data-dismiss="modal">&times;</button>
                     		<h4 class="modal-title">ELIMINAR POSTULANTE</h4>
                    	</div>
                    	<div class="modal-body">
                     		<input type="hidden" name="delete_id" value="<?php echo $codigo; ?>">
                     		<p>Esta seguro de eliminar a <strong><?php echo $nombres; ?>?</strong></p>
                     	</div>
                     	<div class="modal-footer">
                        <button type="submit" name="btnEliminar" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> YES</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> NO</button>
                      </div>
                    </div>
              		</form>
                </div>
					    </div>
							<!-- PAGINACION -->				
							<tfoot>	
			          <?php
								}
								?>
								<tr>
			            <td colspan="5">
			              <ul class="pagination pull-right"></ul>
			            </td>
			          </tr>
         			</tfoot>
			      </table>
          </div>

        </div>
        <div class="ibox float-e-margins">
        	<div class="ibox-title">
          	<h5>NUEVA TERNA</h5> <span class="label label-primary">T-S|S</span>
            <div class="ibox-tools">
		          <a class="collapse-link">
		           	<i class="fa fa-chevron-up"></i>
		          </a>
            </div>
          </div>
                                  <div class="ibox-content">
                                  	<div>
                                      <table class="footable table table-stripped toggle-arrow-tiny">
                                      	<form class="form-signin" action="../controlador/nuevaterna.php?codigo=<?php echo $proceso; ?>" method="POST" enctype="multipart/form-data">
			                            				<thead>
			                            					<tr>
                                  						<div class="row form-group">
			                                        	<div class="col-md-3">
			                                            <label for="cliente">NOMBRES:</label>
			                                            <input type="text" class="form-control" rows="3" name="nombres" required>
			                                        	</div>
			                                        	<div class="col-md-3">
			                                            <label for="cliente">APELLIDOS:</label>
			                                            <input type="text"  class="form-control" rows="3" name="apellidos">
			                                        	</div>
		                                  					<div class="col-md-3">
					                                        	<label for="cliente">DNI:</label>
					                                            <input type="text" class="form-control" rows="3" name="dni" required>
					                                      </div>
			                                      		<div class="col-md-3">
			                                       		 <label for="cliente">TELEFONO:</label>
			                                          	<input type="text" class="form-control" rows="3" name="telefono" >
			                                      		</div>
                                  				</div>
                                  				<div class="row form-group">
                                  					<div class="col-md-9">
		                                        	<label for="cliente">CV:</label>
		                                          <div class="fileinput fileinput-new input-group" data-provides="fileinput">
				                                      	
							                                	<div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
							                                	<span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Seleccionar CV</span><span class="fileinput-exists">Cambiar</span><input type="file" name="cv"></span>
							                                	<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
					                           					</div>  
			                                      </div>
					                                      <div class="col-md-3">
			                                        	<label for="cliente">FECHA DE INICIO:</label>
			                                            <input type="date" class="form-control" rows="3" name="fechaEnvio" >
			                                      		</div>
			                                      
			                                   	</div>
			                                    <div class="row form-group">
			                                    	<div class="col-md-9">
			                                            <label for="cliente">OBSERVACIONES </label>
			                                        	<textarea name="observaciones" class="form-control"
																									style="resize:vertical; min-height: 35px">
																								</textarea>
			                                      </div>
			                                      <div class="col-md-3" align="right">
			                                      	<br>
			                                        	<br style="line-height: 10px;">
			                                            	<button  class="btn btn-primary btn-block" type="submit" style='width:130px; height:35px;'>REGISTRAR</button>
			                                      </div> 
			                                    	</div>
			                                </tr>
			                            </thead>
			                            </form>
			                       	 </table>
                                     	</div>
                                 	</div>
                  </div>
              </div>
          </div>	
      	</div>
          
<?php include('footer.php'); ?>
