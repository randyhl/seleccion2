
<!DOCTYPE html>
<html>


<!-- Mirrored from webapplayers.com/inspinia_admin-v2.7.1/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Sep 2017 15:09:22 GMT -->
<head>
    <script language="JavaScript"> 
function mueveReloj(){ 
    momentoActual = new Date() 
    hora = momentoActual.getHours() 
    minuto = momentoActual.getMinutes() 
    segundo = momentoActual.getSeconds() 

    str_segundo = new String (segundo) 
    if (str_segundo.length == 1) 
        segundo = "0" + segundo 

    str_minuto = new String (minuto) 
    if (str_minuto.length == 1) 
        minuto = "0" + minuto 

    str_hora = new String (hora) 
    if (str_hora.length == 1) 
        hora = "0" + hora 

    horaImprimible = hora + " : " + minuto + " : " + segundo 

    document.form_reloj.reloj.value = horaImprimible 

    setTimeout("mueveReloj()",1000) 
} 
</script>   
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>T-SOLUCIONA | Selección</title>
    <link rel="shortcut icon" type="image/x-icon" href="../images/logo.ico" />
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="../css/plugins/dropzone/basic.css" rel="stylesheet">
    <link href="../css/plugins/dropzone/dropzone.css" rel="stylesheet">
    <link href="../css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="../css/plugins/codemirror/codemirror.css" rel="stylesheet">
    <link href="../css/plugins/footable/footable.core.css" rel="stylesheet">
    <!-- Toastr style -->
    <link href="../css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="../js/plugins/gritter/jquery.gritter.css" rel="stylesheet">
    <link href="../css/animate.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">
    

        
</head>

<body>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">

                        <div class="dropdown profile-element"><span>
                            <center> <img  alt="image" class="img-circle" src="../images/logo1.jpeg" /></center>
                             </span>
                             <center><a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">BIENVENIDO <?php echo strtoupper($_SESSION['usuario']) ; ?></strong>
                             </span>
                            </center>   
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a href="profile.html">Perfil</a></li>
                                <li><a href="contacts.html">Contacto</a></li>
                                <li><a href="../controlador/cerrarsesion.php">Logout</a></li>
                            </ul>
                        </div>
                        <div class="logo-element">
                            T-S
                        </div>

                    </li>
                    <li>
                        <a href="admin.php"><i class="fa fa-home"></i> <span class="nav-label">HOME</span></a>
                    </li>
                    <!--<li class="active">-->
                    <li>
                        <a href="usuario.php"><i class="fa fa-users"></i> <span class="nav-label">USUARIOS</span></a>
                    </li>
                </ul>
            </div>
        </nav>

        <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <form role="search" class="navbar-form-custom" action="http://webapplayers.com/inspinia_admin-v2.7.1/search_results.html">
            </form>
        </div>
            
            <ul class="nav navbar-top-links navbar-right">
                
                <body onload="mueveReloj()">
                <li>
                   <form name="form_reloj"> 
                    <input  type="text" name="reloj" size="12" style="background-color : #f3f3f4; color : Grey; font-family : Arial; font-size : 10pt; text-align : center; border: 0" onfocus="window.document.form_reloj.reloj.blur();"> 
                    <span class="m-r-sm text-muted welcome-message">T-SOLUCIONA|SELECCIÓN</span>  
                    </form>
                     
                </li>
                  
                <li>
                    <a href="../controlador/cerrarsesion.php">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
                </body>
                
                
            </ul>

        </nav>
        
        </div>