<?php 

	
	require '../controlador/funciones.php';
	require '../archivo/perfil.php';
	require '../archivo/cliente.php';
	require '../archivo/procesos.php';
  if(! haIniciadoSesion() )
  {
   header('Location: ../index.php');
  }
  if ($_SESSION['usuario']=='admi' or $_SESSION['usuario']=='gerente' or $_SESSION['usuario']=='vanessa' or $_SESSION['usuario']=='alessandra' or $_SESSION['usuario']=='gianella' or $_SESSION['usuario']=='carmen' or $_SESSION['usuario']=='joe' or $_SESSION['usuario']=='karen' or $_SESSION['gianella']) {
  include('header.php'); 
$conexion = new Conexion();
$cn = $conexion->getConexion();
$estado = 100;
if (isset( $_GET['estado'])) {
	$estado = $_GET['estado'];
}								
?>
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row ">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>TABLA DE PROCESOS</h5><span class="label label-primary">T-S|S</span> 
          <div class="ibox-tools">
          	<a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
          </div>
        </div>
        <div class="col-lg-13">
          <ol class="breadcrumb">
            <li>
                &nbsp &nbsp &nbsp &nbsp Selección
            </li>
            <li>
              Requerimientos
            </li>
            <li class="active">
                <strong>Procesos</strong>
            </li>
          </ol>
        </div>
        <div class="ibox-content">
        	<table class="footable table table-stripped toggle-arrow-tiny">
		        <thead>
		         	<tr>
								<td data-toggle="true" class="text-center"><strong>CODIGO</strong></td>
                <td class="text-center"><strong>CLIENTE</strong></td>
                <td class="text-center"><strong>CARGO</strong></td>
                <td class="text-center"><strong>CONSULTOR</strong></td>
                <td class="text-center"><strong>CANTIDAD</strong></td>
                <th data-hide="all">Ciudad</th>
                <th data-hide="all">Fecha de Pedido</th>
                <th data-hide="all">Fecha de Entrega</th>
                <th data-hide="all">Detalles</th>
                <td class="text-center"><strong>OPCIONES</strong></td>
              </tr>
          	</thead>
            <?php
              $procesos = new procesos($cn);
              $datos = $procesos -> listarprocesos(1); //en culminados le mando 0 
              foreach ($datos as $procesos) {
              	$codigo = $procesos[0];
              	$cliente = $procesos[12];
              	$cargo = $procesos[11];
              	$consultor = $procesos[10];
              	$cantidad = $procesos[2];
              	$ciudad = $procesos[3];
              	$fechapedido = $procesos[4];
              	$fechaentrega = $procesos[5];
              	$detalles = $procesos[6];
              	$estado = $procesos[7];
              	$eliminado = $procesos[8];
              	$grifo = $procesos[9];
						?>
		        <tbody>
              <tr>
                <td class="text-center"><?php echo $codigo; ?></td>
								<td class="text-center"><?php echo $cliente;?></td>
								<td class="text-center"><?php echo $cargo; ?></td>
								<td class="text-center"><?php echo $consultor;?></td>
								<td class="text-center"><?php echo $cantidad; ?></td>
								<td class="text-center"><?php echo $ciudad; ?></td>
								<td class="text-center"><?php echo date('d-m-Y', strtotime($fechapedido)); ?></td>
								<td class="text-center"><?php echo date('d-m-Y', strtotime($fechaentrega)); ?></td>
								<td class="text-center"><?php echo $detalles; ?></td>
								<td class="text-center">
									<a href="#edit<?php echo $codigo;?>" data-toggle="modal">
										<button type='button' class='btn btn-warning btn-sm' title="EDITAR">
											<span class='glyphicon glyphicon-edit' aria-hidden='true'></span>
										</button>
									</a> 
									<a href="#delete<?php echo $codigo;?>" data-toggle="modal">
										<button type='button' class='btn btn-danger btn-sm' title="ELIMINAR">
											<span class='glyphicon glyphicon-trash' aria-hidden='true'></span>
										</button>
									</a>
								</td>
              </tr>
            </tbody>
            <!-- Editar Modal -->
            <div id="edit<?php echo $codigo; ?>" class="modal fade" role="dialog">
								<form class="form-signin" action="../controlador/editarProceso.php?codigo=<?php echo $codigo;?>" method="POST" enctype="multipart/form-data">	
								<div class="modal-dialog" >
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4 class="modal-title">EDITAR PROCESO</h4>
										</div>
										<div class="modal-body">
											<?php  
		                        $lol= $codigo;
		                        $blood=ejecutarQuery("SELECT p.*, c.empresa FROM procesos p inner join perfiles c on c.codigo = p.perfil WHERE p.codigo='".$lol."'");
		                        $sven=mysqli_fetch_assoc($blood);
		                	?>
		                	<div class="row">
		                		<div class="col-md-6">
		                			<div class="form-group">																			
														<label >CODIGO:</label>			
														<input type="text" name='codigo' id='txtcodigo' class='form-control' value="<?php echo $codigo; ?>" readonly="readonly" >
													</div>
		                		</div>
		                	</div>
											<div class="row">
								       	<div class="col-md-6">	
									        <div class="form-group">																			
														<label >CONSULTOR:</label>			
														<select name="consultor" class="form-control" required >
															<option <?php if ($sven['consultor']=='ALESSANDRA'){echo 'selected';}?>>ALESSANDRA</option>
															<option <?php if ($sven['consultor']=='CARMEN'){echo 'selected';}?>>CARMEN</option>
															<option <?php if ($sven['consultor']=='JOE'){echo 'selected';}?>>JOE</option>
															<option <?php if ($sven['consultor']=='MARITA'){echo 'selected';}?>>MARITA</option>
															<option <?php if ($sven['consultor']=='ROBERTO'){echo 'selected';}?>>ROBERTO</option>
															<option <?php if ($sven['consultor']=='VANESSA'){echo 'selected';}?>>VANESSA</option>
														</select>
													</div>										
												</div>
												<div class="col-md-6">
													<div class="form-group">																		    
														<label for="cliente">CARGO:</label>
                            <select name="perfil" class="form-control" required>
                              <?php
                                $consulta = ejecutarQuery("SELECT * FROM perfiles");
                                while ($aaa=mysqli_fetch_assoc($consulta)) { ?>
                                	<OPTION VALUE="<?php echo $aaa['codigo']; ?>" <?php if ($aaa['codigo'] == $sven['perfil']) {
                                		echo 'selected';
                                	} ?>><?php echo $aaa['cargo']; ?></OPTION>  
                                <?php } ?>
                            </select>
													</div>
												</div>
									  	</div>
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<label>CANTIDAD:</label>
														<input type="text" name='cantidad' class='form-control' value="<?php echo $sven['cantidad']; ?>" >
													</div>						
												</div>
								        <div class="col-md-6">
													<div class="form-group">
														<label>CIUDAD:</label>
															<input type="text" name='ciudad' class='form-control' value="<?php echo $sven['ciudad']; ?>" >
													</div>
												</div>					
											</div>
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
																<label>FECHA PEDIDO:</label>
																<input type="date" name='fechaPedido' class='form-control' value="<?php echo $sven['fechaPedido']; ?>" >
														
													</div>						
												</div>	
												<div class="col-md-6">
													<div class="form-group">
														<label>FECHA ENTREGA:</label>
														<input type="date" name='fechaEntrega' class='form-control' value="<?php echo $sven['fechaEntrega']; ?>" >
														
													</div>						
												</div>				
											</div>
											<div class="row">
												<div class="col-md-12">
													<label for="cliente">DETALLES:</label>
		                      <textarea style="resize:vertical; min-height: 35px" type="text" class="form-control" rows="3" name="detalles"><?php echo $sven['detalles'];?></textarea>                         
												</div>					
											</div>
										</div>	
										<div class="modal-footer"> 
											<button type="submit" class="btn btn-primary" name='btnAgregar'>Actualizar</button>
											<button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
										</div>
									</div>							
								</div>
							</form>
						</div>     
						<!-- Eliminar Modal -->
				    <div id="delete<?php echo $codigo; ?>" class="modal fade" role="dialog">
          		<div class="modal-dialog">
            		<form method="post" action="../controlador/eliminarProceso.php?codigo=<?php echo $codigo; ?>">
                	<div class="modal-content">
                  	<div class="modal-header">
                   		<button type="button" class="close" data-dismiss="modal">&times;</button>
                   		<h4 class="modal-title">ELIMINAR PROCESO</h4>
                  	</div>
                  	<div class="modal-body">
                   		<input type="hidden" name="delete_id" value="<?php echo $codigo; ?>">
                   		<p>Esta seguro de eliminar el proceso <strong><?php echo $codigo; ?>?</strong></p>
                   	</div>
                   	<div class="modal-footer">
                      <button type="submit" name="btnEliminar" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> YES</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> NO</button>
                    </div>
                  </div>
            		</form>
              </div>
				    </div>
				   	<tfoot>	
		          <?php
							}
							?>
							<tr>
		            <td colspan="6">
		              <ul class="pagination pull-right"></ul>
		            </td>
		          </tr>
       			</tfoot>
		      </table>
        </div>
			</div>
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>CREAR PROCESO</h5> <span class="label label-primary">T-S|S</span>
          <div class="ibox-tools">
            <a class="collapse-link">
            <i class="fa fa-chevron-up"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content">
          <div>
            <table class="footable table table-stripped toggle-arrow-tiny">
             	<form class="form-signin" action="../controlador/nuevoproceso.php" method="POST" enctype="multipart/form-data">
        			<thead>
        				<tr>
        					<div class="row form-group">
           					<div class="col-md-4">
               				<label for="cliente">PERFIL:</label>
               				<div>
               					<select class="form-control m-b" style="width: 295px" name="cargo" id="empresa">
													<?php
                            $perfil = new perfil($cn);
                            $datos = $perfil -> listarperfil(); 
                            foreach ($datos as $perfil) {
                            	$cargo = $perfil[2];
                            	$codigo = $perfil[0];
															//$rs=ejecutarQuery("SELECT * FROM clientes where eliminado=0 order by razonSocial");
															//while($row=mysqli_fetch_assoc($rs)){
																echo "<option value='$codigo' selected>$cargo
																		</option>";
													}?>
												</select>
											</div>
           					</div>
                    <div class="col-md-2">
                        <label for="procesos">CANTIDAD:</label>
                        <input type="number" class="form-control" rows="2" name="cantidad" >
                    </div>
                    <div class="col-md-3">
                        <label for="procesos">CONSULTOR:</label>
                        <select  class="form-control m-b" style="width: 216px" name="consultor" id="consultor">
                        		<option > ALESSANDRA</option>
                        		<option > CARMEN</option>
                        		<option > JOE</option>
                        		<option > MARITA</option>
                        		<option > ROBERTO</option>
                        		<option > VANESSA</option>
                        	</select>
                    </div>
                    <div class="col-md-3">
                        <label for="procesos">CIUDAD:</label>
                        <input type="text" class="form-control" rows="3" name="ciudad" >
                    </div>
        					</div>
          				<div class="row form-group">
          					<div class="col-md-6">
                      <label for="procesos">DETALLES:</label>
                      <textarea style="resize:vertical; min-height: 35px" type="text" class="form-control" rows="3" name="detalles"></textarea>
                    </div>
                    <div class="col-md-3">
                      <label for="cliente">FECHA DE PEDIDO:</label>
                      <input type="date" class="form-control" rows="3" name="fechaPedido" >
                    </div>
                    <div class="col-md-3">
                      <label for="cliente">FECHA DE ENTREGA:</label>
                      <input type="date"  class="form-control" rows="3" name="fechaEntrega" ="">
                    </div>
                  </div>
          				<div class="row form-group">
                    <div class="col-md-3">
                    	<br style="line-height: 10px">
                        <button class="btn btn-primary btn-block" type="submit">CREAR</button>
                    </div>
          				</div>  
                </tr>
        			</thead>
          	</table>
          </div>
       	</div>
      </div>
    </div>
  </div>	
</div>      
<?php include('footer.php'); }
else { ?> <script>
    alert("NO SE TE CONCEDIO PERMISO PARA ESTA VISTA");
    window.history.go(-1);
    </script> <?php } ?>