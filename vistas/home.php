<?php 


require '../controlador/funciones.php';
require '../archivo/notas.php';
if(! haIniciadoSesion() )
  {
   header('Location: ../index.php');
  }
  include('header.php'); 
$conexion = new Conexion();
$cn = $conexion->getConexion();
$estado = 1;
if (isset( $_GET['estado'])) {
	$estado = $_GET['estado'];
}
?>
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5><?php echo strtoupper($_SESSION['usuario']); ?></h5> <span class="label label-primary">T-S|S</span>
          <div align="right">
                <a href="#n" data-toggle="modal" class="btn btn-primary btn-block fa fa-plus-square fa-lg "  type="submit" style='width:140px; height:28px;'>Agregar notita</a>
          </div>
        </div>
        <div class="col-lg-12">
          <ol class="breadcrumb">
            <li>
                &nbsp &nbsp &nbsp &nbsp Home
            </li>
            <li>
              T|Selección
            </li>
            <li class="active">
                <strong>Notas</strong>
            </li>
          </ol>
        </div>
        <div class="ibox-content">
          <div>
            <div class="ibox-content">
              <ul class="notes">
                <!--MODAL DE NUEVO-->
                <div id="n" class="modal fade" role="dialog">
                  <div class="modal-dialog">
                    <form method="post" action="../controlador/guardarNota.php">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">AGREGAR NOTA</h4>
                          </div>
                          <div class="modal-body">
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label>TÍTULO:</label>
                                <input type="text" name='titulo' id='txtrazonsocial' class='form-control' > 
                            </div>
                          </div>          
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label>DESCRIPCIÓN:</label>
                                <textarea style="resize:vertical; min-height: 35px; " type="text" class="form-control" rows="3" name="descripcion"></textarea>
                            </div>            
                          </div>            
                        </div> 
                          <?php if ($_SESSION['usuario']=='gerente'): ?>
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">
                                <label for="procesos">ENVIAR A:</label>
                                <select  class="form-control m-b" style="width: 216px" name="usuario" id="consultor">
                                  <option value="alessandra"> ALESSANDRA</option>
                                  <option value="carmen"> CARMEN</option>
                                  <option value="joe"> JOE</option>
                                  <option value="gerente"> ROBERTO</option>
                                  <option value="vanessa"> VANESSA</option>
                                  <option value="gianella"> GIANELLA</option>
                                </select>
                              </div>            
                            </div>            
                          </div>  
                         <?php endif ?> 
                      </div>  

                          <div class="modal-footer">
                            <button type="submit" name="btnEliminar" class="btn btn-green"><span class="glyphicon glyphicon-plus"></span>AGREGAR NOTITA</button>
                            <button type="button"  class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> CANCELAR</button>
                          </div>
                          
                      </div>
                    </form>
                  </div>
                </div>
                  <?php 
                  $notas = new notas($cn);
                  $datos = $notas -> listarnotas($_SESSION['usuario']); 
                  foreach ($datos as $notas) {
                    $idNotas = $notas[0];
                    $descripcion = $notas[1];
                    $fecha = $notas[2];
                    $usuario = $notas[3];
                    $titulo = $notas[4];
                  ?>
                <tbody>
                 <li>
                    <div>
                      <a href="#edit<?php echo $idNotas;?>" data-toggle="modal" title="EDITAR"><i class="fa fa-pencil-square-o" aria-hidden="true"></i><font color="white">-----</font></a> 
                       <small><?php echo date('d-m-Y', strtotime($fecha));?></small>
                      <h4><?php echo $titulo;?></h4>
                      <p><?php echo $descripcion;?></p>
                      <a href="#delete<?php echo $idNotas;?>" data-toggle="modal" title="ELIMINAR"><i class="fa fa-trash-o "></i></a>
                    </div>
                  </li> 
                </tbody>
                <!--MODAL DE EDITAR-->
                <div id="edit<?php echo $idNotas; ?>" class="modal fade" role="dialog">
                  <form class="form-signin" action="../controlador/editarNota.php?idNotas=<?php echo $idNotas;?>" method="POST" enctype="multipart/form-data">  
                    <div class="modal-dialog" >
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">EDITAR NOTA</h4>
                        </div>
                        <div class="modal-body">
                          <?php 
                          $rs=ejecutarQuery("SELECT * FROM notas where idNotas=$idNotas");
                          $row=mysqli_fetch_assoc($rs)
                          ?>
                          <div class="row">
                            <div class="form-group col-md-2">                                       
                              <label for="txtcodigo" class="col-2 col-form-label">CODIGO:</label>                         
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">                                        
                                <input type="text" name='txtruc' id='txtruc' class='form-control' value="<?php echo $idNotas; ?>" readonly="readonly" >
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">
                                <label>TÍTULO:</label>
                                  <input type="text" name='titulo' id='txtrazonsocial' class='form-control' value="<?php echo $titulo;?>">  
                              </div>
                            </div>          
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">
                                <label>DESCRIPCIÓN:</label>
                                  <textarea style="resize:vertical; min-height: 35px; " type="text" class="form-control" rows="3" name="descripcion"><?php echo $row['descripcion'];?></textarea>
                              </div>            
                            </div>            
                          </div>  
                        </div>  
                        <div class="modal-footer"> 
                          <button type="submit" class="btn btn-primary" name='btnAgregar'>Editar Nota</button>
                          <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
                        </div>
                      </div>              
                    </div>
                  </form>
                </div>
                  <!--MODAL DE ELIMINAR-->
                <div id="delete<?php echo $idNotas; ?>" class="modal fade" role="dialog">
                  <div class="modal-dialog">
                      <form method="post" action="../controlador/eliminarNota.php?idNotas=<?php echo $idNotas;?>">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">ELIMINAR NOTA</h4>
                            </div>
                            <div class="modal-body">
                              <input type="hidden" name="delete_id" value="<?php echo $ruc; ?>">
                              <p>Esta seguro de Eliminar <strong><?php echo $titulo; ?>?</strong></p>
                            </div>
                            <div class="modal-footer">
                              <button type="submit" name="btnEliminar" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span>YES</button>
                              <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> NO</button>
                            </div>
                        </div>
                      </form>
                  </div>
                </div>

                <tfoot> 
                  <?php
                  }
                  ?>
                  <tr>
                    <td colspan="6">
                      <ul class="pagination pull-right"></ul>
                    </td>
                  </tr>
                </tfoot>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php include('footer.php'); ?>