        <div class="row">
            <div class="col-lg-12">
                <div class="wrapper wrapper-content">
                </div>
                <div class="footer">
                    <div class="pull-right">
                        T-SOLUCIONA|<strong>SELECCIÓN</strong>.
                    </div>
                    <div>
                        <p class="m-t"> <small> <a href="#" title="Randy Huaccha 950980044">Área de Sistemas</a> &copy; 2016-2018</small> </p>
                    </div>
                </div>
            </div>
        </div>
        </div>

    </div>

    <!-- Mainly scripts -->
    <script src="../js/jquery-3.1.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="../js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Flot -->
    <script src="../js/plugins/flot/jquery.flot.js"></script>
    <script src="../js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="../js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="../js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="../js/plugins/flot/jquery.flot.pie.js"></script>

    <!-- Peity -->
    <script src="../js/plugins/peity/jquery.peity.min.js"></script>
    <script src="../js/demo/peity-demo.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="../js/inspinia.js"></script>
    <script src="../js/plugins/pace/pace.min.js"></script>

    <!-- jQuery UI -->
    <script src="../js/plugins/jquery-ui/jquery-ui.min.js"></script>

    <!-- GITTER -->
    <script src="../js/plugins/gritter/jquery.gritter.min.js"></script>

    <!-- Sparkline -->
    <script src="../js/plugins/sparkline/jquery.sparkline.min.js"></script>

    <!-- Sparkline demo data  -->
    <script src="../js/demo/sparkline-demo.js"></script>

    <!-- ChartJS-->
    <script src="../js/plugins/chartJs/Chart.min.js"></script>

    <!-- Toastr -->
    <script src="../js/plugins/toastr/toastr.min.js"></script>

    <!-- FooTable -->
    <script src="../js/plugins/footable/footable.all.min.js"></script>
    <!-- Jasny -->
    <script src="../js/plugins/jasny/jasny-bootstrap.min.js"></script>

    <!-- DROPZONE -->
    <script src="../js/plugins/dropzone/dropzone.js"></script>

    <!-- CodeMirror -->
    <script src="../js/plugins/codemirror/codemirror.js"></script>
    <script src="../js/plugins/codemirror/mode/xml/xml.js"></script>
  

    <script src="../js/hora.js"></script>

<?php 
    switch ($estado) {
        case '1':
            ?>
            <script>
            $(document).ready(function() 
            {
                setTimeout(function()
                {
                    toastr.options = 
                    {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 2000
                    };
                toastr.success('Transformamos ideas en grandes experiencias', 'Bienvenido a SELECCIÓN|T-S');
                }, 1000);    
            });
            </script>
            <?php 
            break;
        default:
            # code...
            break;
    }
 ?>
    
    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function() {

            $('.footable').footable();
            $('.footable2').footable();

        });

    </script>
</body>
</html>
