<?php 
	require '../controlador/funciones.php';
	require '../archivo/perfil.php';
	require '../archivo/cliente.php';
  if(! haIniciadoSesion() )
  {
   header('Location: ../index.php');
  }
  if ($_SESSION['usuario']=='admi' or $_SESSION['usuario']=='gerente' or $_SESSION['usuario']=='vanessa' or $_SESSION['usuario']=='alessandra' or $_SESSION['usuario']=='gianella' or $_SESSION['usuario']=='carmen' or $_SESSION['usuario']=='joe' or $_SESSION['usuario']=='karen' or $_SESSION['gianella']) {
  include('header.php'); 
$conexion = new Conexion();
$cn = $conexion->getConexion();
$estado = 100;
if (isset( $_GET['estado'])) {
	$estado = $_GET['estado'];
}								
?>
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row ">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>TABLA DE PERFILES</h5> <span class="label label-primary">T-S|S</span>
          <div class="ibox-tools">
          	<a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
          </div>
        </div>
        <div class="col-lg-13">
          <ol class="breadcrumb">
            <li>
                &nbsp &nbsp &nbsp &nbsp Selección
            </li>
            <li>
              Requerimientos
            </li>
            <li class="active">
                <strong>Perfiles</strong>
            </li>
          </ol>
        </div>
        <div class="ibox-content">
        	<table class="footable table table-stripped toggle-arrow-tiny">
		        <thead>
		         	<tr>
								<td data-toggle="true" class="text-center"><strong>CODIGO</strong></td>
                <td class="text-center"><strong>EMPRESA</strong></td>
                <td class="text-center"><strong>CARGO</strong></td>
                <td class="text-center"><strong>REMUNERACION</strong></td>
                <th data-hide="all">Grado de Instrucción</th>
                <th data-hide="all">Estado Carrera</th>
                <th data-hide="all">Carrera</th>
                <th data-hide="all">Diplomado(s)</th>
                <th data-hide="all">Postgrado</th>
                <th data-hide="all">Idioma</th>
                <th data-hide="all">Nivel de Idioma</th>
                <th data-hide="all">Programas Informáticos</th>
                <th data-hide="all">Nivel de Prog. Inf.</th>
                <th data-hide="all">Experiencia Requerida</th>
                <th data-hide="all">Tiempo de Experiencia</th>
                <th data-hide="all">Otras Características</th>
                <th data-hide="all">Funciones</th>
                <th data-hide="all">Sexo</th>
                <th data-hide="all">Edad Mínima</th>
                <th data-hide="all">Edad Máxima</th>
                <th data-hide="all">Sentinel</th>
                <th data-hide="all">Horario de Inicio</th>
                <th data-hide="all">Horario de Fin</th>
                <th data-hide="all">Planilla</th>
                <th data-hide="all">Detalle de la Rem.</th>
                <th data-hide="all">Disponibilidad de viajar</th>
                <td class="text-center"><strong>OPCIONES</strong></td>
              </tr>
          	</thead>
            	<?php
                $perfil = new perfil($cn);
                $datos = $perfil -> listarperfil(); 
                foreach ($datos as $perfil) {
                	$codigo = $perfil[0];
                	$empresa = $perfil[26];
                	$cargo = $perfil[2];
                	$gradodeinstruccion = $perfil[3];
                	$estadocarrera = $perfil[4];
                	$carrera = $perfil[5];
                	$diplomados = $perfil[6];
                	$postgrado = $perfil[7];
                	$idioma = $perfil[8];
                	$estadoIdioma = $perfil[9];
                	$programasInformaticos = $perfil[10];
                	$nivelProgramas = $perfil[11];
                	$experienciaRequerida = $perfil[12];
                	$tiempoExperiencia = $perfil[13];
                	$otrasCaracteristicas = $perfil[14];
                	$funciones = $perfil[15];
                	$sexo = $perfil[16];
                	$edadMin = $perfil[17];
                	$edadMax = $perfil[18];
                	$sentinel = $perfil[19];
                	$horarioInicio = $perfil[20];
                	$horarioFin = $perfil[21];
                	$remuneracion = $perfil[22];
                	$planilla = $perfil[23];
                	$remuneraciondetalle = $perfil[24];
                	$viajar = $perfil[25];
							?>
		        <tbody>
              <tr>
                <td class="text-center"><?php echo $codigo; ?></td>
								<td class="text-center"><?php echo $empresa;?></td>
								<td class="text-center"><?php echo $cargo; ?></td>
								<td class="text-center"><?php echo $remuneracion;?></td>
								<td class="text-center"><?php echo $gradodeinstruccion; ?></td>
								<td class="text-center"><?php echo $estadocarrera; ?></td>
								<td class="text-center"><?php echo $carrera; ?></td>
								<td class="text-center"><?php echo $diplomados; ?></td>
								<td class="text-center"><?php echo $postgrado; ?></td>
								<td class="text-center"><?php echo $idioma; ?></td>
								<td class="text-center"><?php echo $estadoIdioma; ?></td>
								<td class="text-center"><?php echo $programasInformaticos; ?></td>
								<td class="text-center"><?php echo $nivelProgramas; ?></td>
								<td class="text-center"><?php echo $experienciaRequerida; ?></td>
								<td class="text-center"><?php echo $tiempoExperiencia; ?></td>
								<td class="text-center"><?php echo $otrasCaracteristicas; ?></td>
								<td class="text-center"><?php echo $funciones; ?></td>
								<td class="text-center"><?php echo $sexo; ?></td>
								<td class="text-center"><?php echo $edadMin; ?></td>
								<td class="text-center"><?php echo $edadMax; ?></td>
								<td class="text-center"><?php echo $sentinel; ?></td>
								<td class="text-center"><?php echo $horarioInicio; ?></td>
								<td class="text-center"><?php echo $horarioFin; ?></td>
								<td class="text-center">
									<?php 
										if ($planilla ==1) {
											echo 'SI';
										} else echo 'NO'; 
									?>
								</td>
								<td class="text-center"><?php echo $remuneraciondetalle; ?></td>
								<td class="text-center">
									<?php 
										if ($viajar ==1) {
											echo 'SI';
										} else echo 'NO'; 
									?>
								</td>
								<td class="text-center">
									<a href="#edit<?php echo $codigo;?>" data-toggle="modal">
										<button type='button' class='btn btn-warning btn-sm' title="EDITAR">
											<span class='glyphicon glyphicon-edit' aria-hidden='true'></span>
										</button>
									</a> 
									<a href="#delete<?php echo $codigo;?>" data-toggle="modal">
										<button type='button' class='btn btn-danger btn-sm' title="ELIMINAR">
											<span class='glyphicon glyphicon-trash' aria-hidden='true'></span>
										</button>
									</a>
								</td>
              </tr>
            </tbody>             	
            <!--MODAL DE EDITAR-->
            <div id="edit<?php echo $codigo; ?>" class="modal fade" role="dialog">
							<div class="modal-dialog modal-lg" >
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4 class="modal-title">EDITAR PERFIL</h4>
									</div>
									<div class="modal-body">
							    	<div class="container-fluid">
			                <div class="row">
			                	<?php  
	                        $lol= $codigo;
	                        $blood=ejecutarQuery("SELECT * FROM perfiles WHERE codigo='".$lol."'");
	                        $sven=mysqli_fetch_assoc($blood);
			                	?>
		                    <div class="col-sm-offset-0 col-sm-12">
	                        <div class="row">
                            <form class="form-signin" action="../controlador/editarPerfil.php?id=<?php echo $sven['codigo'];?>" method="POST" enctype="multipart/form-data">
                              <div class="panel panel-primary">
                                <div class="panel-body">
                                  <div class="row form-group">
                                    <div class="col-md-6">
                                      <label for="cliente">CARGO:</label>
                                      <input type="text" class="form-control" rows="3" name="cargo" required value="<?php echo $sven['cargo'];?>">
                                    </div>
                                    <div class="col-md-3">
                                      <label for="cliente">EMPRESA:</label>
                                      <select name="selectEmpresa" class="form-control" required>
                                        <?php
                                            $consulta = ejecutarQuery("SELECT * FROM clientes WHERE eliminado = 0 ");
                                            while ($aaa=mysqli_fetch_assoc($consulta)) {
                                            	?>
                                            	<OPTION VALUE="<?php echo $aaa['ruc']; ?>" <?php if ($aaa['ruc']== $sven['empresa']) {
                                            		echo 'selected';}?> 
                                            	><?php echo $aaa['razonSocial']; ?></OPTION> 
                                        <?php } ?>   
                                        
                                      </select>
                                    </div>
                                    <div class="col-md-3">
                                      <label for="cliente">GRADO INSTRUCCIÓN:</label>
                                        <?php 
                                       		if($sven['gradoInstruccion']=='Tecnico o Universitario'){
                                        ?>
                                          <select name="selectGradoInstruccion" class="form-control">
                                          	<option>Secundaria</option>
                                            <option>Tecnico</option>
                                          	<option>Universitario</option>
                                          	<option selected="selected">Tecnico o Universitario</option>
                                          </select>
                                        <?php
                                        } else{
                                        		if($sven['gradoInstruccion']=='Tecnico'){
                                       	?>
                                       			<select name="selectGradoInstruccion" class="form-control">
                                            	<option>Secundaria</option>
                                              <option selected="selected">Tecnico</option>
                                            	<option>Universitario</option>
                                            	<option>Tecnico o Universitario</option>
                                            </select>
                                       	<?php
                                        	}
                                        			else{
                                                    if($sven['gradoInstruccion']=='Secundaria'){
                                        ?>
                                                        <select name="selectGradoInstruccion" class="form-control">
                                                        	<option selected="selected">Secundaria</option>
                                                        	<option>Tecnico</option>
                                                        	<option >Universitario</option>
                                                        	<option>Tecnico o Universitario</option>
                                                        </select>
                                        <?php
                                        }
                                          else{
                                        ?>                
                                          		<select name="selectGradoInstruccion" class="form-control">
                                                <option>Secundaria</option>
                                                <option>Tecnico</option>
                                                <option selected="selected">Universitario</option>
                                                <option>Tecnico o Universitario</option>
                                              </select>
                                       	<?php
                                        		}
                                        	}
                                    		}
                                        ?>
                                    </div>
                                  </div>
                                  <div class="row form-group">
                                    <div class="col-md-6">
                                      <label for="cliente">CARRERA:</label>
                                      <input type="text" class="form-control" rows="3" name="carrera"  value="<?php echo $sven['carrera'];?>">
                                    </div>
                                    <div class="col-md-3">
                                      <label for="cliente">ESTADO DE LA CARRERA</label>
                                        <?php
                                        	if($sven['estadoCarrera']=='Ultimos Ciclos'){
                                        ?>
                                            <select name="selectEstadoCarrera" class="form-control" required>
                                              <option selected="selected">Ultimos Ciclos</option>
                                              <option>Egresado</option>
                                              <option>Bachiller</option>
                                              <option>Titulado</option>
                                              <option>Colegiado</option>
                                            </select>
                                        <?php
                                        } else {
                                              if($sven['estadoCarrera']=='Egresado'){  
                                        ?>
                                            <select name="selectEstadoCarrera" class="form-control" required>
                                              <option>Ultimos Ciclos</option>
                                              <option selected="selected">Egresado</option>
                                              <option>Bachiller</option>
                                              <option>Titulado</option>
                                              <option>Colegiado</option>
                                            </select>
                                        <?php
                                        }else{ 
                                            if($sven['estadoCarrera']=='Bachiller'){
                                        ?>
                                            <select name="selectEstadoCarrera" class="form-control" required>
                                              <option>Ultimos Ciclos</option>
                                              <option>Egresado</option>
                                              <option selected="selected">Bachiller</option>
                                              <option>Titulado</option>
                                              <option>Colegiado</option>
                                            </select>
                                        <?php
                                            } else{
                                                if($sven['estadoCarrera']=='Titulado'){
                                        ?>
                                            <select name="selectEstadoCarrera" class="form-control" required>
                                                <option>Ultimos Ciclos</option>
                                                <option>Egresado</option>
                                                <option>Bachiller</option>
                                                <option selected="selected">Titulado</option>
                                                <option>Colegiado</option>
                                            </select>
                                        <?php
                                                } else {
                                        ?>
                                                  <select name="selectEstadoCarrera" class="form-control" required>
                                                    <option>Ultimos Ciclos</option>
                                                    <option>Egresado</option>
                                                    <option>Bachiller</option>
                                                    <option>Titulado</option>
                                                    <option selected="selected">Colegiado</option>
                                                  </select>
                                        <?php
                                                }
                                            }}
                                        }  
                                        ?>
                                    </div>
                                    <div class="col-md-3">
                                      <label for="cliente">DIPLOMADO:</label>
                                      <input type="text" class="form-control" rows="3" name="diplomado" value="<?php echo $sven['diplomados'];?>">
                                    </div>
                                  </div>
                                  <div class="row form-group">
                                    <div class="col-md-6">
                                      <label for="cliente">IDIOMAS:</label>
                                      <input type="text" class="form-control" rows="3" name="idiomas"  value="<?php echo $sven['idioma'];?>">
                                    </div>
                                    <div class="col-md-3">
                                      <label for="cliente">NIVEL DE IDIOMA</label>
                                        <?php  
                                        switch ($sven['estadoIdioma']) {
                                        	case 'Básico':
                                        ?>
                                        	<select name="selectNivelIdioma" class="form-control" >
                                            <option selected="selected">Básico</option>
                                            <option>Intermedio</option>
                                            <option>Avanzado</option>
                                           </select>
                                    			<?php
                                        		break;
                                        	case 'Intermedio':
                                        ?>
                                        		<select name="selectNivelIdioma" class="form-control" required>
                                              <option>Básico</option>
                                              <option selected="selected">Intermedio</option>
                                              <option>Avanzado</option>
                                            </select>
                                   			 <?php
                                        		break;
                                        	case 'Avanzado':
                                        ?>
                                        		<select name="selectNivelIdioma" class="form-control" required>
                                              <option>Básico</option>
                                              <option>Intermedio</option>
                                              <option selected="selected">Avanzado</option>
                                            </select>
                                   			 <?php
                                        		break;
                                        	default:
                                        		# code...
                                        		break;
                                        }
                                       	?>
                                    </div>
                                    <div class="col-md-3">
                                      <label for="cliente">POST GRADO:</label>
                                      <input type="text" class="form-control" rows="3" name="postGrado"  value="<?php echo $sven['postgrado'];?>">
                                    </div>
                                  </div>
                                  <div class="row form-group">
                                    <div class="col-md-6">
                                      <label for="cliente">PROGRAMAS INFORMÁTICOS:</label>
                                      <input type="text" class="form-control" rows="3" name="programas"  value="<?php echo $sven['programasInformaticos'];?>">
                                    </div>
                                    <div class="col-md-3">
                                      <label for="cliente">NIVEL DEL DOMINIO</label>
                                        <?php  
                                        switch ($sven['nivelProgramas']) {
                                        	case 'Básico':
                                        ?>
                                          	<select name="selectNivelProgramas" class="form-control" >
                                              <option selected="selected">Básico</option>
                                              <option>Intermedio</option>
                                              <option>Avanzado</option>
                                            </select>
                                   			 <?php
                                        		break;
                                        	case 'Intermedio':
                                        ?>
                                        		<select name="selectNivelProgramas" class="form-control" >
                                              <option>Básico</option>
                                              <option selected="selected">Intermedio</option>
                                              <option>Avanzado</option>
                                            </select>
                                   			 <?php
                                        		break;
                                        	case 'Avanzado':
                                        ?>
                                        		<select name="selectNivelProgramas" class="form-control" >
                                              <option>Básico</option>
                                              <option>Intermedio</option>
                                              <option selected="selected">Avanzado</option>
                                            </select>
                                    		<?php
                                        		break;
                                        	default:
                                        		# code...
                                        		break;
                                        }
                                       	?>
                                    </div>
                                    <div class="col-md-3">
                                      <label for="cliente">SEXO:</label>
                                      	<?php  
                                        	switch ($sven['sexo']) {
                                        		case 'Femenino':
                                        ?>
                                          		<select name="selectSexo" class="form-control" >
                                                <option selected="selected">Femenino</option>
                                                <option>Masculino</option>
                                                <option>Indistinto</option>
                                            	</select>
                                    		<?php
                                        		break;
                                        	case 'Masculino':
                                        ?>
                                        		<select name="selectSexo" class="form-control" >
                                              <option>Femenino</option>
                                              <option selected="selected">Masculino</option>
                                              <option>Indistinto</option>
                                            </select>
                                    		<?php
                                        		break;
                                        	case 'Indistinto':
                                        ?>
                                        		<select name="selectSexo" class="form-control" >
                                              <option>Femenino</option>
                                              <option>Masculino</option>
                                              <option selected="selected">Indistinto</option>
                                            </select>
                                      	<?php
                                          		break;
                                          	default:
                                            ?>
                                          		  <select name="selectSexo" class="form-control" >
                                                <option selected="selected">Femenino</option>
                                                <option>Masculino</option>
                                                <option>Indistinto</option>
                                              </select>
                                              <?php 
                                          		break;
                                          }
                                         	?>
                                    </div>
                                  </div>
                                  <div class="row form-group">
                                    <div class="col-md-6">
                                      <label for="cliente">EXPERIENCIA REQUERIDA</label>
                                      <input type="text" class="form-control" rows="3" name="experiencia"  value="<?php echo $sven['experienciaRequerida'];?>">
                                    </div>
                                    <div class="col-md-3">
                                      <label for="cliente">TIEMPO DE EXPERIENCIA</label>
                                      <input type="text" class="form-control" rows="3" name="tiempoExperiencia"  value="<?php echo $sven['tiempoExperiencia'];?>">
                                    </div>
                                    <div class="col-md-3">
                                      <label for="cliente">EDAD MÍNIMA:</label>
                                      <input type="number" class="form-control" rows="3" name="edadMinima" required=""  value="<?php echo $sven['edadMin'];?>">
                                    </div>
                                  </div>
                                  <div class="row form-group">
                                    <div class="col-md-6">
                                      <label for="cliente">SENTINEL:</label>
                                      <input type="text" class="form-control" rows="3" name="sentinel" required value="<?php echo $sven['direccion'];?>">
                                    </div>
                                    <div class="col-md-3">
                                      <label for="cliente">DISPONIBILIDAD DE VIAJAR</label>
                                      <?php 
                                      if($sven['viajar']==0){
                                      ?>
                                          <select name="selectViajar" class="form-control" required>
                                            <option value="0" selected="selected">No</option>
                                            <option value="1">Si</option>
                                        </select>
                                      <?php
                                      } else{
                                      ?>
                                          <select name="selectViajar" class="form-control" required>
                                            <option value="0">No</option>
                                            <option value="1" selected="selected">Si</option>
                                        </select>
                                      <?php
                                      }
                                      ?>
																		</div>
                                    <div class="col-md-3">
                                      <label for="cliente">EDAD MÁXIMA:</label>
                                      <input type="number" class="form-control" rows="3" name="edadMaxima" required value="<?php echo $sven['edadMax'];?>">
                                    </div>
                                  </div>
                                  <div class="row form-group">
                                    <div class="col-md-6">
                                      <label for="cliente">Funciones:</label>
                                      <textarea style="resize:vertical; min-height: 35px" type="text" class="form-control" rows="3" name="funciones" required><?php echo $sven['funciones'];?></textarea>
                                    </div>
                                    <div class="col-md-6">
                                      <label for="cliente">Detalle de la remuneracion:</label>
                                      <textarea style="resize:vertical; min-height: 35px" type="text" class="form-control" rows="3" name="detalleRemuneracion" ><?php echo $sven['remuneracionDetalle'];?></textarea>
                                    </div>
                                  </div>
                                  <div class="row form-group">
                                    <div class="col-md-3">
                                        <label for="cliente">Horario de Inicio</label>
                                        <input type="time" class="form-control" rows="3" name="horaInicio"  value="<?php echo $sven['horarioInicio'];?>">
                                    </div>
                                    <div class="col-md-3">
                                        <label for="cliente">Horario de Fin</label>
                                        <input type="time" class="form-control" rows="3" name="horaFin"  value="<?php echo $sven['horarioFin'];?>">
                                    </div>
                                    <div class="col-md-3">
                                        <label for="cliente">Remuneracion:</label>
                                        <input type="number" class="form-control" rows="3" name="remuneracion" required value="<?php echo $sven['remuneracion'];?>">
                                    </div>
                                    <div class="col-md-3">
                                      <label for="cliente">Planilla:</label>
                                        <?php 
                                        if($sven['planilla']==0){
                                        ?>
                                          <select name="selectPlanilla" class="form-control" required>
                                            <option value="0" selected="selected">No</option>
                                            <option value="1">Si</option>
                                          </select>
                                          <?php
                                          } else{
                                          ?>
                                            <select name="selectPlanilla" class="form-control" required>
                                              <option value="0">No</option>
                                              <option value="1" selected="selected">Si</option>
                                          	</select>
                                        	<?php
                                        	}
                                        	?>
                                    </div>
                                  </div>
                                  <div class="row form-group">
                                    <div class="col-md-6">
                                      <label for="cliente">Otras características:</label>
                                      <textarea style="resize:vertical; min-height: 35px" type="text" class="form-control" rows="3" name="otros"><?php echo $sven['otrasCaracteristicas'];?></textarea>
                                    </div>
                                    <div class="col-md-3">
                                      <br><br>
                                      <button type="submit" style="width: 180px" class="btn btn-primary" name='btnAgregar'>Actualizar</button>
                                    </div>
                                    <div class="col-md-3">
                                      <br><br>
                                      <button type="button" style="width: 180px" class="btn btn-default" data-dismiss="modal">Salir</button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </form>
	                        </div>
		                    </div>
			                </div>
							      </div>
								  </div>
								</div>
							</div>
						</div>
						<!-- Eliminar Modal -->
				    <div id="delete<?php echo $codigo; ?>" class="modal fade" role="dialog">
          		<div class="modal-dialog">
            		<form method="post" action="../controlador/eliminarPerfil.php?codigo=<?php echo $codigo; ?>">
                	<div class="modal-content">
                  	<div class="modal-header">
                   		<button type="button" class="close" data-dismiss="modal">&times;</button>
                   		<h4 class="modal-title">ELIMINAR PERFIL</h4>
                  	</div>
                  	<div class="modal-body">
                   		<input type="hidden" name="delete_id" value="<?php echo $codigo; ?>">
                   		<p>Esta seguro de eliminar el perfil de  <strong><?php echo $cargo; ?>?</strong></p>
                   	</div>
                   	<div class="modal-footer">
                      <button type="submit" name="btnEliminar" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> YES</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> NO</button>
                    </div>
                  </div>
            		</form>
              </div>
				    </div>		
						<tfoot>	
		          <?php
							}
							?>
							<tr>
		            <td colspan="5">
		              <ul class="pagination pull-right"></ul>
		            </td>
		          </tr>
       			</tfoot>
		      </table>
        </div>
			</div>
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>CREAR PERFIL</h5> <span class="label label-primary">T-S|S</span>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content">
          <div>
            <table class="footable table table-stripped toggle-arrow-tiny">
              <form class="form-signin" action="../controlador/nuevoperfil.php" method="POST" enctype="multipart/form-data">
		            <thead>
                	<tr>
                		<div class="row form-group">
                   		<div class="col-md-3">
                       	<label for="cliente">EMPRESA:</label>
                     		<div>
                     			<select class="form-control m-b" style="width: 216px" name="empresa" id="empresa">
														<?php
                              $cliente = new cliente($cn);
                              $datos = $cliente -> listarcliente(); 
                              foreach ($datos as $cliente) {
                              	$razonSocial = $cliente[1];
                              	$ruc = $cliente[0];
																//$rs=ejecutarQuery("SELECT * FROM clientes where eliminado=0 order by razonSocial");
																//while($row=mysqli_fetch_assoc($rs)){
																	echo "<option value='$ruc' selected>$razonSocial
																			</option>";
																}?>
													</select>
												</div>
                   		</div>
                      <div class="col-md-3">
                        <label for="cliente">CARGO:</label>
                        <input type="text" class="form-control" rows="3" name="cargo" required>
                      </div>
                      <div class="col-md-6">
                        <label for="cliente">EXPERIENCIA REQUERIDA:</label>
                        <input type="text" class="form-control" rows="3" name="experienciaRequerida" >
                      </div>
                		</div>
              				<div class="row form-group">
              					<div class="col-md-3">
                          <label for="cliente">GRADO DE INSTRUCCIÓN:</label>
                          <div>
                            <select  class="form-control m-b" style="width: 216px" name="gradodeInstruccion" id="gradodeInstruccion">
                          		<option value="secundaria"> SECUNDARIA</option>
                          		<option value="tecnico"> TÉCNICO</option>
                          		<option value="universitario"> UNIVERSITARIO</option>
                          		<option value="tecnico o universitario"> TÉCNICO O UNIVERSITARIO</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <label for="cliente">TIEMPO DE EXPERIENCIA:</label>
                          <input type="text" class="form-control" rows="3" name="tiempoExperiencia" required>
                        </div>
                        <div class="col-md-6">
                          <label for="cliente">SENTINEL:</label>
                          <input type="text"  class="form-control" rows="3"  required="" name="sentinel">
                        </div>   
              				</div>
              				<div class="row form-group">
              					<div class="col-md-3">
                          <label for="cliente">CARRERA:</label>
                          <input type="text" class="form-control" rows="3" name="carrera" required>
                        </div>
              					<div class="col-md-3">
                                      <label for="cliente">ESTADO DE LA CARRERA</label>
                                        <?php
                                          if($sven['estadoCarrera']=='Ultimos Ciclos'){
                                        ?>
                                            <select name="estadoCarrera" class="form-control" required>
                                              <option selected="selected">Ultimos Ciclos</option>
                                              <option>Egresado</option>
                                              <option>Bachiller</option>
                                              <option>Titulado</option>
                                              <option>Colegiado</option>
                                            </select>
                                        <?php
                                        } else {
                                              if($sven['estadoCarrera']=='Egresado'){  
                                        ?>
                                            <select name="estadoCarrera" class="form-control" required>
                                              <option>Ultimos Ciclos</option>
                                              <option selected="selected">Egresado</option>
                                              <option>Bachiller</option>
                                              <option>Titulado</option>
                                              <option>Colegiado</option>
                                            </select>
                                        <?php
                                        }else{ 
                                            if($sven['estadoCarrera']=='Bachiller'){
                                        ?>
                                            <select name="estadoCarrera" class="form-control" required>
                                              <option>Ultimos Ciclos</option>
                                              <option>Egresado</option>
                                              <option selected="selected">Bachiller</option>
                                              <option>Titulado</option>
                                              <option>Colegiado</option>
                                            </select>
                                        <?php
                                            } else{
                                                if($sven['estadoCarrera']=='Titulado'){
                                        ?>
                                            <select name="estadoCarrera" class="form-control" required>
                                                <option>Ultimos Ciclos</option>
                                                <option>Egresado</option>
                                                <option>Bachiller</option>
                                                <option selected="selected">Titulado</option>
                                                <option>Colegiado</option>
                                            </select>
                                        <?php
                                                } else {
                                        ?>
                                                  <select name="estadoCarrera" class="form-control" required>
                                                    <option>Ultimos Ciclos</option>
                                                    <option>Egresado</option>
                                                    <option>Bachiller</option>
                                                    <option>Titulado</option>
                                                    <option selected="selected">Colegiado</option>
                                                  </select>
                                        <?php
                                                }
                                            }}
                                        }  
                                        ?>
                                    </div>
                        <div class="col-md-6">
                          <label for="cliente">OTRAS CARACTERISTICAS: </label>
                        	<textarea name="otrasCaracteristicas" class="form-control"
														style="resize:vertical; min-height: 35px">
													</textarea>
                        </div>
                      </div>
                      <div class="row form-group">
                       	<div class="col-md-3">
                          <label for="cliente">DIPLOMADO:</label>
                          <input type="text" class="form-control" rows="3" name="diplomados" >
                        </div>
                        <div class="col-md-3">
                          <label for="cliente">POSTGRADO:</label>
                          <input type="text" class="form-control" rows="3" name="postgrado" >
                        </div>
                        <div class="col-md-3">
                          <label >PLANILLA:</label>
                          <div>
                          	<select  class="form-control m-b" name="planilla" id="Planilla">
                          		<option value="1"> SI</option>
                          		<option value="0"> NO</option>
                          	</select>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <label>REMUNERACIÓN:</label>
                          <input type="number"  class="form-control" rows="3" name="remuneracion" required>
                        </div>
                      </div>
                      <div class="row form-group">
                      	<div class="col-md-3">
                          <label for="cliente">IDIOMA:</label>
                          <input type="text" class="form-control" rows="3" name="idioma" >
                        </div>
              					<div class="col-md-3">
                          <label for="cliente">NIVEL DE IDIOMA:</label>
                          <div>
                          	<select  class="form-control m-b" style="width: 216px" name="estadoIdioma" id="estadoIdioma">
                          		<option value="Básico"> BÁSICO</option>
                          		<option value="Intermedio"> INTERMEDIO</option>
                          		<option value="Avanzado"> AVANZADO</option>
                          	</select>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <label for="cliente">DETALLE DE LA REMUNERACIÓN:</label>
                          <textarea  name="remuneracionDetalle" class="form-control"
														style="resize:vertical; min-height: 35px">
													</textarea>
                        </div>
                    	</div>
                      <div class="row form-group">
                      	<div class="col-md-3">
                          <label for="cliente">PROGRAMAS INFORMÁTICOS:</label>
                          <input type="text" class="form-control" rows="3" name="programasInformaticos">
                        </div>
                        <div class="col-md-3">
                          <label for="cliente">DOMINIO:</label>
                          <div>
                          	<select  class="form-control m-b" style="width: 216px" name="nivelProgramas" id="nivelProgramas">
                          		<option value="Básico"> BÁSICO</option>
                          		<option value="Intermedio"> INTERMEDIO</option>
                          		<option value="Avanzado"> AVANZADO</option>
                          	</select>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <label for="cliente">FUNCIONES:</label>
                          <textarea name="funciones" class="form-control"
														style="resize:vertical; min-height: 35px" >
													</textarea>
                        </div>
                      </div>
                      <div class="row form-group">
                        <div class="col-md-2">
                          <label for="cliente" style="width: 250px" >DISP. DE VIAJAR:</label>
                          <div>
                            	<select class="form-control m-b"  name="viajar" id="viajar">
                            		<option value="1"> SI</option>
                            		<option value="0"> NO</option>
                            	</select>
                            </div>
                        </div>
                        <div class="col-md-2">
                          <label for="cliente">SEXO:</label>
                          <div>
                          	<select class="form-control m-b"  name="sexo" id="sexo">
                          		<option value="Masculino"> MASCULINO</option>
                          		<option value="Femenino"> FEMENINO</option>
                          		<option value="Indistinto"> INDISTINTO</option>
                          	</select>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <label for="cliente">EDAD MÍNIMA:</label>
                          <input type="number" class="form-control" rows="3" name="edadMin" required>
                        </div>
              					<div class="col-md-2">
                          <label for="cliente">EDAD MÁXIMA:</label>
                          <input type="number" class="form-control" rows="3" name="edadMax" required >
                        </div>
                        <div class="col-md-2">
                          <label for="cliente">HORARIO DE INICIO:</label>
                          <input type="time" class="form-control" rows="3" name="horarioInicio" required>
                        </div>
                        <div class="col-md-2">
                          <label for="cliente">HORARIO DE FIN:</label>
                          <input type="time" class="form-control" rows="3" name="horarioFin" required>
                        </div>
                      </div>	
              				<div class="row form-group">
                				<div class="col-md-3">
                				</div>
                				<div class="col-md-3">
                				</div>
                				<div class="col-md-3">
                				</div>
                        <div class="col-md-3">
                        	<br style="line-height: 10px">
                          <button class="btn btn-primary btn-block" type="submit">CREAR</button>
                        </div>
              				</div>
                  </tr>
                </thead>
		          </form>
		        </table>
          </div>
        </div>
      </div>
    </div>
  </div>	
</div>
<?php include('footer.php'); }
else { ?> <script>
    alert("NO SE TE CONCEDIO PERMISO PARA ESTA VISTA");
    window.history.go(-1);
    </script> <?php } ?>