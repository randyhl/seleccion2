<?php 
	require '../controlador/funciones.php';
	require '../archivo/perfil.php';
	require '../archivo/cliente.php';
	require '../archivo/procesos.php';
  if(! haIniciadoSesion() )
  {
   header('Location: ../index.php');
  }
  if ($_SESSION['usuario']=='admi' or $_SESSION['usuario']=='gerente' or $_SESSION['usuario']=='vanessa' or $_SESSION['usuario']=='alessandra' or $_SESSION['usuario']=='gianella' or $_SESSION['usuario']=='carmen' or $_SESSION['usuario']=='joe' or $_SESSION['usuario']=='karen' or $_SESSION['gianella']) {
  include('header.php');
$conexion = new Conexion();
$cn = $conexion->getConexion();
$estado = 100;
if (isset( $_GET['estado'])) {
	$estado = $_GET['estado'];
}				
?>
	<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row ">
      <div class="col-lg-12">
        <div class="ibox float-e-margins">
          <div class="ibox-title">
            <h5>TABLA DE TODOS LOS PROCESOS CULMINADOS</h5> <span class="label label-primary">T-S|S</span>
            <div class="ibox-tools">
            	<a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
              </a>
            </div>
          </div>
          <div class="col-lg-13">
          <ol class="breadcrumb">
            <li>
                &nbsp &nbsp &nbsp &nbsp Selección
            </li>
            <li>
              Procesos
            </li>
            <li class="active">
                <strong>Culminados</strong>
            </li>
          </ol>
        </div>
          <div class="ibox-content" >
          	<input  type="text" class="form-control input-sm m-b-xs" id="filter" style="width:400px"
                                   placeholder="Buscar en tabla">
          	<table class="footable table table-stripped" data-page-size="10" data-filter=#filter>
			        <thead>
			         	<tr>
									<td data-toggle="true" class="text-center"><strong>CODIGO</strong></td>
                  <td class="text-center"><strong>CLIENTE</strong></td>
                  <td class="text-center"><strong>CARGO</strong></td>
                  <td class="text-center"><strong>CONSULTOR</strong></td>
                  <td class="text-center"><strong>CANTIDAD</strong></td>
                  <td class="text-center"><strong>CIUDAD</strong></td>
                  <th data-hide="all">Fecha de Pedido</th>
                  <th data-hide="all">Fecha de Entrega</th>
                  <th data-hide="all">Detalles</th>
                  <td class="text-center"><strong>OPCIONES</strong></td>
                </tr>
            	</thead>
              <?php
                $procesos = new procesos($cn);
                $datos = $procesos -> listarculminados(1); 
                foreach ($datos as $procesos) {
                	$codigo = $procesos[0];
                	$cliente = $procesos[12];
                	$cargo = $procesos[11];
                	$consultor = $procesos[10];
                	$cantidad = $procesos[2];
                	$ciudad = $procesos[3];
                	$fechapedido = $procesos[4];
                	$fechaentrega = $procesos[5];
                	$detalles = $procesos[6];
                	$estado = $procesos[7];
                	$eliminado = $procesos[8];
                	$grifo = $procesos[9];
							?>
			        <tbody>
                <tr>
                  <td class="text-center"><?php echo $codigo; ?></td>
									<td class="text-center"><?php echo $cliente;?></td>
									<td class="text-center"><?php echo $cargo; ?></td>
									<td class="text-center"><?php echo $consultor;?></td>
									<td class="text-center"><?php echo $cantidad; ?></td>
									<td class="text-center"><?php echo $ciudad; ?></td>
									<td class="text-center"><?php echo date('d-m-Y', strtotime($fechapedido)); ?></td>
									<td class="text-center"><?php echo date('d-m-Y', strtotime($fechaentrega)); ?></td>
									<td class="text-center"><?php echo $detalles; ?></td>
									<td class="text-center">
										<a href="postulante.php?codigo=<?php echo $codigo;?>">
											<button class="btn btn-primary btn-circle" type="button" title="POSTULANTE"><i class="fa fa-user"></i>
                      </button>
										</a> 
										<a href="terna.php?codigo=<?php echo $codigo;?>" data-toggle="modal">
											<button class="btn btn-primary btn-circle" type="button" title="TERNA"><i class="fa fa-list"></i>
                      </button>
										</a>
									</td>
                </tr>
              </tbody>    
							<tfoot>	
			          <?php
								}
								?>
								<tr>
			            <td colspan="7">
			              <ul class="pagination pull-right"></ul>
			            </td>
			          </tr>
         			</tfoot>
			      </table>
          </div>
        </div>
<?php include('footer.php'); }
else { ?> <script>
    alert("NO SE TE CONCEDIO PERMISO PARA ESTA VISTA");
    window.history.go(-1);
    </script> <?php } ?>           
