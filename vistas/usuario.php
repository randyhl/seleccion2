<?php 
  require '../controlador/funciones.php';
  require '../archivo/usuario.php';
  if(! haIniciadoSesion() )
  {
   header('Location: ../index.php');
  }
  include('headeradmi.php');
  $conexion = new Conexion();
  $cn = $conexion->getConexion();
   ?>
  <div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
    <div class="col-lg-13">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5>TABLA DE USUARIOS</h5> <span class="label label-primary">T-S|S</span>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
          </div>
        </div>
        <div class="col-lg-13">
          <ol class="breadcrumb">
            <li>
                &nbsp &nbsp &nbsp &nbsp Selección
            </li>
            <li>
              Administración
            </li>
            <li class="active">
                <strong>Gestor de Usuarios</strong>
            </li>
          </ol>
        </div>
        <div class="ibox-content">
          <div>
            <table class="footable table table-stripped toggle-arrow-tiny table-hover">
              <thead>
                <tr>
                  <td class="text-center"><strong>USUARIO</strong></td>
                  <td class="text-center"><strong>CLAVE</strong></td>
                  <td class="text-center"><strong>ADMIN</strong></td>
                </tr>
              </thead>
                <?php
                $usuario = new usuario($cn);
                $datos = $usuario -> listarusuario(); 
                foreach ($datos as $usuario) {
                  $usuarios = $usuario[0];
                  $clave = $usuario[1];
                  $admin = $usuario[2];
                //$rs=ejecutarQuery("SELECT * FROM clientes where eliminado=0 order by razonSocial");
                //while($row=mysqli_fetch_assoc($rs)){
                ?>
              <tbody>
                <tr>
                  <td class="text-center"><?php echo $usuarios;?></td>
                  <td class="text-center"><?php echo $clave;?></td>
                  <td class="text-center"><?php echo $admin;?></td>
                  <td class="text-center">
                    <a href="#edit<?php echo $usuarios;?>" data-toggle="modal"><button type='button' title="EDITAR"><span class='glyphicon glyphicon-edit' aria-hidden='true'></span></button></a> 
                    <a href="#delete<?php echo $usuarios;?>" data-toggle="modal"><button type='button' title="ELIMINAR"><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></button></a>
                  </td>
                </tr>
              </tbody>
              <!--MODAL DE EDITAR-->
              <div id="edit<?php echo $usuarios; ?>" class="modal fade" role="dialog">
                <form class="form-signin" action="../controlador/guardarUsuario.php?usuario=<?php echo $usuarios;?>" 
                    method="POST" enctype="multipart/form-data">  
                    <div class="modal-dialog" >
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">EDITAR USUARIO</h4>
                        </div>
                        <div class="modal-body">
                            <?php 
                            $rs=ejecutarQuery("SELECT * FROM usuarios WHERE usuario='$usuarios'");
                            $row=mysqli_fetch_assoc($rs)
                            ?>
                          
                           <div class="row">
                            <div class="col-md-4">
                              <div class="form-group">
                                <label>USUARIO:</label>
                                <input type="text" name='usuario' id='usuario' class='form-control' value="<?php echo $usuarios; ?>" required" >  
                              </div>
                            </div>   
                            <div class="col-md-4">
                              <div class="form-group">
                                <label>CLAVE:</label>
                                <input type="text" name='clave' id='clave' class='form-control' value="<?php echo $clave;?>" required>  
                              </div>
                            </div> 
                            <div class="col-md-4">
                              <div class="form-group">
                                <label>ADMIN:</label>
                                <select name="admin" class="form-control" required>
                                 <OPTION <?php if (1== $admin){ echo 'selected';}?>>1</OPTION> 
                                 <OPTION <?php if (0== $admin){ echo 'selected';}?>>0</OPTION>
                                </select>
                              </div>            
                            </div>       
                          </div>
                        </div>  
                        <div class="modal-footer"> 
                          <button type="submit" class="btn btn-primary" name='btnAgregar'>Actualizar</button>
                          <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
                        </div>
                      </div>              
                    </div>
                </form>
              </div>
              <!-- Eliminar Modal -->
              <div id="delete<?php echo $usuarios;?>" class="modal fade" role="dialog">
                <div class="modal-dialog">
                  <form method="post" action="../controlador/eliminarUsuario.php?usuario=<?php echo $usuarios;?>">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">ELIMINAR USUARIO</h4>
                      </div>
                      <div class="modal-body">
                        <input type="hidden" name="delete_id" value="<?php echo $ruc; ?>">
                        <p>Esta seguro de Eliminar <strong><?php echo $usuarios; ?>?</strong></p>
                      </div>
                      <div class="modal-footer">
                        <button type="submit" name="btnEliminar" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span>YES</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> NO</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>

              <tfoot> 
                <?php
                }
                ?>
                <tr>
                  <td colspan="6">
                    <ul class="pagination pull-right"></ul>
                  </td>
                </tr>
              </tfoot>
              
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>REGISTRAR NUEVO USUARIO</h5> <span class="label label-primary">T-S|S</span>
        <div class="ibox-tools">
          <a class="collapse-link">
            <i class="fa fa-chevron-up"></i>
          </a>
        </div>
      </div>
      <div class="ibox-content">
        <div>
          <table class="footable table table-stripped toggle-arrow-tiny">
            <form class="form-signin" action="../controlador/nuevousuario.php" method="POST" enctype="multipart/form-data">
              <thead>
                <div class="row form-group">
                  <div class="col-md-3">
                    <label for="cliente">USUARIO:</label>
                      <input type="text" class="form-control" rows="3" name="usuario" required>
                  </div>
                  <div class="col-md-3">
                    <label for="usuario">CLAVE:</label>
                      <input type="text" class="form-control" rows="3" name="clave" required>
                  </div>
                  <div class="col-md-3">
                    <label for="usuario">ADMIN:</label>

                    <select class="form-control m-b" name="admin" style="size: 100">
                      <OPTION>1</OPTION>
                      <OPTION>0</OPTION>
                    </select>
                  </div>
                  <div class="col-md-3">
                    <br style="line-height: 23px">
                      <button class="btn btn-primary btn-block" type="submit">REGISTRAR</button>
                  </div>
                </div>
              </thead>
            </form>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
  <?php  
  include('footer.php');   
 ?>