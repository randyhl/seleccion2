<?php 
	require '../controlador/funciones.php';
	require '../archivo/perfil.php';
	require '../archivo/cliente.php';
	require '../archivo/terna.php';
	if(! haIniciadoSesion() )
  {
   header('Location: ../index.php');
  }
  include('header.php');
$conexion = new Conexion();
$cn = $conexion->getConexion();
$estado = 100;
if (isset( $_GET['estado'])) {
	$estado = $_GET['estado'];
}	
$proceso = $_GET['codigo'];							
?>
	<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row ">
      <div class="col-lg-13">
        <div class="ibox float-e-margins">
          <div class="ibox-title">
            <h5>TABLA DE POSTULANTES</h5> 
            <div align="right">
            	<a href="terna.php?codigo=<?php echo $proceso;?>" class="btn btn-primary btn-block" type="submit" style='width:90px; height:30px;'>Ir a terna</a>
            </div>
	            	
            <div class="ibox-tools">
            	<a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
              </a>
            </div>
          </div>
          <div class="col-lg-13">
          <ol class="breadcrumb">
            <li>
                &nbsp &nbsp &nbsp &nbsp Selección
            </li>
            <li>
              Procesos
            </li>
            <li class="active">
                <strong>Postulante</strong>
            </li>
          </ol>
        </div>
          <div class="ibox-content">
          	<table class="footable table table-stripped toggle-arrow-tiny">
			        <thead>
			         	<tr>
									<td data-toggle="true" class="text-center"><strong>CÓDIGO</strong></td>
                  <td class="text-center"><strong>POSTULANTE</strong></td>
                  <td class="text-center"><strong>OBSERVACIÓN</strong></td>
                  <th data-hide="all">DNI</th>
                  <th data-hide="all">TELEFONO</th>
                  <th data-hide="all">FECHA DE INICIO</th>
                  <td class="text-center"><strong>OPCIONES</strong></td>
                </tr>
            	</thead>
              <?php
                $postulante = new terna($cn);
                $datos = $postulante -> listarpostulante($proceso); 
                foreach ($datos as $postulante) {
                	$codigo = 	$postulante[0];
                	$proceso = 	$postulante[1];
                	$apellidos = 	$postulante[2];
                	$nombres = 	$postulante[3];
                	$rutaCV = 	$postulante[4];
                	$fechaEnvio = 	$postulante[5];
                	$rrhhCliente = 	$postulante[6];
                	$induccion = 	$postulante[7];
                	$entrevista = 	$postulante[8];
                	$informe = 	$postulante[9];
                	$fechaIngreso = 	$postulante[10];
                	$ordenServicio = 	$postulante[11];
                	$factura = 	$postulante[12];
                	$observacion = 	$postulante[13];
                	$eliminado = 	$postulante[14];
                	$terna = 	$postulante[15];
                	$dni = 	$postulante[16];
                	$telefono = 	$postulante[17];
                	$estacion = 	$postulante[18];
                	$porcentaje = 	$postulante[19];
                	$verLaboral = 	$postulante[20];
                	$elegido = 	$postulante[21];
							?>
			        <tbody>
                <tr>
                  <td class="text-center"><?php echo $codigo; ?></td>
									<td class="text-center"><?php echo $nombres." ".$apellidos; ?></td>
									<td class="text-center"><?php echo $observacion;?></td>
									<td class="text-center"><?php echo $dni; ?></td>
									<td class="text-center"><?php echo $telefono; ?></td>
									<td class="text-center"><?php if ($fechaEnvio == '0000-00-00') {
																								echo 'FECHA NO REGISTRADA';
																					} else echo date('d-m-Y', strtotime($fechaEnvio));?></td> 
									<td class="text-center">
										<a href="">
											<button class="btn btn-primary btn-circle" style="background-color: #878787;" type="button" title="CV" onClick="window.open('<?php echo $rutaCV;?>');"><i class="fa fa-file"></i>
                      </button>
										</a>
										<a href="#edit<?php echo $codigo;?>" data-toggle="modal">
											<button type='button' class='btn btn-warning btn-circle' title="EDITAR">
												<span class='glyphicon glyphicon-edit ' aria-hidden='true'></span>
											</button>
										</a>
										<a href="../controlador/terna.php?codigo=<?php echo $codigo;?>">
											<button class="btn btn-primary btn-circle" type="button" title="TERNA"><i class="fa fa-check"></i>
                      </button>
										</a>
										<a href="#backup<?php echo $codigo;?>" data-toggle="modal">
											<button class="btn btn-primary btn-circle" style="background-color: #008CBA;" type="button" title="BACKUP"><i class="fa fa-cloud-upload"></i>
                      </button>
										</a>
										<a href="#delete<?php echo $codigo;?>" data-toggle="modal">
											<button type='button' class='btn btn-danger btn-circle' title="ELIMINAR">
												<span class='glyphicon glyphicon-trash' aria-hidden='true'></span>
											</button>
										</a>
									</td>
                </tr>
              </tbody>
	            <!-- Back up Modal -->
					    <div id="backup<?php echo $codigo;?>" class="modal fade" role="dialog">
            		<div class="modal-dialog">
              		<form method="post" action="../controlador/backup.php?codigo=<?php echo $codigo; ?>">
                  	<div class="modal-content">
                    	<div class="modal-header">
                     		<button type="button" class="close" data-dismiss="modal">&times;</button>
                     		<h4 class="modal-title">BACK UP</h4>
                    	</div>
                    	<div class="modal-body">
                     		<input type="hidden" name="delete_id" value="<?php echo $codigo; ?>">
                     		<p>¿En que puesto consideraría a <strong><?php echo $nombres; ?></strong> como back up?</p><br>
                     		<div >
                     			<input type="text" name="puestoBackup" size="60">
                     		</div>
                     	</div>

                     	<div class="modal-footer">
                        <button type="submit" name="btnEliminar" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> ENVIAR</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> CANCELAR</button>
                      </div>
                    </div>
              		</form>
                </div>
					    </div>                  	
              <!--MODAL DE EDITAR-->
              <div id="edit<?php echo $codigo; ?>" class="modal fade" role="dialog">
								<div class="modal-dialog modal-lg" >
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4 class="modal-title">EDITAR POSTULANTE</h4>
										</div>
										<div class="modal-body">
									    	<div class="container-fluid">
					                <div class="row">
					                	<?php  
					                        $lol= $codigo;
					                        $blood=ejecutarQuery("SELECT * FROM ternas WHERE codigo=$lol");
					                        $sven=mysqli_fetch_assoc($blood);
					                	?>
					                    <div class="col-sm-offset-0 col-sm-12">
					                        <div class="row">
					                            
					                                <div class="panel panel-primary">
					                                    <div class="panel-body">
					                                    	<form class="form-signin" action="../controlador/editarPostulante.php?id=<?php echo $sven['codigo'];?>" method="POST" enctype="multipart/form-data">
				                                        <div class="row form-group">
				                                        	<div class="col-md-4">
			                                                <label for="cliente">CODIGO:</label>
			                                                <input type="text" readonly class="form-control" rows="3" name="codigo" required value="<?php echo $sven['codigo'];?>">
			                                            </div>
			                                            <div class="col-md-4">
			                                                <label for="cliente">NOMBRES:</label>
			                                                <input type="text" class="form-control" rows="3" name="nombres" required value="<?php echo $sven['nombres'];?>">
			                                            </div>
			                                            <div class="col-md-4">
			                                                <label for="cliente">APELLIDOS:</label>
			                                                <input type="text" class="form-control" rows="3" name="apellidos" required value="<?php echo $sven['apellidos'];?>">
			                                            </div>
				                                        </div>
				                                        <div class="row form-group">
			                                            <div class="col-md-4">
			                                                <label for="cliente">DNI:</label>
			                                                <input type="text" class="form-control" rows="3" name="dni" required value="<?php echo $sven['dni'];?>">
			                                            </div>
			                                            <div class="col-md-4">
			                                                <label for="cliente">TELEFONO:</label>
			                                                <input type="text" class="form-control" rows="3" name="telefono" required value="<?php echo $sven['telefono'];?>">
			                                            </div>
			                                            <div class="col-md-4">
		                                                <label for="cliente">FECHA DE INICIO:</label>
		                                                <input type="date" class="form-control" rows="3" name="fechaEnvio"  value="<?php echo $sven['fechaIngreso'];?>">
			                                            </div>
			                                            <div class="col-md-12">
			                                              <label for="cliente">CV:</label>
			                                              <div class="fileinput fileinput-new input-group" data-provides="fileinput">
									                                		<div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
										                                	<span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Seleccionar CV</span><span class="fileinput-exists">Cambiar</span><input type="file" name="vc">
										                                	</span>
									                                		<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remover
									                                		</a>
								                           					</div>
			                                            </div>
				                                        </div>
				                                        <div class="row form-group">
			                                            <div class="col-md-9">
				                                                <label for="cliente">OBSERVACIONES:</label>
				                                                <textarea style="resize:vertical; min-height: 50px" type="text" class="form-control" rows="3" name="observacion"><?php echo $sven['observacion'];?></textarea>
				                                            </div>
				                                            <div class="col-md-3">
				                                            	<br>
				                                                <a class="btn btn-danger btn-block" data-dismiss="modal"">VOLVER</a>
				                                            </div>
				                                            <div class="col-md-3">
				                                            	<br>
				                                                <button class="btn btn-primary btn-block" type="submit">ACTUALIZAR</button>
				                                            </div>
				                                        </div>
				                                        </form>
					                                    </div>
					                                   
					                                </div>
					                            
					                        </div>
					                    </div>
					                </div>
									      </div>
									  </div>
									</div>
								</div>
							</div>
							<!-- Eliminar Modal -->
					    <div id="delete<?php echo $codigo; ?>" class="modal fade" role="dialog">
            		<div class="modal-dialog">
              		<form method="post" action="../controlador/eliminarPostulante.php?codigo=<?php echo $codigo; ?>">
                  	<div class="modal-content">
                    	<div class="modal-header">
                     		<button type="button" class="close" data-dismiss="modal">&times;</button>
                     		<h4 class="modal-title">ELIMINAR POSTULANTE</h4>
                    	</div>
                    	<div class="modal-body">
                     		<input type="hidden" name="delete_id" value="<?php echo $codigo; ?>">
                     		<p>Esta seguro de eliminar a <strong><?php echo $nombres; ?>?</strong></p>
                     	</div>
                     	<div class="modal-footer">
                        <button type="submit" name="btnEliminar" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> YES</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> NO</button>
                      </div>
                    </div>
              		</form>
                </div>
					    </div>
							<!-- PAGINACION -->				
							<tfoot>	
			          <?php
								}
								?>
								<tr>
			            <td colspan="5">
			              <ul class="pagination pull-right"></ul>
			            </td>
			          </tr>
         			</tfoot>
			      </table>
          </div>

        </div>
        <div class="ibox float-e-margins">
        	<div class="ibox-title">
          	<h5>NUEVO POSTULANTE</h5> <span class="label label-primary">T-S|S</span>
            <div class="ibox-tools">
		          <a class="collapse-link">
		           	<i class="fa fa-chevron-up"></i>
		          </a>
            </div>
          </div>
                                  <div class="ibox-content">
                                  	<div>
                                      <table class="footable table table-stripped toggle-arrow-tiny">
                                      	<form class="form-signin" action="../controlador/nuevopostulante.php?codigo=<?php echo $proceso; ?>" method="POST" enctype="multipart/form-data">
			                            				<thead>
			                            					<tr>
                                  						<div class="row form-group">
			                                        	<div class="col-md-3">
			                                            <label for="cliente">NOMBRES:</label>
			                                            <input type="text" class="form-control" rows="3" name="nombres" required>
			                                        	</div>
			                                        	<div class="col-md-3">
			                                            <label for="cliente">APELLIDOS:</label>
			                                            <input type="text"  class="form-control" rows="3" name="apellidos">
			                                        	</div>
		                                  					<div class="col-md-3">
					                                        	<label for="cliente">DNI:</label>
					                                            <input type="text" class="form-control" rows="3" name="dni" required>
					                                      </div>
			                                      		<div class="col-md-3">
			                                       		 <label for="cliente">TELEFONO:</label>
			                                          	<input type="text" class="form-control" rows="3" name="telefono" >
			                                      		</div>
                                  				</div>
                                  				<div class="row form-group">
                                  					<div class="col-md-9">
		                                        	<label for="cliente">CV:</label>
		                                          <div class="fileinput fileinput-new input-group" data-provides="fileinput">
				                                      	
							                                	<div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
							                                	<span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Seleccionar CV</span><span class="fileinput-exists">Cambiar</span><input type="file" name="cv"></span>
							                                	<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
					                           					</div>  
			                                      </div>
					                                      <div class="col-md-3">
			                                        	<label for="cliente">FECHA DE INICIO:</label>
			                                            <input type="date" class="form-control" rows="3" name="fechaEnvio" >
			                                      		</div>
			                                      
			                                   	</div>
			                                    <div class="row form-group">
			                                    	<div class="col-md-9">
			                                            <label for="cliente">OBSERVACIONES </label>
			                                        	<textarea name="observaciones" class="form-control"
																									style="resize:vertical; min-height: 35px">
																								</textarea>
			                                      </div>
			                                      <div class="col-md-3" align="right">
			                                      	<br>
			                                        	<br style="line-height: 10px;">
			                                            	<button  class="btn btn-primary btn-block" type="submit" style='width:130px; height:35px;'>REGISTRAR</button>
			                                      </div> 
			                                    	</div>
			                                </tr>
			                            </thead>
			                            </form>
			                       	 </table>
                                     	</div>
                                 	</div>
                  </div>
              </div>
          </div>	
      </div>
          
<?php include('footer.php'); ?>
