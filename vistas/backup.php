<?php 
	require '../controlador/funciones.php';
	require '../archivo/terna.php';
	if(! haIniciadoSesion() )
  {
   header('Location: ../index.php');
  }
  if ($_SESSION['usuario']=='admi' or $_SESSION['usuario']=='gerente' or $_SESSION['usuario']=='vanessa' or $_SESSION['usuario']=='alessandra' or $_SESSION['usuario']=='gianella' or $_SESSION['usuario']=='carmen' or $_SESSION['usuario']=='joe' or $_SESSION['usuario']=='karen') {
  include('header.php');
$conexion = new Conexion();
$cn = $conexion->getConexion();
$estado = 100;
if (isset( $_GET['estado'])) {
	$estado = $_GET['estado'];
}					
?>
	<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row ">
      <div class="col-lg-12">
        <div class="ibox float-e-margins">
          <div class="ibox-title">
            <h5>TABLA DE POSTULANTES ENVIADOS A BACKUP</h5> 
            <div class="ibox-tools">
            	<a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
              </a>
            </div>
          </div>
          <div class="col-lg-13">
          <ol class="breadcrumb">
            <li>
                &nbsp &nbsp &nbsp &nbsp T|Selección
            </li>
            <li>
              Postulantes
            </li>
            <li class="active">
                <strong>Back Up</strong>
            </li>
          </ol>
        </div>
          <div class="ibox-content">
          	<input  type="text" class="form-control input-sm m-b-xs" id="filter" style="width:400px"
                                   placeholder="Buscar en tabla">
	          	<table class="footable table table-stripped" data-page-size="10" data-filter=#filter>
				        <thead>
				         	<tr>
										<td data-toggle="true" class="text-center"><strong>CÓDIGO</strong></td>
	                  <td class="text-center"><strong>POSTULANTE</strong></td>
	                  <td class="text-center"><strong>PUESTO BACKUP</strong></td>
	                  <th data-hide="all">OBSERVACIÓN</th>
	                  <th data-hide="all">DNI</th>
	                  <th data-hide="all">TELEFONO</th>
	                  <td class="text-center"><strong>OPCIONES</strong></td>
	                </tr>
	            	</thead>
	              <?php
	                $postulante = new terna($cn);
	                $datos = $postulante -> listarbackup(); 
	                foreach ($datos as $postulante) {
	                	$codigo = 	$postulante[0];
	                	$proceso = 	$postulante[1];
	                	$apellidos = 	$postulante[2];
	                	$nombres = 	$postulante[3];
	                	$rutaCV = 	$postulante[4];
	                	$fechaEnvio = 	$postulante[5];
	                	$rrhhCliente = 	$postulante[6];
	                	$induccion = 	$postulante[7];
	                	$entrevista = 	$postulante[8];
	                	$informe = 	$postulante[9];
	                	$fechaIngreso = 	$postulante[10];
	                	$ordenServicio = 	$postulante[11];
	                	$factura = 	$postulante[12];
	                	$observacion = 	$postulante[13];
	                	$eliminado = 	$postulante[14];
	                	$terna = 	$postulante[15];
	                	$dni = 	$postulante[16];
	                	$telefono = 	$postulante[17];
	                	$puestoBackup = 	$postulante[18];
	                	$porcentaje = 	$postulante[19];
	                	$verLaboral = 	$postulante[20];
	                	$elegido = 	$postulante[21];
								?>
				        <tbody>
	                <tr>
	                  <td class="text-center"><?php echo $codigo; ?></td>
										<td class="text-center"><?php echo $nombres.' '.$apellidos; ?></td>
										<td class="text-center"><?php echo $puestoBackup;?></td>
										<td class="text-center"><?php echo $observacion;?></td>
										<td class="text-center"><?php echo $dni; ?></td>
										<td class="text-center"><?php echo $telefono; ?></td>
										<td class="text-center">
											<a >
												<button class="btn btn-primary btn-circle" style="background-color: #878787;" type="button" title="CV" onClick="window.open('<?php echo $rutaCV;?>');"><i class="fa fa-file"></i>
	                      </button>
											</a>
											<a href="#proceso<?php echo $codigo;?>" data-toggle="modal">
												<button  type="button" class="btn btn-primary btn-circle"  title="PROCESO"><span class='fa fa-check' aria-hidden='true'></span>
	                      </button>
											</a>
											<a href="#delete<?php echo $codigo;?>" data-toggle="modal">
												<button type='button' class='btn btn-danger btn-circle' title="ELIMINAR">
													<span class='glyphicon glyphicon-trash' aria-hidden='true'></span>
												</button>
											</a>
										</td>
	                </tr>
	              </tbody>
	              <!-- Proceso Modal -->
						    <div id="proceso<?php echo $codigo;?>" class="modal fade" role="dialog">
	            		<div class="modal-dialog">
	              		<form method="post" action="../controlador/enviaraproceso.php?codigo=<?php echo $codigo; ?>">
	                  	<div class="modal-content">
	                    	<div class="modal-header">
	                     		<button type="button" class="close" data-dismiss="modal">&times;</button>
	                     		<h4 class="modal-title">ENVIAR A PROCESO</h4>
	                    	</div>
	                    	<div class="modal-body">
	                     		<p>¿A que proceso desea enviar a <strong><?php echo $nombres.' '.$apellidos; ?></strong>?</p>
	                     		<br>
	                     		<label for="cliente">PROCESO:</label>
                          <select name="selectProceso" class="form-control" required style="width: 70px">
                            <?php
                                $consulta = ejecutarQuery("SELECT * FROM procesos WHERE eliminado = 0 ");
                                while ($aaa=mysqli_fetch_assoc($consulta)) {
                                	?>
                                	<OPTION VALUE="<?php echo $aaa['codigo']; ?>"><?php echo $aaa['codigo']; ?></OPTION> 
                            <?php } ?> 
                          </select></div>
	                     	<div class="modal-footer">
	                        <button type="submit" name="btnEnviar" class="btn btn-danger"><span class="glyphicon glyphicon-share"></span> ENVIAR</button>
	                        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> CANCELAR</button>
	                      </div>
	                    </div>
	              		</form>
	                </div>
						    </div>   
								<!-- Eliminar Modal -->
						    <div id="delete<?php echo $codigo; ?>" class="modal fade" role="dialog">
	            		<div class="modal-dialog">
	              		<form method="post" action="../controlador/eliminarBackup.php?codigo=<?php echo $codigo; ?>">
	                  	<div class="modal-content">
	                    	<div class="modal-header">
	                     		<button type="button" class="close" data-dismiss="modal">&times;</button>
	                     		<h4 class="modal-title">ELIMINAR POSTULANTE DE BACK UP</h4>
	                    	</div>
	                    	<div class="modal-body">
	                     		<input type="hidden" name="delete_id" value="<?php echo $codigo; ?>">
	                     		<p>Esta seguro de eliminar a <strong><?php echo $nombres.' '.$apellidos; ?>?</strong></p>
	                     	</div>
	                     	<div class="modal-footer">
	                        <button type="submit" name="btnEliminar" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> YES</button>
	                        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> NO</button>
	                      </div>
	                    </div>
	              		</form>
	                </div>
						    </div>
								<!-- PAGINACION -->				
								<tfoot>	
				          <?php
									}
									?>
									<tr>
				            <td colspan="5">
				              <ul class="pagination pull-right"></ul>
				            </td>
				          </tr>
	         			</tfoot>
				      </table>
          </div>

        </div>
          
<?php include('footer.php'); }
else { ?> <script>
    alert("NO SE TE CONCEDIO PERMISO PARA ESTA VISTA");
    window.history.go(-1);
    </script> <?php } ?>       
