<?php 
class Terna
{
	private $cn;
	public function __construct($cn)
 {
	$this->cn = $cn;
 }
	public function listarterna($idproceso)
	 { 	
		$item = array();	
		$sql = "SELECT * FROM ternas  where eliminado=0 and terna=1 and proceso=$idproceso";
	    $result = mysqli_query($this->cn,$sql);
		while($fila = mysqli_fetch_array($result)){
			$item[] = $fila;		
		}	
		return $item;
	  }
	  public function listarpostulante($idproceso)
	 { 	
		$item = array();	
		$sql = "SELECT * FROM ternas  where eliminado=0 and terna=0 and proceso=$idproceso";
	    $result = mysqli_query($this->cn,$sql);
		while($fila = mysqli_fetch_array($result)){
			$item[] = $fila;		
		}	
		return $item;
	  }
	  public function listarbackup()
	 { 	
		$item = array();	
		$sql = "SELECT * FROM ternas  where eliminado=0 and terna=2";
	    $result = mysqli_query($this->cn,$sql);
		while($fila = mysqli_fetch_array($result)){
			$item[] = $fila;		
		}	
		return $item;
	  }
}
?>