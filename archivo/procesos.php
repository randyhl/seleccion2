<?php 
class procesos
{
	private $cn;
	public function __construct($cn)
 {
	$this->cn = $cn;
 }
 public function listarprocesocodigo($codigo)
	 { 	
		$item = array();	
		$sql = "SELECT p.*, c.cargo, e.razonSocial FROM procesos p inner join perfiles c on p.perfil =c.codigo inner join clientes e on c.empresa = e.ruc where p.codigo =$codigo";
	    $result = mysqli_query($this->cn,$sql);
		while($fila = mysqli_fetch_array($result)){
			$item[] = $fila;		
		}	
		return $item;
	  }
	public function listarprocesos($estado)
	 { 	
		$item = array();	
		$sql = "SELECT p.*, c.cargo, e.razonSocial FROM procesos p inner join perfiles c on p.perfil =c.codigo inner join clientes e on c.empresa = e.ruc where p.eliminado=0 and p.estado =$estado order by fechaPedido desc";
	    $result = mysqli_query($this->cn,$sql);
		while($fila = mysqli_fetch_array($result)){
			$item[] = $fila;		
		}	
		return $item;
	  }
	  public function listarprocesosconsultores($estado,$consultor)
	 { 	
		$item = array();	
		$sql = "SELECT p.*, c.cargo, e.razonSocial FROM procesos p inner join perfiles c on p.perfil =c.codigo inner join clientes e on c.empresa = e.ruc where p.eliminado=0 and p.estado =$estado and p.consultor = '$consultor'	 order by fechaPedido desc";
	    $result = mysqli_query($this->cn,$sql);
		while($fila = mysqli_fetch_array($result)){
			$item[] = $fila;		
		}	
		return $item;
	  }
	  public function listarculminados()
	 { 	
		$item = array();	
		$sql = "SELECT p.*, c.cargo, e.razonSocial FROM procesos p inner join perfiles c on p.perfil =c.codigo inner join clientes e on c.empresa = e.ruc where p.eliminado=1 order by fechaPedido desc";
	    $result = mysqli_query($this->cn,$sql);
		while($fila = mysqli_fetch_array($result)){
			$item[] = $fila;		
		}	
		return $item;
	  }
}
?>