-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 24-11-2017 a las 14:48:48
-- Versión del servidor: 5.6.38
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `may28tso_seleccion`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `ID_Categoria` int(11) NOT NULL,
  `categoria` varchar(45) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `ruta` varchar(40) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`ID_Categoria`, `categoria`, `descripcion`, `ruta`) VALUES
(1, 'CLIENTES', 'requerimiento', 'clientes.php'),
(2, 'PERFILES', 'requerimiento', 'perfiles.php'),
(3, 'PROCESOS', 'requerimiento', 'procesos.php'),
(4, 'NUEVO CLIENTE', 'cliente', 'nuevoCliente.php'),
(5, 'VER CLIENTE', 'cliente', 'verCliente.php'),
(6, 'NUEVO PERFIL', 'perfil', 'nuevoPerfil.php'),
(7, 'VER PERFIL', 'perfil', 'verPerfil.php'),
(8, 'NUEVO PROCESO', 'proceso', 'nuevoProceso.php'),
(9, 'VER PROCESO', 'proceso', 'verProceso.php'),
(10, 'PROCESOS', 'seleccion', 'listaProcesos.php'),
(11, 'TERNA', 'terna', 'terna.php'),
(12, 'BACK UP', 'seleccion', 'backUp.php'),
(13, 'POSTULANTES', 'postulantes', 'postulantes.php'),
(14, 'PRIMAX', 'clientes', 'primax.php'),
(15, 'PRIMAX POSTULANTES', 'verPostulantes', 'verPostulantes.php'),
(16, 'PRIMAX TERNA', 'verTernas', 'verTernas.php'),
(17, 'PRIMAX REPORTE', 'reportes', 'reportePrimax.php'),
(18, 'ELEGIR', 'seleccion1', 'elegir.php'),
(19, 'CULMINADOS', 'seleccion', 'culminados.php');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `ruc` char(11) NOT NULL,
  `razonSocial` varchar(50) DEFAULT NULL,
  `contacto` varchar(50) DEFAULT NULL,
  `celular` varchar(50) DEFAULT NULL,
  `correo` varchar(50) DEFAULT NULL,
  `logo` varchar(100) DEFAULT NULL,
  `eliminado` bit(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`ruc`, `razonSocial`, `contacto`, `celular`, `correo`, `logo`, `eliminado`) VALUES
('20554545743', 'PRIMAX', 'contacto x de primax', '123456789', 'primax@primax.com', '../images/primaxLogo.png', b'1'),
('12345678', 'GMO', 'contacto gmo', '999999999', 'gmo@gmo.com', '../images/', b'1'),
('56565565655', 'Hotel Huerta Grande', 'Pavel', ' 961429689', 'pcg1980@hotmail.com', '../images/', b'0'),
('20396466768', 'HUEMURA', 'ROMY CARPENA', '969386794', 'recursoshumanos@huemura.com.pe', '../images/Logo Huemura.jpg', b'0'),
('22222222222', 'COESTI', 'LUZ CHACCHA', '56', 'g@h', '../images/', b'0'),
('12345678901', 'GASCOP', 'Raúl Negrón ', '962317533', 'raul.negron@gascop.com.pe', '../images/', b'0'),
('33333333333', 'SUNGLASS HUT', 'José Vasquez', '951300181', 'Jose.Vasquez@pe.luxottica.com', '../images/', b'0'),
('20516390060', 'CLASEM S.A.C', 'DELY RIOS', '981134192', 'dely.rios@esparq.com', '../images/', b'0'),
('20482204440', 'T - Soluciona', 'ROBERTO PEREZ', '970009124', 'rperez@t-soluciona.com.pe', '../images/', b'0'),
('-', 'EULEN', 'José Caycho Soto', '944891410', 'jcaycho@eulen.com', '../images/', b'0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datospersonales`
--

CREATE TABLE `datospersonales` (
  `usuario` varchar(45) NOT NULL,
  `nombre` varchar(65) DEFAULT NULL,
  `apellidos` varchar(65) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `datospersonales`
--

INSERT INTO `datospersonales` (`usuario`, `nombre`, `apellidos`, `email`) VALUES
('admi', 'Willy', 'Arias Capcha', 'sistemas@t-soluciona.com.pe');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamentos`
--

CREATE TABLE `departamentos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `departamentos`
--

INSERT INTO `departamentos` (`id`, `nombre`) VALUES
(1, 'ANCASH'),
(2, 'AREQUIPA'),
(3, 'CAJAMARCA'),
(4, 'CUSCO'),
(5, 'ICA'),
(6, 'JUNIN'),
(7, 'LA LIBERTAD'),
(8, 'LAMBAYEQUE'),
(9, 'LIMA CENTRO'),
(10, 'LIMA NORTE'),
(11, 'LIMA SUR'),
(12, 'PIURA'),
(13, 'TACNA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grifos`
--

CREATE TABLE `grifos` (
  `id` int(11) NOT NULL,
  `idDepartamento` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `grifos`
--

INSERT INTO `grifos` (`id`, `idDepartamento`, `nombre`) VALUES
(1, 1, 'SALVADOR'),
(2, 2, 'ANGELITO'),
(3, 2, 'CERRO COLORADO'),
(4, 2, 'CHARACATO'),
(5, 2, 'DOLORES'),
(6, 2, 'DON ROLO'),
(7, 2, 'FORTALEZA'),
(8, 2, 'GOYENECHE'),
(9, 2, 'GUARDIA CIVIL I'),
(10, 2, 'GUARDIA CIVIL II'),
(11, 2, 'INDEPENDENCIA'),
(12, 2, 'ITALIA'),
(13, 2, 'KENNEDY'),
(14, 2, 'LA 48'),
(15, 2, 'LA JOYA'),
(16, 2, 'LANIFICIO'),
(17, 2, 'MOLLENDO'),
(18, 2, 'MONTERREY'),
(19, 2, 'PARRA'),
(20, 2, 'RESERVORIO'),
(21, 2, 'SACHACA'),
(22, 2, 'SAN JUAN'),
(23, 2, 'SANTA MONICA'),
(24, 2, 'SAO PAULO'),
(25, 2, 'SERVIMAX'),
(26, 2, 'SOCABAYA'),
(27, 2, 'SUR PERU'),
(28, 2, 'TENIENTE PALACIOS'),
(29, 2, 'VENEZUELA'),
(30, 2, 'VILLA DEL SUR'),
(31, 2, 'VIRGEN DEL ROSARIO'),
(32, 2, 'YANAHUARA'),
(33, 3, 'BAÑOS DEL INCA'),
(34, 3, 'LOS ANGELES'),
(35, 3, 'LISTO G&N'),
(36, 4, 'AEROPUERTO'),
(37, 4, 'OVALO'),
(38, 4, 'SUPERSERVICENTRO'),
(39, 5, 'CHINCHA BAJA'),
(40, 5, 'ESTRELLA'),
(41, 5, 'NASCA'),
(42, 5, 'SANTIAGO'),
(43, 5, 'SEÑOR DE LUREN'),
(44, 5, 'TIZON'),
(45, 6, 'JUNIN'),
(46, 6, 'MARCAVALLE'),
(47, 6, 'SANTA ISABEL'),
(48, 6, 'TARMA'),
(49, 7, 'AMERICA NORTE'),
(50, 7, 'BALDI'),
(51, 7, 'ESPAÑA'),
(52, 7, 'LA PERLA'),
(53, 7, 'LARCO'),
(54, 7, 'LOS POSTRES'),
(55, 7, 'MOCHE'),
(56, 7, 'PACASMAYO'),
(57, 7, 'SANTO DOMINGUITO'),
(58, 7, 'UNION'),
(59, 7, 'LISTO CALIFORNIA'),
(60, 7, 'LISTO MOCHICA'),
(61, 8, 'BALTA'),
(62, 8, 'CHICLAYO'),
(63, 8, 'DIVINO NIÑO'),
(64, 8, 'GASCOP'),
(65, 8, 'JUAN TOMIS'),
(66, 8, 'LA PURISIMA'),
(67, 8, 'LAS BRISAS'),
(68, 8, 'MEXICO'),
(69, 8, 'MOCHUMI'),
(70, 8, 'NOVO GAS'),
(71, 8, 'PALMERAS'),
(72, 8, 'PICSI'),
(73, 8, 'PIMENTEL'),
(74, 8, 'QUIOLA'),
(75, 8, 'SANTA ELENA'),
(76, 8, 'SANTA VICTORIA'),
(77, 8, 'SANTO TORIBIO'),
(78, 8, 'LISTO COSTA GAS'),
(79, 9, '28 DE JULIO'),
(80, 9, 'AREQUIPA'),
(81, 9, 'ARRIOLA'),
(82, 9, 'ATE'),
(83, 9, 'BAHIA'),
(84, 9, 'CANADA'),
(85, 9, 'CORCONA'),
(86, 9, 'FELVERANA'),
(87, 9, 'FERRERO'),
(88, 9, 'FRUTALES'),
(89, 9, 'HIPODROMO'),
(90, 9, 'JAVIER PRADO'),
(91, 9, 'LA MOLINA'),
(92, 9, 'MONTERRICO'),
(93, 9, 'ÑAÑA'),
(94, 9, 'REPUBLICA'),
(95, 9, 'SAN LUIS'),
(96, 9, 'SAN LUIS II'),
(97, 9, 'UNIVERSIDAD'),
(98, 9, 'VALLE HERMOSO'),
(99, 9, 'LISTO CAMACHO'),
(100, 9, 'LISTO CHACARILLA'),
(101, 9, 'LISTO FONTANA'),
(102, 9, 'MONTERREY'),
(103, 9, 'LISTO PARODI'),
(104, 9, 'LISTO RINCONADA'),
(105, 10, 'ARGENTINA'),
(106, 10, 'AULY'),
(107, 10, 'BRASIL'),
(108, 10, 'CALLE DERECHA'),
(109, 10, 'CHANCAY'),
(110, 10, 'COLLIQUE'),
(111, 10, 'DUEÑAS'),
(112, 10, 'EL CARMELO'),
(113, 10, 'HUACHO'),
(114, 10, 'HUARAL'),
(115, 10, 'HUIRACOCHA'),
(116, 10, 'IGARSA'),
(117, 10, 'LA MARINA'),
(118, 10, 'LA PAZ'),
(119, 10, 'PANDO'),
(120, 10, 'PERSHING'),
(121, 10, 'QUILCA'),
(122, 10, 'SALAVERRY'),
(123, 10, 'SAN JOSE'),
(124, 10, 'SUDAMERICANO'),
(125, 10, 'SUPE'),
(126, 10, 'TINGO MARIA'),
(127, 10, 'ZARATE'),
(128, 10, 'ZORRITOS'),
(129, 10, 'LISTO ELIO'),
(130, 10, 'LISTO MARBELLA'),
(131, 10, 'LISTO PETIT THOUARS'),
(132, 10, 'LISTO PLAZA NORTE'),
(133, 10, 'LISTO TRAPICHE'),
(134, 11, 'ARMENDARIZ'),
(135, 11, 'BENAVIDES'),
(136, 11, 'CHAMA'),
(137, 11, 'EL CHORRILLANO'),
(138, 11, 'EL ROSARIO'),
(139, 11, 'ESCOSA'),
(140, 11, 'FERRARI'),
(141, 11, 'FLORA TRISTAN'),
(142, 11, 'GRANADA'),
(143, 11, 'LA ENCANTADA'),
(144, 11, 'LOS CASTAÑOS'),
(145, 11, 'MONTREAL'),
(146, 11, 'PANSUR'),
(147, 11, 'SAN ANTONIO'),
(148, 11, 'SAN CARLOS'),
(149, 11, 'TAVIRSA'),
(150, 11, 'LISTO BENAVIDES 2'),
(151, 11, 'LISTO BOLOGNESI'),
(152, 11, 'LISTO GASSURCO'),
(153, 11, 'LISTO HIGUERETA'),
(154, 11, 'LISTO LARCO 2'),
(155, 11, 'LISTO PACHACUTEC'),
(156, 11, 'LISTO PARDO'),
(157, 11, 'LISTO PRIMAVERA'),
(158, 11, 'LISTO RICARDO PALMA'),
(159, 11, 'LISTO SHELL'),
(160, 12, 'CHICLAYITO'),
(161, 12, 'DONALD'),
(162, 12, 'GASCOP'),
(163, 12, 'GRAU'),
(164, 12, 'LUKAS'),
(165, 12, 'MACARENA'),
(166, 12, 'MEGA'),
(167, 12, 'PERU'),
(168, 12, 'PIURA'),
(169, 12, 'SULLANA'),
(170, 12, 'TALARA'),
(171, 13, 'AV. MUNICIPAL');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfiles`
--

CREATE TABLE `perfiles` (
  `codigo` int(11) NOT NULL,
  `empresa` char(11) DEFAULT NULL,
  `cargo` varchar(50) NOT NULL,
  `gradoInstruccion` varchar(100) NOT NULL,
  `estadoCarrera` varchar(50) NOT NULL,
  `carrera` varchar(100) NOT NULL,
  `diplomados` varchar(50) DEFAULT NULL,
  `postgrado` varchar(50) DEFAULT NULL,
  `idioma` varchar(50) DEFAULT NULL,
  `estadoIdioma` varchar(50) DEFAULT NULL,
  `programasInformaticos` varchar(100) DEFAULT NULL,
  `nivelProgramas` varchar(50) DEFAULT NULL,
  `experienciaRequerida` varchar(100) DEFAULT NULL,
  `tiempoExperiencia` varchar(50) DEFAULT NULL,
  `otrasCaracteristicas` text,
  `funciones` text,
  `sexo` varchar(50) DEFAULT NULL,
  `edadMin` int(11) DEFAULT NULL,
  `edadMax` int(11) DEFAULT NULL,
  `direccion` text,
  `horarioInicio` time DEFAULT NULL,
  `horarioFin` time DEFAULT NULL,
  `remuneracion` int(11) DEFAULT NULL,
  `planilla` bit(1) DEFAULT NULL,
  `remuneracionDetalle` text,
  `viajar` bit(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `perfiles`
--

INSERT INTO `perfiles` (`codigo`, `empresa`, `cargo`, `gradoInstruccion`, `estadoCarrera`, `carrera`, `diplomados`, `postgrado`, `idioma`, `estadoIdioma`, `programasInformaticos`, `nivelProgramas`, `experienciaRequerida`, `tiempoExperiencia`, `otrasCaracteristicas`, `funciones`, `sexo`, `edadMin`, `edadMax`, `direccion`, `horarioInicio`, `horarioFin`, `remuneracion`, `planilla`, `remuneracionDetalle`, `viajar`) VALUES
(1, '20554545743', 'GRIFEROS DE PLAYA', 'Secundaria', 'Ultimos Ciclos', '-', '-', '-', '-', 'Básico', 'Deseable Excel', 'Básico', 'En atencion al cliente', '6 meses', 'Va tener que estar de pie durante todo las horas de trabajo', '- Abastecer de combustible a los clientes', '', 19, 50, 'maxima deuda permitida: 500', '07:00:00', '15:00:00', 850, b'1', 'Se paga el basico + bonos + beneficios de leuy', b'1'),
(2, '56565565655', 'Analista de Marketing Junior', 'Técnico o Universitario', 'Ultimos Ciclos', 'Marketing, Administración y Marketing, Ciencias de la Comunicación', '-', '-', 'No necesario', 'Básico', 'Microsoft Word, Microsoft Excel, Microsoft Power Point', 'Básico', 'Plan y Presupuesto Anual de Marketing, Campañas de Marketing, Plan de Ventas', '1 año', 'Ingreso a planilla desde el 2do. mes.\r\nVacaciones de 15 días.\r\nOfrece línea de carrera.\r\nHorario (flexible en caso de estudios).', '- Proponer, desarrollar y administrar el Plan y Presupuesto anual de Marketing.\r\n- Planificar, supervisar y evaluar las campañas y medios de comunicación.\r\n- Implementar campañas de comunicación y ventas con los clientes.\r\n- Mantener base de datos de clientes.\r\n- Supervisar redacción creativa para contenido para medios impresos y redes sociales.\r\n- Manejo operativo y coordinación de material promocional con los medios de comunicación.\r\n- Proponer y ejecutar las políticas comerciales orientadas a lograr una mejor posición en el mercado.\r\n- Definir y proponer el plan de ventas de la empresa de manera periódica.', 'Femenino', 23, 30, 'Pérdida hasta 10,000 soles', '08:00:00', '14:00:00', 900, b'1', 'Puede llegar a subir de sueldo', b'1'),
(3, '20396466768', 'Ejecutivo de Ventas de Campo', 'Secundaria', 'Egresado', 'Ventas y Marketing, Administración y/o Ing. Industrial', '-', '-', 'No necesario', 'Básico', 'Microsoft Word, Microsoft Excel, Microsoft Power Point, Open Office', 'Avanzado', 'Ventas de Campo en rubro Ferretero', '2 años', 'Horario de trabajo: Lunes a sábado.\r\nManejo de Cartera de Clientes actual.\r\nDisponibilidad para viajar al norte y oriente.', '- Trabajo de campo (90%) y administrativo (10%).\r\n- Visitar a los diferentes clientes y negocios de la región.\r\n- Cumplir los objetivos de Ventas, Volumen de ventas, mix de productos y cobertura de su territorio.\r\n- Desarrollo portafolio y manejo de indicadores de ventas.\r\n- Trámite documentario de los comprobantes de pago.\r\n- Realizar los reportes de cobranzas diarias.', 'Indistinto', 25, 40, 'Pérdida hasta 1,000 soles', '08:30:00', '18:20:00', 850, b'1', 'Por los 3 primeros meses recibe sólo 1,500 soles. A partir del 4to. mes, su remuneración en 850 soles (fijo) + Comisiones (puede llegar hasta 3,000 o 3,500)', b'1'),
(4, '22222222222', 'Asistente Administrativo', 'Técnico o Universitario', 'Egresado', 'Administración- Contabilidad- Ing. Industrial o afines', 'Seguridad y Salud Ocupacional', '-', 'Ninguno', 'Básico', 'Excell', 'Intermedio', 'Asistente Administrativo o Logístico', '1 año', 'Beneficios de acuerdo a Ley.', '- Supervisar las actividades del personal.\r\n-Realizar inventarios.\r\n-Coordinar con proveedores, etc.', 'Indistinto', 22, 29, '1200', '07:00:00', '18:00:00', 1200, b'1', '1200 a 1500.', b'1'),
(5, '12345678901', 'Técnico Mecánico de Mantenimiento de Motor y Compr', 'Técnico', 'Egresado', 'Técnico de mantenimiento, Técnico automotriz.', '-', '-', '-', 'Básico', '-', 'Básico', 'Mantenimiento preventivo y correctivo de Motor y Compresor a Gas.', '02 años', 'Contrato de 06 meses renovable, según desempeño. Lugar de trabajo en El Alto - Talara. Horario: de Lunes a Sábado. Necesariamente personal que resida en la ciudad de Talara ( NO ENACE)  o El Alto o Negritos.', '- Revisar diariamente las instalaciones a su cargo.\r\n- Realizar mantenimiento preventivo y correctivo de Motor y Compresor a Gas.\r\n- Realizar el montaje y desmontaje de los componentes de sistema de refrigeración.\r\n. Diagnosticar las fallas las causas de fallas.\r\n- Mantener con orden y limpieza en todas las instalaciones.\r\n- Realizar el llenado y cierre de la OT.\r\n- Cumplir con los estándares de calidad y seguridad.\r\n', 'Masculino', 24, 45, 'Deudas financieras pagando con regularidad.', '07:00:00', '17:00:00', 3000, b'1', 'Remuneración sujeto a descuento por beneficios.', b'1'),
(6, '12345678901', 'Auxiliar Logístico', 'Técnico', 'Egresado', 'Electricidad, Instrumentación Industrial, Mecánico de mantenimiento, mecánico de producción o simila', '-', '-', '-', 'Básico', '-', 'Básico', 'En gestión de almacén.', '02 años', 'Contrato por 03 meses renovable, según desempeño. Indispensable con licencia de conducir A1. Lugar de trabajo: EL Alto - Talaba. Indispensable residir en Talara, Negritos, El Alto ( Menos ENACE).', '•	De mantener una permanente comunicación con el supervisor  de Planta y el auxiliar de compras para desarrollar sus planes de acción.\r\n•	Realizar tareas de apoyo a las labores de operación de los equipos sólo cuando el supervisor de planta lo solicite.\r\n•	Conocer la operación de los compresores, de los grupos electrógenos, de los tableros eléctricos y otros equipos secundarios.\r\n•	Informar sobre fallas/fugas o defectos que puedan tener los equipos \r\n•	Mantener organizado los movimientos del almacén así como mantener un stock adecuado dentro del mismo\r\n•	Recepcionar la mercadería y actualizar mediante registro informático del bien\r\n•	Proporcionar materiales y suministros, mediante solicitudes autorizadas, a los técnicos que los requieran\r\n•	Mantener las líneas de producción ampliamente abastecidos de materiales directos ó indirectos (repuestos) y de todos los elementos necesarios para un flujo continuo de trabajo.\r\n•	Mantener el almacén limpio y en orden, teniendo un lugar para cada cosa y manteniendo cada cosa en su lugar, es decir, en los lugares destinados según los sistemas aprobados para clasificación y facilidad de inventarios.\r\n•	Custodiar fielmente todo lo que se le ha dado a guardar, tanto su cantidad como su buen estado\r\n•	Realizar los movimientos de recibo, almacenamiento y despacho con el mínimo de tiempo y costo posible\r\n•	Ejecutar las tareas de puesta en marcha y parada de los equipos cuando sea necesario sólo cuando el supervisor lo solicite\r\n•	Llevar un control de llegada/salida de las cisternas así como control de los m3 consumidos sólo cuando el supervisor lo solicite\r\n•	Realizar tareas de traslado de bienes, materiales o insumos desde  proveedores a Almacen  o viceversa\r\n•	Participar en los inventarios mensuales y auditorias.\r\n•	Realizar las tareas de conexión y desconexión de mangueras en los cambios de cisternas  sólo cuando el supervisor lo solicite\r\n•	Del cumplimiento de estándares de calidad y seguridad\r\n', 'Masculino', 25, 35, 'Deudas financieras pagando con regularidad. ', '07:00:00', '17:00:00', 1400, b'1', '-', b'1'),
(7, '33333333333', 'STORE MANAGER', 'Universitario', '', 'ADMINISTRACIÓN, MARKETING O AFINES', '-', '-', 'INGLES', 'Básico', 'EXCEL', 'Básico', 'EN EL MISMO CARGO, ADMINISTRANDO TIENDAS RETAIL', '01', 'Horarios rotativos, de acuerdo a la apertura o cierre de la tienda comercial 10 u 11 am. – 10 pm aproximadamente.', '- Velar por el cumplimiento de normas y disposiciones definidas por la organización.\r\n- Verificar que la tienda cuente con el equipo e insumos necesarios.\r\n-	Asegurar la implementación oportuna en la tienda de las promociones o campañas definidas.\r\n-	Supervisar el desarrollo eficiente de las labores de personal de tienda.\r\n-	Asegurar el cumplimiento de las cuotas de venta.\r\n', 'Femenino', 25, 39, 'NORMAL', '10:00:00', '22:00:00', 2200, b'1', '-	Sueldo Fijo + Comisiones + Bonos', b'1'),
(8, '22222222222', 'ADMINISTRADOR DE ESTACION', 'Universitario', 'Egresado', 'ADMINISTRACIÓN, ING. INDUSTRIAL, CONTABILIDAD', '-', '-', '-', 'Básico', '-', 'Básico', 'EN JEFATURAS', '02', '-', 'ADMINISTRAR, PLANIFICAR, CONTROLAR, ORGANIZACIÓN ESTACIÓN DE SERVICIO', 'Indistinto', 26, 37, 'NO DEBE ESTAR EN PERDIDA', '08:00:00', '06:00:00', 2500, b'1', '2500 MAS BENEFICIOS SOCIALES', b'0'),
(9, '20516390060', 'EJECUTIVO COMERCIAL INMOBILIARIO', 'Técnico', 'Ultimos Ciclos', 'ADMINISTRACIÓN O AFINES', '', '', '', 'Básico', 'COMPUTACIÓN', 'Básico', 'SI', '1 AÑO', 'SE OFRECE:\r\n• Ingreso a Planilla con todos los beneficios de Ley.\r\nHorarios: Lunes a Viernes de 9:00 am a 6:00 pm \r\n                Sábados o domingos (rotativos con sus compañeros)\r\n', '•	Prospectar clientes \r\n•	Llevar registros de visitas recibidas\r\n•	Realizar el incremento de la cartera de clientes \r\n•	Incentivas a la compra de lotes.\r\n•	Comunicar adecuadamente acerca del proyecto, sus ventajas y formas de pago \r\n•	Cierre de ventas \r\n', 'Indistinto', 22, 34, 'SI', '16:30:00', '12:00:00', 850, b'1', '850 MAS COMISIONES', b'0'),
(10, '20482204440', 'Ps. Free Lance', 'Universitario', 'Bachiller', 'Psicología', '-', '', '-', 'Básico', 'EXCEL', 'Básico', '01 año', '01 año', '-', '-  Reclutar personal usando los diferentes medios (Bolsas \r\n   de trabajo virtuales, bolsas de trabajo  de \r\n   municipalidades, avisos en el periódico, trabajo de campo,  \r\n   etc.)\r\n- Entrevistar y evaluar a los candidatos aplicando \r\n  assessment center, entrevistas por competencias, batería \r\n de pruebas psicológicas (psicométricas como proyectivas).\r\n- Realizar la evaluación del Comportamiento, evaluación del desempeño y habilidades \r\n- Cumplir con todas las etapas y documentos del proceso de Selección de Personal.\r\n- Apoyar en la realización de otras tareas que su jefe inmediato requiera', 'Femenino', 24, 35, '-', '08:00:00', '18:00:00', 50, b'0', 'El pago por cada ingreso ( candidato que haya firmado contrato) es de S/. 50.0. Se tomará en cuenta aquellos que estén trabajando como mínimo 1 semana. El pago es al cierre de cada mes. Por último, cada ingreso tiene un asegurable de 45 días, de no pasar los días en mención, deberá reponer a dicho candidato en el siguiente requerimiento', b'0'),
(11, '12345678901', 'Coordinador de Operaciones', 'Universitario', 'Titulado', 'Ing. Industrial', '', '', 'Ingles', 'Básico', 'Microsoft Office', 'Básico', '1 año  de experiencia en mantenimiento y 1 año de exp en Seguridad en Estaciones de compresión, refi', '02 años', 'Habilidades:\r\n•	Dirección y toma de decisiones\r\n•	Manejo de equipos e instrumentos de medición\r\n\r\nActitudes:\r\n•	Debe ser analítico, proactivo, motivado por aprender, desarrollarse y enfrentar mayores desafíos.\r\n•	Manejo de personal y desarrollo a colaboradores \r\n•	Manejo de muy buen lenguaje oral y corporal \r\n\r\nAsignado a la organización a tiempo completo (100%), ingresa a planilla de GASCOP con contrato de 06 meses renovable según desempeño, Sueldo básico entre S/. 3 000  nuevos soles mensuales. El lugar de trabajo será en Chiclayo,  con viajes programados a sedes de Talara,  Piura y Lambayeque( Cayanca, Motupe). Sexo Masculino.  Profesional que resida en la ciudad de Chiclayo.', '1)      Administrar los recursos humanos, financieros (caja chica) y materiales que le sean asignados para el cumplimiento de la planificación de Gerencia de Operación y Gerencia Comercial  y las funciones y procesos que le son propios.\r\n2)       Mantener las mejores relaciones laborales con todo el personal bajo su cargo velando por el cumplimiento del Reglamento Interno  Seguridad y Salud en el trabajo así como el cumplimiento del Reglamento interno de trabajo y del Código de ética, obteniendo el mayor compromiso de dicho personal con la empresa.\r\n3)      Establecer  la matriz de riesgos de los procesos operativos\r\n4)      Proponer los planes de mitigación de los riesgos que afectan a los procesos de la organización\r\n5)      Verificar la programación de cambios de cisternas en los clientes.\r\n6)      Evaluar la funcionalidad del programa de mantenimiento de equipos y hacer las actualizaciones necesarias.\r\n7)      Realizar la emisión y seguimiento al cumplimiento de las órdenes de trabajo\r\n8)      Cumplir con las tareas específicas que les asigna, Manual de Procedimientos de Mantenimiento del  sistema de gestión de mantenimiento y los procedimientos internos relacionados con las actividades de la\r\nGerencia de Operaciones, proponiendo la actualización de estos documentos cuando sea requerido.\r\n9)      Consolidad y Revisar los resultados de indicadores, cumplimiento de metas y evaluar la eficacia y oportunidad de las acciones determinadas.\r\n10)   Lograr los niveles de eficiencia productiva que permitan, entregar los productos y servicios en la oportunidad y calidad acordados con los clientes y dentro de los costos establecidos\r\n11)   Evaluar la incorporación de nuevas tecnologías en todos los ámbitos de la Gerencia de Operaciones, propiciando un ambiente adecuado para la innovación y desarrollo.\r\n\r\n\r\n\r\n', 'Masculino', 27, 40, 'Ninguna deuda en pérdida', '08:00:00', '18:00:00', 3000, b'1', 'S/ 3000 mas todos los beneficios de acuerdo a ley ( Gratificación, cts, vacaciones, seguro, excepto utilidades).', b'1'),
(12, '12345678901', 'Técnico de Mantenimiento Nivel I - Piura', 'Técnico', 'Egresado', 'Electricidad, Instrumentación Industrial.', '', '', 'Ingles', 'Básico', 'Microsoft Office', 'Básico', 'En tareas de mantenimiento de equipos electrógenos y conocimiento de motores y deseable conocimiento', '02 años', 'Horarios tentativos:\r\n* Lun a Vie de 8 a.m a 5 p.m y Sab de 5 a.m a 1 p.m\r\n* Lun a Sab de 5 a.m a 1 p.m\r\n* Lunes a Sabado de 1 p.m a 9 p.m\r\n- Importante resaltar que su día de descanso va a variar entre sab y dom', '•	Revisar diariamente las instalaciones que tiene a su cargo.\r\n•	Ejecutar las tareas de mantenimiento preventivo y correctivos que se indiquen que figuran en las OT\r\n•	Realizar seguimiento a las tareas de mantenimiento preventivo y correctivo.\r\n•	Realizar el llenado y cierre de  las OT al finalizar la intervención de mantenimiento.\r\n•	Organizar y coordinar los programas y las acciones de mantenimiento correctivo y preventivo que sean necesarias, de acuerdo a las instrucciones del encargado de mantenimiento incluyendo presentación de informes siempre y cuando sean requeridos.\r\n•	Administrar las tareas de mantenimiento actualizando dichos trabajos haciendo uso del software de Mantenimiento.\r\n•	Proponer las acciones correctivas oportunas y organizar las intervenciones \r\n•	Proponer mejoras en los métodos de trabajo\r\n•	Cumplimiento de estándares definidos por el área de mantenimiento \r\n•	De mantener orden y limpieza en todas las instalaciones \r\n•	Del cumplimiento de estándares de calidad y seguridad\r\n', 'Masculino', 25, 39, 'Ninguna deuda en pérdida', '08:00:00', '17:00:00', 2000, b'1', 'Asignado a la organización a tiempo completo (100%), ingresa a planilla de GASCOP con contrato de 06 meses renovable según desempeño, Sueldo de S/. 2000 bruto (Con beneficios de acuerdo a ley excepto utilidades). El lugar en la ciudad de Piura. Necesariamente tiene que ser personal que resida en el distrito de Piura ó Distrito de Castilla ó Distrito 26 de Octubre (No serán considerados candidatos que residan en otros distritos)', b'0'),
(13, '33333333333', 'Asesor de Modas Part Time', 'Técnico', 'Ultimos Ciclos', 'Indistinta', '', '', 'Ingles', 'Básico', 'Microsoft Office', 'Básico', 'En atención al cliente y/o ventas.', '06 meses', 'Linea de Carrera + Premios + Bonos e incentivos + Contrato de 1 año', '- Cumplir con los protocolos de atención al cliente (de acuerdo a la política de la organización).\r\n- Informar al cliente sobre las características y beneficios del producto.\r\n- Ofrecer las promociones o campañas vigentes.\r\n- Apoyar con la limpieza y el mantenimiento de la tienda.\r\n- Otras funciones que requiera su jefe inmediato', 'Indistinto', 18, 29, 'Que no excedan los S/. 900 soles en pérdida. Si es muy bueno. Deseable registrar deudas con normalidad.', '10:00:00', '22:00:00', 425, b'1', 'S/.425 fijo + Bonos  por meta individual (S/.100 )', b'0'),
(14, '56565565655', 'RECEPCIONISTA', 'Secundaria', 'Ultimos Ciclos', 'Administración o carreras afines', '', '', 'ninguno', 'Básico', 'Microsoft excel y Microsoft Word', 'Básico', '3 meses en ', '3', '-Flexibilidad en el día de descanso entre martes a jueves.\r\n-Ingresa a Planilla al tercer mes.', '- Atender a los clientes desde su llegada y realizar un seguimiento durante su estadía en el hotel.\r\n-Gestionar las reservas de los huéspedes y llevar un registro.\r\n-Atender las sugerencias y quejas de huéspedes en el Hotel.\r\n-Recibir las llamadas y brindar información solicitada.\r\n-Llevar el control de entrada/salida de los huéspedes.\r\n-Realizar cualquier otra tarea asignada por el jefe inmediato.\r\n', 'Femenino', 20, 30, 'Sin deudas  ', '08:00:00', '08:00:00', 800, b'1', '800 + COMISIÓN', b'0'),
(15, '56565565655', 'Lavandera', 'Secundaria', 'Ultimos Ciclos', 'ninguno', '', '-', 'ninguno', 'Básico', 'ninguno', 'Básico', '3 meses en lavado a mano.', '3', '-Trabajo de medio turno.\r\n-Si en antecedentes presenta juicio por alimentos si se acepta.', '-Experiencia en lavado de sábanas a mano. \r\n-Tener 3 meses de experiencia en el rubro.\r\n-Disponibilidad para trabajar en turno de mañana.\r\n-Secundaria completa.\r\n', 'Femenino', 20, 35, 'Sin deudas  ', '07:00:00', '01:00:00', 700, b'1', 'Ingresa a Planilla a partir del tercer mes.', b'1'),
(16, '33333333333', 'Asesor de Modas Full Time', 'Técnico o Universitario', 'Ultimos Ciclos', '-', '-', '-', '-', 'Básico', 'Microsoft Office', 'Básico', 'En ventas y/o atención al cliente.', '06 meses', '- Horarios rotativos.\r\n- Contrato mínimo por un año .', '-', 'Indistinto', 18, 30, 'Hasta 900 en pérdida y 2000 abonando con normalidad', '10:00:00', '22:00:00', 850, b'1', '- Salario básico más bonos por meta individual y meta de tienda, con todos los beneficios de acuerdo a ley.', b'0'),
(17, '20482204440', 'Analista de Selección', 'Universitario', 'Titulado', 'Psicología', '', '', 'ninguno', 'Básico', 'Microsoft excel y Microsoft Word', 'Básico', '1 año en Procesos de Reclutamiento y Selección', '1', 'Requisitos:\r\n- Egresados de la carrera de Psicología.\r\n- Experiencia en Selección de Personal. \r\n\r\nBeneficios:\r\n-Sueldo Fijo+Bono por Objetivos.\r\n-Ingreso a Planilla.\r\n-Grato Ambiente Laboral.\r\n\r\n', 'Funciones:\r\n-  Reclutar personal usando los diferentes medios (Bolsas de trabajo virtuales, bolsas de trabajo de \r\n   municipalidades, avisos en el periódico, trabajo de campo, etc.)\r\n- Entrevistar y evaluar a los candidatos aplicando assessment center, entrevistas por competencias, batería \r\n de pruebas psicológicas (psicométricas como proyectivas).\r\n- Realizar la evaluación del Comportamiento, evaluación del desempeño y habilidades. \r\n- Cumplir con todas las etapas y documentos del proceso de Selección de Personal.\r\n- Apoyar en la realización de otras tareas que su jefe inmediato requiera.\r\n\r\n\r\n\r\n', 'Indistinto', 24, 40, 'ninguno', '08:30:00', '06:00:00', 1500, b'1', 'Básico + Bono', b'0'),
(18, '20516390060', 'Jefe de Ventas', 'Universitario', 'Ultimos Ciclos', '-', '', '', 'Inglés', 'Básico', '-', 'Básico', 'EXPERIENCIA EN VENTAS 1 AÑO + EXPERIENCIA CON PERSONAL A CARGO', '1 año', 'Horario de trabajo: De Lun. a Dom con un día de descanso a la semana.', '- Coordinar y aumentar el porcentaje de ventas en función del plan estratégico  organizacional.\r\n-Planteamiento de objetivos a los supervisores y velar por el cumplimiento de los mismos.\r\n-Asegurar el cumplimiento de las normas de calidad y seguridad establecidas para el servicio.\r\n-Potenciar las habilidades del equipo, facilitando condiciones que propicien un buen clima laboral; aplicando coaching y programas de motivación y desarrollo.\r\n- Realizar y revisar el seguimiento del correcto pago de comisiones y bonos del personal a su cargo.\r\n-Realizar reportes de indicadores de gestión.\r\n- Formular estrategias para la captación de nuevos clientes.\r\n-Otras funciones que le asigne su jefe inmediato.', 'Femenino', 30, 45, 'Ninguna deuda en pérdida.', '09:00:00', '18:00:00', 2000, b'1', 'S/. 2000 brutos más comisiones por ventas (S7.190 C/ comisión)', b'0'),
(19, '12345678901', 'Técnico Operador de Producción Nivel I', 'Técnico', 'Egresado', 'Técnico egresado de instituto de prestigio de las carreras: Electricidad, Instrumentación Industrial', '', '', 'Inglés', 'Básico', 'Microsoft Office', 'Básico', '-', '-', 'El lugar de trabajo será en campo: Lote II altura del km 1122.3 de la carretera Panamericana Norte (Talara – El Alto). Necesariamente tiene que ser personal que resida en  Talara, Negritos o El Alto.', 'supervisor de mantenimiento  para desarrollar sus planes de acción.\r\n•	Realizar tareas de apoyo a las labores de mantenimiento de los equipos sólo cuando el responsable del mantenimiento lo solicite.\r\n•	La operación de los compresores, de los grupos electrógenos, de los tableros eléctricos y otros equipos secundarios.\r\n•	Informar sobre fallas/fugas o defectos que puedan tener los equipos al responsable del mantenimiento\r\n•	Cumplir estrictamente con los programas y las rutinas implementadas\r\n•	Ejecutar las tareas de puesta en marcha y parada de los equipos cuando sea necesario\r\n•	Llevar un control de llegada/salida de las cisternas así como control de los m3 consumidos.\r\n•	Realizar las tareas de conexión y desconexión de mangueras en los cambios de cisternas \r\n•	De mantener orden y limpieza en todas las instalaciones \r\n•	Del cumplimiento de estándares de calidad y seguridad\r\n', 'Masculino', 24, 35, 'Ninguna deuda en pérdida', '08:00:00', '18:00:00', 1300, b'1', 'Sueldo básico 1300  nuevos soles/mensuales', b'0'),
(20, '20396466768', 'Supervisor de Almacén ', 'Técnico o Universitario', 'Egresado', 'ninguno', '', '', 'ninguno', 'Básico', 'Microsoft excel y Microsoft Word', 'Básico', '1 AÑO EN ALMACÉN', '1', 'Las competencias que tiene que tener los candidatos son los siguientes:\r\n   - Liderazgo.\r\n   - Comunicación Efectiva\r\n   - Trabajo en equipo\r\n   - Productividad\r\n   - Ambiente motivador \r\n   - Manejo de conflictos.\r\n', '•	Verificar la documentación proveniente de los proveedores durante la recepción de los productos y realizar las respectivas devoluciones cuando se presente el caso. \r\n•	Elaborar notas de recepción, Control físico y reporte diario del stock.\r\n•	Supervisar la carga de despachos a los clientes.\r\n•	Certificar la asistencia diaria de los operarios del almacén a su cargo.\r\n•	 Velar por el buen cumplimiento de las normativas internas de la empresa las normativas de higiene y seguridad laboral.\r\n•	Participar en la toma física mensual y anual de los inventarios del almacén.\r\n•	Experiencia en la logística de manejo correcto de materiales, almacenaje, traslado, carga y descarga, manual con el uso del montacargas\r\n•	Ver y supervisar que no acurra cualquier tipo de anomalía.\r\n•	Supervisar el cumplimiento de las tareas asignadas al personal del almacén.\r\n•	Coordinar y ejecutar los procesos de picking y packing de mercadería de acuerdo a la ruta de despacho del día.\r\n•	Velar por el correcto despacho de mercadería de acuerdo a las Guías de Remisión emitidas.\r\n•	Coordinar la carga y descarga de unidades de transporte propias y de terceros.\r\n•	Supervisar y ejecutar la adecuada manipulación de mercadería, para evitar la generación de mermas, así como la correcta ejecución del FIFO en caso de mercadería con control de lotes.\r\n•	Ejecutar la recepción de mercadería.\r\n•	Llevar control de los materiales necesarios en las operaciones de almacén para evitar su desabastecimiento.\r\n•	Reportar toda incidencia que ocurra con los equipos para su reparación.\r\n•	Reportar toda merma detectada en el almacén.\r\n•	Detectar e informar toda incidencia que ocurriera durante los procesos propios de almacén.\r\n•	Realizar informes periódicos de las actividades realizadas.\r\n•	Responsable del orden y limpieza del almacén.\r\n•	Proponer mejoras en los procesos de recepción, picking y distribución de mercadería, que contribuyan a la disminución de los tiempos de entrada y salida de mercadería.\r\n•	Remplazo en ausencia del Jefe de almacén.\r\n•	Otras actividades asignadas al cargo.\r\n', 'Masculino', 24, 45, 'ninguno', '08:00:00', '06:00:00', 2000, b'0', '-', b'0'),
(21, '-', 'Asistente de Operaciones', 'Universitario', 'Ultimos Ciclos', 'Administración, ingeniería industrial y/o afines.', '', '', 'Inglés', 'Básico', 'Microsoft Office', 'Básico', '-', '2 año', '-', '1.	Programar los turnos de trabajo y la asignación en los centros de destaque de los trabajadores destinados a cubrir inasistencias, descansos médicos y trabajos especiales realizando el monitoreo de su asistencia y cumplimiento del horario pactado.\r\n2.	Realizar el Tareo de los trabajadores asignados para inasistencia, descansos médicos y trabajos especiales.\r\n3.	Realizar pedidos de uniformes para las nuevas incorporaciones de personal nuevo.\r\n4.	Realizar la imputación de facturas correspondientes a servicios o productos contratados por el Área de Operaciones.\r\n5.	Administración de Caja Chica\r\n6.	Seguimiento y recopilación de liquidaciones de los supervisores zonales\r\n7.	Recepción y envío de sobre y documentación a Lima y demás provincias\r\n8.	Apoyo y soporte en las actividades comerciales a realizarse en la región\r\n9.	Emitir las cartas de presentación u otra correspondencia relacionada a los trabajadores o al servicio solicitada por el cliente.\r\n10.	Emitir por trabajos especiales o nuevos servicios regulares a solicitud de la jefatura inmediata.\r\n11.	Realizar las Autorizaciones de Pago para las movilidades de los supervisores.\r\n12.	Preparar la documentación requerida por el Departamento de su ámbito de actuación, organizando la misma para cumplir con la legalidad y normativa vigente. \r\n13.	Atender las llamadas distribuyéndolas a quien corresponda en el departamento o resolviendo las mismas con el objetivo de conseguir una eficiente atención. \r\n14.	Realizar cualquier otra tarea administrativa (búsqueda y entrega de documentación, fotocopias, faxes, elaboración y envío de cartas, escáner, etc.) a través del medio determinado por la Compañía para facilitar el desarrollo de la actividad. \r\n15.	Participar en la implementación, mantenimiento y mejora continua del Sistema de Gestión Integrado, teniendo especial cuidado de no apartarse de los procedimientos, asegurar el control y la seguridad de las operaciones, observar a las personas del entorno que realizan actividades bajo control de la organización y ser consciente que cualquier acción o inacción puede repercutir en la eficacia del sistema.\r\n16.	Otras funciones establecidas en los procedimientos de trabajo y aquellas que le sean asignadas por su Jefe inmediato.\r\n', 'Indistinto', 23, 0, 'Normal', '08:00:00', '18:00:00', 2000, b'1', 's/2000.00 - s/2200.00 (dependiendo de la experiencia)', b'0'),
(22, '-', 'ANALISTA DE COMPRAS', 'Universitario', 'Egresado', 'ING INDUSTRIAL, ADMINISTRACION, AFINES', '', '', '', 'Básico', 'EXCEL', 'Intermedio', 'LOGISTICA', '2 AÑOS', '--', '- TRATO DIRECTO CON PROVEEDORES\r\n- GESTIONAR LAS COMPRAS', 'Indistinto', 23, 50, 'NO PUEDE TENER PERDIDA', '07:30:00', '17:45:00', 3000, b'1', '---', b'1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos`
--

CREATE TABLE `permisos` (
  `usuario` varchar(45) NOT NULL,
  `ID_Categoria` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `permisos`
--

INSERT INTO `permisos` (`usuario`, `ID_Categoria`) VALUES
('alessandra', 1),
('alessandra', 2),
('alessandra', 3),
('alessandra', 4),
('alessandra', 5),
('alessandra', 6),
('alessandra', 7),
('alessandra', 8),
('alessandra', 9),
('alessandra', 10),
('alessandra', 11),
('alessandra', 12),
('alessandra', 13),
('carmen', 1),
('carmen', 2),
('carmen', 3),
('carmen', 4),
('carmen', 5),
('carmen', 6),
('carmen', 7),
('carmen', 8),
('carmen', 9),
('carmen', 10),
('carmen', 11),
('carmen', 12),
('carmen', 13),
('gerente', 1),
('gerente', 2),
('gerente', 3),
('gerente', 4),
('gerente', 5),
('gerente', 6),
('gerente', 7),
('gerente', 8),
('gerente', 9),
('gerente', 10),
('gerente', 11),
('gerente', 12),
('gerente', 13),
('gerente', 14),
('gerente', 15),
('gerente', 16),
('gerente', 17),
('gerente', 18),
('gerente', 19),
('gianella', 1),
('gianella', 2),
('gianella', 3),
('gianella', 4),
('gianella', 5),
('gianella', 6),
('gianella', 7),
('gianella', 8),
('gianella', 9),
('gianella', 10),
('gianella', 11),
('gianella', 12),
('gianella', 13),
('joe', 1),
('joe', 2),
('joe', 3),
('joe', 4),
('joe', 5),
('joe', 6),
('joe', 7),
('joe', 8),
('joe', 9),
('joe', 10),
('joe', 11),
('joe', 12),
('joe', 13),
('karen', 1),
('karen', 2),
('karen', 3),
('karen', 4),
('karen', 5),
('karen', 6),
('karen', 7),
('karen', 8),
('karen', 9),
('karen', 10),
('karen', 11),
('karen', 12),
('karen', 13),
('karen', 18),
('marita', 1),
('marita', 2),
('marita', 3),
('marita', 4),
('marita', 5),
('marita', 6),
('marita', 7),
('marita', 8),
('marita', 9),
('marita', 10),
('marita', 11),
('marita', 12),
('marita', 13),
('seleccion', 2),
('seleccion', 3),
('seleccion', 6),
('seleccion', 7),
('seleccion', 8),
('seleccion', 9),
('seleccion', 10),
('seleccion', 11),
('seleccion', 12),
('seleccion', 13),
('vanessa', 1),
('vanessa', 2),
('vanessa', 3),
('vanessa', 4),
('vanessa', 5),
('vanessa', 6),
('vanessa', 7),
('vanessa', 8),
('vanessa', 9),
('vanessa', 10),
('vanessa', 11),
('vanessa', 12),
('vanessa', 13);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `procesos`
--

CREATE TABLE `procesos` (
  `codigo` int(11) NOT NULL,
  `perfil` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `ciudad` varchar(50) DEFAULT NULL,
  `fechaPedido` date DEFAULT NULL,
  `fechaEntrega` date DEFAULT NULL,
  `detalles` text,
  `estado` bit(1) DEFAULT NULL,
  `eliminado` bit(1) DEFAULT NULL,
  `grifo` varchar(200) NOT NULL,
  `consultor` varchar(80) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `procesos`
--

INSERT INTO `procesos` (`codigo`, `perfil`, `cantidad`, `ciudad`, `fechaPedido`, `fechaEntrega`, `detalles`, `estado`, `eliminado`, `grifo`, `consultor`) VALUES
(1, 1, 2, 'TRUJILLO', '2017-06-19', '2017-06-24', 'tenemos prioridad para esto', b'1', b'1', 'España', ''),
(3, 1, 1, 'TRUJILLO', '2017-07-04', '2017-07-10', 'REQ TRUJILLO', b'1', b'1', 'España', ''),
(4, 2, 1, 'Trujillo', '2017-07-14', '2017-07-24', 'Experiencia: 1 año en funciones similares.', b'1', b'1', 'America Norte', 'CARLA'),
(5, 3, 1, 'Chiclayo', '2017-07-10', '2017-07-21', '-', b'1', b'1', 'America Norte', 'CARLA'),
(6, 4, 1, 'Chiclayo', '2017-07-07', '2017-07-18', 'E/S La Purísima. ', b'1', b'1', 'America Norte', 'MARITA'),
(7, 3, 1, 'TUMBES', '2017-07-10', '2017-07-21', 'EL COLABORADOR PUEDE VIVIR EN OTRA CIUDAD', b'1', b'1', 'America Norte', 'CARMEN'),
(8, 3, 1, 'Trujillo', '2017-07-10', '2017-07-21', '-', b'1', b'1', 'America Norte', 'VANESSA'),
(9, 3, 1, 'Nuevo Chimbote', '2017-07-10', '2017-07-21', '-', b'1', b'1', 'America Norte', 'VANESSA'),
(10, 5, 1, 'Talara', '2017-06-12', '2017-07-28', '-', b'1', b'1', 'America Norte', 'VANESSA'),
(11, 1, 2, 'trujillo', '2017-07-19', '2017-07-25', '-', b'1', b'1', 'Larco', 'WILLY'),
(12, 7, 1, 'Trujillo', '2017-07-20', '2018-08-03', '-', b'1', b'1', 'America Norte', 'VANESSA'),
(13, 6, 1, 'El Alto ', '2017-07-19', '2017-07-27', 'Auxiliar Logístico ', b'1', b'1', 'America Norte', 'ALESSANDRA'),
(14, 8, 1, 'TALARA', '2017-07-01', '2017-07-24', 'ADMINISTRADOR ES PARA LA SEDE TALARA', b'1', b'1', 'España', 'CARMEN'),
(15, 9, 1, 'TRUJILLO', '2017-07-31', '2017-08-11', 'Ejecutivo Comercial Inmobiliario fijo mas comisiones', b'1', b'1', 'America Norte', 'CARMEN'),
(16, 1, 1, 'CUSCO', '2017-07-31', '2017-08-11', 'BASICO MAS COMISIONES CON 1 AÑO DE EXPERIENCIA', b'1', b'1', 'America Norte', 'CARMEN'),
(17, 9, 1, 'CUSCO', '2017-07-31', '2017-08-11', 'EXPERIENCIA 1 AÑO BÁSICO 850', b'1', b'1', 'America Norte', 'MARITA'),
(18, 3, 1, 'Chimbote', '2017-07-10', '2017-07-22', '-', b'1', b'1', 'America Norte', 'CARMEN'),
(19, 1, 1, 'TRUJILLO', '2001-08-17', '2007-08-17', 'VENDEDOR DE TIENDA', b'1', b'1', 'Listo California', 'MARITA'),
(20, 1, 1, 'TRUJILLO', '2001-08-17', '2007-08-17', 'VENDEDOR DE TIENDA', b'1', b'1', 'Listo California', 'MARITA'),
(21, 1, 3, 'NUEVO CHIMBOTE', '2001-08-17', '2007-08-17', 'E/S el salvador', b'1', b'1', 'America Norte', 'MARITA'),
(22, 9, 2, 'CHICLAYO', '2017-07-05', '2017-07-20', 'ASESOR INMOBILIARIO', b'1', b'1', 'America Norte', 'CARMEN'),
(23, 1, 1, 'TRUJILLO', '2017-08-02', '2017-08-31', '-', b'1', b'1', 'America Norte', 'MARITA'),
(24, 10, 2, 'Lima', '2017-08-01', '2017-08-10', '-', b'1', b'1', 'America Norte', 'VANESSA'),
(25, 10, 1, 'CHIMBOTE', '2017-08-07', '2017-08-11', '-', b'1', b'1', 'America Norte', 'MARITA'),
(26, 10, 1, 'CHINCHA', '2017-08-07', '2017-08-11', '-', b'1', b'1', 'America Norte', 'ROBERTO'),
(27, 11, 1, 'Chiclayo', '2017-08-16', '2017-08-29', '-', b'1', b'1', 'America Norte', 'VANESSA'),
(28, 12, 1, 'Piura', '2017-08-16', '2017-08-29', '-', b'1', b'1', 'America Norte', 'VANESSA'),
(29, 13, 1, 'Trujillo', '2017-08-16', '2017-08-28', '-', b'1', b'1', 'America Norte', 'VANESSA'),
(30, 8, 1, 'Lima', '2017-09-07', '2017-09-13', '-', b'1', b'1', '', 'VANESSA'),
(31, 4, 1, 'Mollendo ', '2017-09-08', '2017-09-14', '-', b'1', b'1', '', 'VANESSA'),
(32, 15, 1, 'Trujillo', '2017-09-18', '2017-09-22', '-', b'1', b'1', '', 'VANESSA'),
(33, 14, 1, 'Trujillo', '2017-09-18', '2017-09-22', '-', b'1', b'1', '', 'VANESSA'),
(34, 16, 1, 'Trujillo', '2017-09-21', '2017-10-04', '-', b'1', b'0', '', 'VANESSA'),
(35, 16, 1, 'Piura', '2017-09-21', '2017-10-04', '-', b'1', b'0', '', 'VANESSA'),
(36, 8, 1, 'LIMA', '2017-10-10', '2017-10-23', '-', b'1', b'1', '', 'VANESSA'),
(37, 8, 1, 'LIMA', '2017-10-10', '2017-10-23', '-', b'1', b'1', '', 'VANESSA'),
(38, 8, 1, 'NAZCA', '2017-10-10', '2017-10-23', '-', b'1', b'0', '', 'VANESSA'),
(39, 17, 1, 'Lima', '2017-09-18', '2017-10-02', '-', b'1', b'1', '', 'MARITA'),
(40, 10, 1, 'Iquitos', '2017-09-18', '2017-10-02', '-', b'1', b'1', '', 'MARITA'),
(41, 9, 1, 'CUSCO', '2017-09-20', '2017-09-28', '-', b'1', b'1', '', 'ROBERTO'),
(42, 14, 1, 'TRUJILLO', '2017-09-15', '2017-09-27', '-', b'1', b'1', '', 'MARITA'),
(43, 10, 1, 'Lima', '2017-09-18', '2017-09-30', '-', b'1', b'1', '', 'ROBERTO'),
(44, 9, 1, 'CUSCO', '2017-09-20', '2017-09-28', '-', b'1', b'1', '', 'MARITA'),
(45, 10, 1, 'Lima', '2017-09-18', '2017-09-30', '-', b'1', b'1', '', 'MARITA'),
(46, 18, 1, 'Trujillo', '2017-09-21', '2017-10-04', '-', b'1', b'0', '', 'VANESSA'),
(47, 4, 1, 'CUSCO', '2017-10-16', '2017-10-23', '-', b'1', b'0', '', 'MARITA'),
(48, 19, 1, 'Talara', '2017-10-18', '2017-11-16', '-', b'1', b'0', '', 'VANESSA'),
(49, 20, 1, 'Lima', '2017-10-17', '2017-10-19', '-', b'1', b'1', '', 'MARITA'),
(50, 1, 26, 'Iquitos', '2017-10-02', '2017-10-30', 'Evaluación de postulantes para puestos de Atención al Cliente para la Ciudad de Iquitos.', b'1', b'0', '', 'MARITA'),
(51, 21, 1, 'Zona Norte', '2017-10-24', '2017-11-13', '-', b'1', b'0', '', 'VANESSA'),
(52, 8, 1, 'IQUITOS', '2017-10-23', '2017-11-06', 'IQUITOS ', b'1', b'0', '', 'MARITA'),
(53, 8, 1, 'LIMA SUR', '2017-10-10', '2017-10-23', '-', b'1', b'1', '', 'VANESSA'),
(54, 8, 1, 'SURCO', '2017-10-10', '2017-10-23', '-', b'1', b'0', '', 'CARMEN'),
(55, 8, 1, 'CHORRILLOS', '2017-10-10', '2017-10-23', '-', b'1', b'0', '', 'VANESSA'),
(56, 22, 1, 'LIMA', '2017-10-26', '2017-11-15', '---', b'1', b'0', '', 'JOE'),
(57, 7, 4, 'LIMA NORTE', '2017-10-31', '2017-11-14', '-', b'1', b'0', '', 'VANESSA'),
(58, 8, 4, 'LIMA NORTE', '2017-10-31', '2017-11-14', '-', b'1', b'0', '', 'VANESSA'),
(59, 8, 1, 'ICA', '2017-11-21', '2017-12-04', '-', b'1', b'0', '', 'VANESSA'),
(60, 21, 1, 'LIMA', '2017-12-17', '2017-12-01', 'Para el distrito de Magdalena ', b'1', b'0', '', 'MARITA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ternas`
--

CREATE TABLE `ternas` (
  `codigo` int(11) NOT NULL,
  `proceso` int(11) NOT NULL,
  `apellidos` varchar(50) NOT NULL,
  `nombres` varchar(50) NOT NULL,
  `rutaCV` varchar(100) DEFAULT NULL,
  `fechaEnvio` date DEFAULT NULL,
  `rrhhCliente` char(1) DEFAULT NULL,
  `induccion` char(1) DEFAULT NULL,
  `entrevista` char(1) DEFAULT NULL,
  `informe` char(1) DEFAULT NULL,
  `fechaIngreso` date DEFAULT NULL,
  `ordenServicio` varchar(50) DEFAULT NULL,
  `factura` varchar(50) DEFAULT NULL,
  `observacion` text,
  `eliminado` bit(1) DEFAULT b'0',
  `terna` int(1) DEFAULT '0',
  `dni` char(8) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  `estacion` varchar(200) NOT NULL,
  `porcentaje` int(11) NOT NULL,
  `verLaboral` char(1) DEFAULT NULL,
  `elegido` char(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ternas`
--

INSERT INTO `ternas` (`codigo`, `proceso`, `apellidos`, `nombres`, `rutaCV`, `fechaEnvio`, `rrhhCliente`, `induccion`, `entrevista`, `informe`, `fechaIngreso`, `ordenServicio`, `factura`, `observacion`, `eliminado`, `terna`, `dni`, `telefono`, `estacion`, `porcentaje`, `verLaboral`, `elegido`) VALUES
(32, 1, 'Diaz Guevara ', 'Gary', '../cv/', '2017-04-10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Documentos completos. No quiere Playa', b'0', 2, '48668175', '936337680', 'España', 90, '1', '0'),
(33, 1, 'Olaya Zapata', 'Mariana', '../cv/', '2017-04-19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Mañana (27-04-2017) entrega documentos.', b'0', 2, '74988515', '74988515', 'Santo Dominguito', 40, '1', '0'),
(34, 3, 'Alayo Alayo', 'Martha', '../cv/', '2017-04-18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Ya tiene todos los documentos.', b'0', 1, '76364338', '984269175', 'España', 100, '1', '0'),
(35, 1, 'Reyes Ysla', 'Katherine', '../cv/', '2017-04-24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'FALTA ANTECEDENTES POLICIAS', b'0', 2, '70146530', '927094758', 'Moche', 40, '1', '0'),
(36, 1, 'Cabanillas Valderrama', 'Anthony', '../cv/', '2017-05-05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tramitando documentos', b'0', 2, '48512393', '970042949', 'Union', 40, '1', '0'),
(37, 1, 'Jimenez Mendoza', 'Kevin', '../cv/', '2017-05-05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tramitando documentos', b'0', 2, '11111111', '971663978', 'Postes', 40, '1', '0'),
(38, 1, 'Lopez De la Cruz', 'Quincinio', '../cv/', '2017-05-10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tiene todo', b'0', 2, '42398354', '955967059', 'Listo California', 100, '1', '0'),
(39, 1, 'Vilcabana Flores', 'Jaime', '../cv/', '2017-05-11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tiene todo', b'0', 2, '72805421', '958031578', 'Pacasmayo', 100, '1', '0'),
(40, 1, 'Merino Rios', 'José', '../cv/', '2017-05-13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tramitando Documentos', b'0', 2, '71062380', '965387358', 'España', 40, '1', '0'),
(41, 1, 'Okon Chalan', 'Rosa', '../cv/', '2017-05-13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tramitando Documentos', b'0', 2, '17936844', '936646303', 'Santo Dominguito', 40, '1', '0'),
(71, 4, 'LA MADRID VASQUEZ', 'JHOANA LISSET', '../cv/CV La Madrid Vásquez Jhoana Lisset.docx', '2017-07-14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, ' lamadridjhoana94@gmail.com', b'1', 1, '70161525', '950714222', 'America Norte', 0, '1', '0'),
(43, 1, 'Jimenez Vicente', 'Luz', '../cv/', '2017-05-19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'TIENE TODO', b'0', 2, '73487337', '960692100', 'España', 100, '1', '0'),
(44, 11, 'Carranza Aparicio', 'Sol', '../cv/', '2017-05-20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'TIENE TODO', b'0', 1, '73056399', '968356003', 'Larco', 100, '1', '0'),
(45, 1, 'Muñoz Garcia', 'Ana', '../cv/', '2017-05-20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'TIENE TODO LOS DOCUMENTOS', b'0', 2, '76536159', '927338733', 'Larco', 100, '1', '0'),
(46, 1, 'Silva Cotrina', 'Santos', '../cv/', '2017-05-22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'TIENE TODO', b'0', 2, '44999961', '949864285', 'Santo Dominguito', 100, '1', '0'),
(47, 1, 'Villanueva Lozano', 'Silvia', '../cv/', '2017-05-23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'TIENE TODO', b'0', 2, '46333290', '942204916', 'Larco', 100, '1', '0'),
(48, 1, 'Quispe Alayo', 'Cristhian', '../cv/', '2017-05-25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tramitando documentos', b'0', 2, '72538940', '943376095', 'Union', 40, '1', '0'),
(49, 1, 'Cabrera Mantilla', 'Pedro', '../cv/', '2017-05-26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'TIENE TODO DOCUMENTOS', b'0', 2, '70845135', '920025043', 'Postes', 100, '1', '0'),
(50, 1, 'Vera Barreno', 'Anthony', '../cv/', '2017-05-27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tramitando documentos', b'0', 2, '70272237', '960685290', 'España', 40, '1', '0'),
(51, 1, 'Placido Garcia', 'Rosa', '../cv/', '2017-06-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tramitando documentos', b'0', 2, '41424143', '976203632', 'Postes', 40, '1', '0'),
(52, 1, 'Garcia Solar', 'Evelyn', '../cv/', '2017-05-31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tramitando documentos', b'0', 2, '43028810', '973350036', 'Larco', 40, '1', '0'),
(31, 1, 'Agreda Mondragon ', 'Samuel', '../cv/', '2017-03-23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tramitando documentos, ya tiene Antecedentes.', b'0', 1, '44472412', '945121637', 'Santo Dominguito', 40, '1', '0'),
(53, 1, 'Araujo Casas', 'Jannitxa', '../cv/', '2017-06-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Ya tiene carnet de Sanidad/ Llamar para recordarle', b'0', 2, '70928146', '973731686', 'Perla', 60, '1', '0'),
(54, 1, 'Flores Valera', 'Oriana', '../cv/', '2017-06-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tramitando documentos', b'0', 2, '74908246', '949300427', 'Postes', 40, '1', '0'),
(55, 1, 'Zamudio Valdiviezo', 'Otilia', '../cv/', '2017-06-03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tramitando documentos', b'0', 2, '47993916', '982472824', 'Perla', 40, '1', '0'),
(56, 1, 'Villena Rivera', 'Jesus', '../cv/', '2017-06-06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'TIENE TODO', b'0', 2, '72499190', '937690245', 'Postes', 100, '1', '0'),
(57, 1, 'Blas Hilario', 'Alex', '../cv/', '2017-06-06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'TIENE TODOS SUS DOCUMENTOS', b'0', 2, '70091372', '970009582', 'Santo Dominguito', 100, '1', '0'),
(58, 1, 'Vargas Rumay', 'Brandom', '../cv/', '2017-06-07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tramitando documentos', b'0', 2, '70153752', '921752605', 'America Norte', 80, '1', '0'),
(59, 1, 'Briceño Rodriguez', 'Mariela', '../cv/', '2017-06-13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tramitando documentos', b'0', 2, '18011325', '989027799', 'America Norte', 40, '1', '0'),
(60, 1, 'Cusque Vasquez', 'Marianela', '../cv/', '2017-06-13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tramitando documentos', b'0', 2, '45087609', '928292832', 'Moche', 100, '1', '0'),
(61, 1, 'Velasquez Oyola', 'Saul', '../cv/', '2017-06-13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Falta traer carnet de sanidad/ CV', b'0', 2, '76944585', '951409017', 'Union', 60, '1', '0'),
(62, 1, 'Ramirez Rodriguez', 'Jean', '../cv/', '2017-06-13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tramitando documentos', b'0', 2, '71742126', '943483846', 'Postes', 100, '1', '0'),
(63, 1, 'Moreno Torres ', 'Raul', '../cv/', '2017-06-14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tramitando documentos', b'0', 2, '46567206', '989220830', 'Union', 40, '1', '0'),
(64, 1, 'Flores Sanchez', 'Anghela', '../cv/', '2017-06-12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tramitando documentos', b'0', 2, '48881380', '', 'Postes', 100, '1', '0'),
(65, 1, 'Ramos Montes', 'Anita', '../cv/', '2017-06-14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tramitando documentos', b'0', 2, '79669939', '978720919', 'Perla', 60, '1', '0'),
(66, 1, 'Ramirez Sarabia', 'Hormencinda', '../cv/', '2017-06-14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tramitando documentos', b'0', 2, '45872980', '943267850', 'Postes', 40, '1', '0'),
(67, 1, 'Guitierrez Santillan', 'Jose', '../cv/', '2017-06-14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tramitando documentos', b'0', 2, '46918920', '973776349', 'España', 40, '1', '0'),
(68, 1, 'Salirrosas Garcia', 'Deivy', '../cv/', '2017-06-15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tramitando documentos', b'0', 2, '76163224', '948391424', 'Moche', 40, '1', '0'),
(69, 1, 'Morales Hurtado', 'Catherine', '../cv/', '2017-06-14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'TIENE TODOS SUS DOCUMENTOS', b'0', 2, '46301083', '940181407', 'Postes', 100, '1', '0'),
(70, 1, 'Avila Briceño ', 'Bertha', '../cv/', '2017-06-20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tramitando Documentos', b'0', 2, '48126916', '977460869', 'Postes', 40, '1', '0'),
(72, 0, 'LA MADRID VASQUEZ', 'JHOANA LISSET', '../cv/', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0', 0, NULL, NULL, '', 0, '1', '0'),
(74, 4, 'GARCÍA FLORES', 'LOURDES PAMELA', '../cv/CV García Flores Lourdes Pamela.docx', '2017-07-14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'glourdespamela@gmail.com', b'1', 0, '70852496', '973445168', 'America Norte', 0, '1', '0'),
(75, 4, 'ROBLES BENAVIDES', 'MARCO ANTONIO', '../cv/', '2017-07-14', '1', '1', '1', '1', '2017-08-01', '', '', 'marco.robles.b@outlook.com', b'0', 2, '45478884', '949326741', 'America Norte', 0, '1', '1'),
(76, 5, 'MONTOYA CASTILLO', 'CRISTHIAN ALFREDO', '../cv/', '2017-07-10', '1', '1', '1', '1', '2017-08-01', 'Seleccionado', '001-1172', 'empezó a laborar el 01.08 tiene 60 días de garanti', b'0', 1, '46215098', '970410670', 'America Norte', 0, '1', '1'),
(77, 5, 'CASUSOL LAMAS', 'HENRY CARLOS', '../cv/CV CASUSOL LAMAS HENRY CARLOS.docx', '2017-07-10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'hc_casusol@hotmail.com / tentativos', b'0', 0, '40049445', '941180390 - 95485916', 'America Norte', 0, '1', '0'),
(78, 5, 'CASUSOL LAMAS', 'HENRY CARLOS', '../cv/CV CASUSOL LAMAS HENRY CARLOS.docx', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'1', 0, NULL, NULL, '', 0, '1', '0'),
(79, 5, 'MORALES CHICOMA', 'DANIEL', '../cv/CV MORALES CHICOMA DANIEL.docx', '2017-07-10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'renato2001@hotmail.es / tentativos', b'1', 0, '16705517', '(074) 263766 - 93765', 'America Norte', 0, '1', '0'),
(80, 6, 'FERNANDEZ MONTENEGRO ', 'ESTEFANI ELIZABETH', '../cv/Estefani Fernandez Montenegro.docx', '2017-07-12', '1', '1', '1', '1', '0000-00-00', '', '', 'no paso entrevista final porque PRIMAX la rechazó,', b'0', 2, '46669845', '947473838', 'America Norte', 0, '1', '0'),
(81, 6, 'CUADRA LÁZARO', 'GIANCARLO', '../cv/Albin Cuadra Lázaro - CV.docx', '0000-00-00', '1', '1', '1', '1', '0000-00-00', '', '', 'no pasó entrevista final por tener rotación y poco', b'0', 2, '47477111', '941120763LTA INFORME', 'America Norte', 0, '1', '0'),
(82, 6, 'LIMO RIOJA', 'REINA', '../cv/Reina Limo Rioja- CV.docx', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', b'0', 0, '46256772', '94264440', 'America Norte', 0, '1', '0'),
(83, 7, 'LOPEZ ASTUDILLO', 'HUGO', '../cv/', '2017-07-17', '1', '1', '1', '1', '0000-00-00', '', '', 'NO SELECCIONARON A LA TERNA', b'0', 2, '70070262', '950081918', 'America Norte', 0, '1', '0'),
(84, 8, 'ARAUJO ARROYO ', 'CARMEN CITA', '../cv/curriculum carmen.docx', '2017-07-13', '1', '1', '1', '1', '0000-00-00', '', '', 'Descartada por malas referencias.', b'0', 2, '44083028', ' 999074390', 'America Norte', 0, '1', '0'),
(85, 8, 'CORCUERA IDROGO ', 'JORGE ANTONIO', '../cv/', '2017-07-12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Postulante tentativo', b'0', 2, '42085255', '942098395', 'America Norte', 0, '1', '0'),
(86, 9, ' TORRES SANES', 'ViCTOR GIANCARLO', '../cv/Formato de Curriculum Vitae T-Soluciona (1) (20).docx', '2017-07-14', '1', '0', '1', '1', '0000-00-00', '', '', '', b'0', 2, '43315473', '930-627736', 'America Norte', 0, '1', '0'),
(87, 9, ' Angeles Saenz', 'Ambar Kris', '../cv/Formato de Curriculum Vitae T-Soluciona (1) (21).docx', '0000-00-00', '1', '0', '1', '1', '0000-00-00', '', '', 'Ejecutivo de campo ferretero', b'0', 2, '42170302', ' 958457525', 'America Norte', 0, '1', '0'),
(88, 9, ' Angeles Saenz', 'Ambar Kris', '../cv/Formato de Curriculum Vitae T-Soluciona (1) (21).docx', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'1', 0, NULL, NULL, '', 0, '1', '0'),
(89, 9, ' CARRION RAMIREZ ', 'DAVID YSAIAS', '../cv/david carrion CV.docx', '2017-07-12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tentativo', b'0', 2, '46029271', '960475701', 'America Norte', 0, '1', '0'),
(90, 10, 'PALACIOS NIÑO ', 'PERCY PAUL', '../cv/CV  Palacios Niño Percy Paul.pdf', '2017-06-21', '1', '1', '1', '1', '0000-00-00', '', '', 'Pasó entrevista con Negrón, área de RR.HH y con re', b'0', 1, '48267722', '968237455', 'America Norte', 0, '2', '0'),
(91, 10, 'FARIAS AREVALO', ' LUIS CARLO ', '../cv/CV  Farías Arévalo Luis Carlo.pdf', '2017-07-05', '1', '1', '1', '0', '0000-00-00', '', '', 'Descartado, tiene experiencia en compresores y mot', b'0', 1, '45208297', '964251245', 'America Norte', 0, '0', '0'),
(92, 10, 'MARCA IBAÑEZ ', 'VICTOR MANUEL', '../cv/', '2017-07-14', '1', '1', '1', '0', '0000-00-00', '', '', 'Otro celular 921824490/ 969722815. Pasó entrevista', b'0', 1, '40297451', '942325718', 'America Norte', 0, '0', '0'),
(93, 7, 'ESPINOZA ZAPATA', 'JOSE ARMANDO', '../cv/HUEMURA TUMBES - ESPINOZA ZAPATA JOSE ARMANDO.docx', '2017-07-18', '1', '1', '1', '1', '0000-00-00', '', '', '', b'0', 2, '80533053', '969479908', 'America Norte', 0, '1', '0'),
(94, 6, 'LIMO LIZARZABURU', 'ROBERTO', '../cv/Roberto Limo Lizarzaburu.docx', '2017-07-10', '1', '1', '1', '1', '2017-08-21', '51163', '001-1217', 'Postulante empieza a laborar el 21-08-17.', b'0', 1, NULL, NULL, '', 0, '1', '1'),
(95, 11, 'prueba', 'prueba', '../cv/', '2017-07-20', '1', '1', '1', '1', '0000-00-00', '', '', 'as', b'0', 1, '12121212', '987654321', 'America Norte', 0, '1', '0'),
(96, 5, 'VALDIVIEZO EZPINOZA', 'LUIS ALBERTO', '../cv/CV Luis Valdiviezo.docx', '2017-07-10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'luisin_16_05@hotmail.com', b'0', 0, '72879511', '073-393307 / 9684119', 'America Norte', 0, NULL, '0'),
(97, 4, 'LA MADRID VÁSQUEZ', 'JHOANA LISSET', '../cv/CV La Madrid Vásquez Jhoana Lisset.pdf', '2017-07-10', '1', '1', '1', '1', '2017-08-01', '', '', 'lamadridjhoana94@gmail.com', b'0', 2, '70161525', '950714222', 'America Norte', 0, '1', '1'),
(98, 4, 'GARCÍA FLORES', 'LOURDES PAMELA', '../cv/CV García Flores Lourdes Pamela.pdf', '2017-07-10', '1', '1', '1', '1', '2017-08-01', 'pago en efectivo', 'efectivo', 'glourdespamela@gmail.com', b'0', 1, '70852496', '973445168', 'America Norte', 0, '1', '1'),
(99, 5, 'MARCELO CLEMENTE', 'DORA', '../cv/CV Dora Marcelo Clemente.pdf', '2017-07-21', '1', '1', '1', '1', '0000-00-00', '', '', 'domac28@hotmail.com', b'0', 2, '16715042', '949913187', 'America Norte', 0, '1', '0'),
(100, 5, 'LARA DELGADO', 'JOSE ALONSO', '../cv/CV José Alonso Lara Delgado.pdf', '2017-07-20', '1', '1', '1', '1', '0000-00-00', '', '', 'alonsolaradelgado@gmail.com', b'0', 2, '41751298', '948839479', 'America Norte', 0, '1', '0'),
(101, 7, 'PRECIADO GARCÍA', 'CARLOS OSWALDO', '../cv/CV Carlos Oswaldo Preciado Garcia.pdf', '2017-07-21', '1', '1', '1', '1', '0000-00-00', '', '', '', b'0', 2, '42474688', '924461377', 'America Norte', 0, '1', '0'),
(102, 8, 'MORAN PEREZ', 'ALEX RENZO', '../cv/CV Alex  Renzo  Morán Pérez.pdf', '0000-00-00', '0', '1', '1', '1', '0000-00-00', '', '', 'Sobrecalificado', b'0', 2, '18167352', '976591555 / 94500478', 'America Norte', 0, '1', '0'),
(103, 8, 'SANCHEZ AVALOS', 'EDWIN MARTIN', '../cv/CV Edwin Martín Sanchez Avalos.pdf', '2017-08-16', '1', '1', '1', '1', '2017-08-15', '001', '1189', 'SELECCIONADO', b'0', 1, '18229127', '985556078', 'America Norte', 0, '1', '1'),
(104, 6, ' Incio Torres', 'José Luis Antonio ', '../cv/CV- TORRES INCIO JOSE LUIS ANTONIO.docx', '0000-00-00', '1', '1', '1', '1', '0000-00-00', '', '', 'ya pasó entrevista final', b'0', 2, '72918644', '947820773', 'America Norte', 0, '1', '0'),
(105, 9, ' ABANTO ASIAN', 'ALDO JOEL', '../cv/CV Aldo Joel Abanto Asian.pdf', '2017-08-21', '1', '1', '1', '1', '2017-08-21', '001', '1190', 'SELECCIONADO', b'0', 1, '41188875', '986162001', 'America Norte', 0, '1', '1'),
(106, 7, 'PRECIADO GARCIA', 'CARLOS OSWALDO', '../cv/', '2017-07-15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', b'0', 0, '42474688', '924461377', 'America Norte', 0, NULL, '0'),
(107, 14, 'PEREZ', 'ROBERTO', '../cv/', '2017-07-24', '0', '0', '0', '', '0000-00-00', '', '', 'COESTI TALARA', b'1', 1, '25830144', '970009124', 'España', 0, '0', '0'),
(108, 14, 'RUJEL AGUILAR', 'YURI ARTURO ', '../cv/', '2017-08-01', '1', '1', '1', '1', '2017-08-01', '', '', 'FUE DESCARTADO POR PERTENECER A LA COMPETENCIA', b'0', 2, '0 564023', '945040184', 'America Norte', 0, '1', '1'),
(109, 13, 'DIAZ COQUEHUANCA ', 'CRISTIAN MARTIN', '../cv/CHRISTIAN MARTIN DIAZ CHOQUEHUANCA.pdf', '2017-07-26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Auxiliar Logístico - No cumple con el perfil Gasco', b'1', 0, '44843258', '932868831/975123142', 'America Norte', 0, NULL, '0'),
(110, 13, 'Alburquerque Amaya ', 'Rodwin Franklin ', '../cv/RODWIN FRANKLIN ALBURQUERQUE AMAYA.docx', '2017-07-26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Auxiliar Logístico - No cumple con el perfil Gasco', b'0', 0, '45692101', '927583124 ', 'America Norte', 0, NULL, '0'),
(111, 13, 'Chinga García ', 'Luis Rodolfo ', '../cv/LUIS RODOLFO CHINGA GARCÍA..pdf', '2017-07-26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Auxiliar Logístico - No cumple con el perfil Gasco', b'0', 0, '44031761', '954456358', 'America Norte', 0, NULL, '0'),
(112, 14, 'MORALES DIAZ', 'SUSANA NABELISA ', '../cv/CV SUSANA NABELISA MORALES DÍAZ.pdf', '2017-08-01', '1', '1', '1', '1', '2017-08-01', '', '', 'ENTREVISTA 01.08 EN MEGA PIURA', b'0', 2, '70618355', '968661327', 'America Norte', 0, '1', '1'),
(113, 14, 'PRICE SOCOLA', 'JOSELINNE MAGALY ', '../cv/CV JOSELINNE MAGALY PRICE SOCOLA.pdf', '2017-08-01', '1', '1', '1', '1', '2017-08-01', '51163', '001-1217', 'FUE SELECCIONADO TALARA', b'0', 1, '40274617', '964999989', 'America Norte', 0, '1', '1'),
(114, 13, 'Arica Vera ', 'Juan Manuel ', '../cv/JUAN MANUEL ARICA..pdf', '2017-07-26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Auxiliar Logístico - No cumple con el perfil Gasco', b'0', 0, '47521997', '980356284', 'America Norte', 0, NULL, '0'),
(115, 12, 'DELGADO MEDINA', ' LUIS JORDAN', '../cv/', '0000-00-00', '1', '1', '1', '0', '0000-00-00', '', '', '', b'1', 2, NULL, NULL, '', 0, '1', '0'),
(116, 12, 'JIMENEZ ESPEJO ', 'IRVIN ATILIO', '../cv/', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'No se presentó', b'1', 0, '', '', 'America Norte', 0, NULL, '0'),
(117, 12, 'AMAYA LUNA VICTORIA ', 'ALFREDO RAFAEL', '../cv/', '0000-00-00', '1', '1', '1', '0', '0000-00-00', '', '', '', b'1', 1, NULL, NULL, '', 0, '1', '0'),
(118, 12, 'RAMOS VASQUEZ ', 'ROSMERI ', '../cv/', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Puede desempeñarse como Administrador. Es más ofic', b'1', 2, '', '', 'America Norte', 0, NULL, '0'),
(119, 12, 'SALDAÑA RODRIGUEZ ', 'GILMER', '../cv/', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Tiene antecedentes por hurto.', b'0', 2, '', '', 'America Norte', 0, NULL, '0'),
(120, 12, 'CALDERON CENIZARIO ', 'EDWIN JHAROL', '../cv/', '0000-00-00', '1', '1', '1', '0', '0000-00-00', '', '', '', b'1', 1, '', '', 'America Norte', 0, '1', '0'),
(121, 11, 'sadfg', 'sdfghj', '../cv/', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, b'0', 2, NULL, NULL, '', 0, NULL, '0'),
(122, 6, 'Rentería Morales', 'Frankie Elmer', '../cv/', '0000-00-00', '1', '1', '1', '1', '0000-00-00', '', '', 'ya pasó entrevista final', b'0', 2, NULL, NULL, '', 0, '1', '0'),
(123, 12, 'LLERENA ALFARO ', 'RUTH ADRIANA', '../cv/', '0000-00-00', '1', '1', '1', '0', '0000-00-00', '', '', '', b'1', 1, NULL, NULL, '', 0, '1', '0'),
(124, 20, 'CRUZ VALDEZ', 'HUMBERTO', '../cv/', '2007-08-17', '1', '1', '1', '1', '2007-08-17', '', '', 'ingresa 07-08-2017', b'1', 1, '48791750', '997898469', 'Listo California', 0, '1', '1'),
(125, 20, 'CARRANZA APARICIO', 'SOL DAIANA', '../cv/', '0000-00-00', '0', '0', '0', '0', '0000-00-00', 'DOCUMENTOS COMPLETOS', 'INGRESA 21-08-17', '', b'0', 2, '', '', '', 0, '1', '0'),
(126, 17, 'HUANCA HUAYTA', 'NICOLE', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'persona apta para asesora de ventas. ', b'0', 2, '', '', 'America Norte', 0, '0', '0'),
(127, 12, 'DELGADO MEDINA ', 'LUIS JORDAN', '../cv/Formato de Curriculum Vitae T-Soluciona (1) (25).docx', NULL, '0', '0', '0', '0', NULL, NULL, NULL, NULL, b'1', 0, NULL, NULL, '', 0, '0', '0'),
(128, 12, 'DELGADO MEDINA ', 'LUIS JORDAN', '../cv/Formato de Curriculum Vitae T-Soluciona (1) (25).docx', NULL, '0', '0', '0', '0', NULL, NULL, NULL, NULL, b'1', 0, NULL, NULL, '', 0, '0', '0'),
(129, 12, 'DELGADO MEDINA ', 'LUIS JORDAN', '../cv/Formato de Curriculum Vitae T-Soluciona (1) (25).docx', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'Tiene experiencia como ADMINISTRADOR y CONDUCTOR, ', b'1', 2, '', '', 'America Norte', 0, '0', '0'),
(130, 12, 'DELGADO MEDINA', 'LUIS JORDAN', '../cv/Formato de Curriculum Vitae T-Soluciona (1) (25).docx', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'Experiencia como Administrador y Conductor de TV. ', b'0', 2, '47262718', '934107323/ 665716/ 9', 'America Norte', 0, '0', '0'),
(131, 12, 'RAMOS VASQUEZ ', 'ROSMERI ', '../cv/Formato de Curriculum Vitae T-Soluciona (1) (29).docx', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'Tiene experiencia como ADMINISTRADOR de tda retail', b'0', 2, '41949774', '923736923/044613194', 'America Norte', 0, '0', '0'),
(132, 12, 'ORTECHO CHACON ', 'ROMMY OLENKA', '../cv/ORTECHO CHACON ROMMY OLENKA.pdf', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'Su perfil encaja para Administrador o asistente Ad', b'0', 2, '40487880', '939280341', 'America Norte', 0, '0', '0'),
(133, 12, 'LLERENA ALFARO ', 'RUTH ADRIANA', '../cv/CV  Llerena Alfaro Ruth Adriana.pdf', '0000-00-00', '1', '1', '1', '0', '0000-00-00', '', '', '', b'1', 2, NULL, NULL, '', 0, '1', '0'),
(134, 12, 'AMAYA LUNA VICTORIA', ' ALFREDO RAFAEL', '../cv/CV  Amaya Luna Victoria Alfredo Rafael.pdf', '0000-00-00', '1', '1', '1', '0', '0000-00-00', '', '', 'Administrador o gerente de Tienda Retail', b'1', 2, NULL, NULL, '', 0, '1', '0'),
(135, 12, 'CALDERON CENIZARIO ', 'EDWIN JHAROL', '../cv/CV  Calderon Cenizario Edwin Jharol.pdf', '0000-00-00', '1', '1', '1', '0', '0000-00-00', '', '', 'Asistente de tienda retail', b'0', 2, NULL, NULL, '', 0, '1', '0'),
(136, 21, 'Saavedra Rojas', 'Xiomara', '../cv/', '2017-08-01', '0', '0', '0', '0', '0000-00-00', '', '', 'documentos completos pero desistieron.', b'0', 2, '76943135', '960451321', 'America Norte', 0, '0', '0'),
(137, 21, 'Estrada Matheus', 'Stefanny', '../cv/', '2017-08-01', '1', '1', '1', '0', '0000-00-00', '', '', 'documentos completos pero desistió.', b'0', 2, '70179446', '942001819', 'America Norte', 0, '1', '0'),
(138, 22, 'BARSALLO CRUZADO', 'CATHERINE', '../cv/CV - Wendy Barsallo.pdf', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'SELECCIONADA', b'1', 2, '40113534', '978751296', 'America Norte', 0, '0', '0'),
(139, 17, 'PORROA GUDIEL ', 'KAREN', '../cv/', '2017-08-01', '1', '1', '1', '0', '0000-00-00', '', '', 'desistió del puesto, se encuentra laborando.', b'0', 2, '43449311', '949760045', 'America Norte', 0, '1', '0'),
(140, 22, 'BARSALLO CRUZADO', 'CATHERINE', '../cv/CV - Wendy Barsallo.pdf', NULL, '0', '0', '0', '0', NULL, NULL, NULL, NULL, b'1', 0, NULL, NULL, '', 0, '0', '0'),
(141, 22, 'VERA SULLON', 'JULLIANA VANESSA', '../cv/CV - Julliana Vera.pdf', '0000-00-00', '1', '1', '1', '1', '2017-07-26', '', '1142/1177', '001-1142 (50%) 001-1177 (50% RESTANTE)', b'0', 1, '41940384', '950667030', 'America Norte', 0, '1', '1'),
(142, 21, 'Rodriguez Garay', 'Christian', '../cv/', '2017-07-01', '0', '0', '0', '0', NULL, NULL, NULL, 'próximo requerimiento.', b'0', 2, '72733508', '934998955', 'America Norte', 0, '0', '0'),
(143, 22, 'BARSALLO CRUZADO', 'WENDY CATHERINE', '../cv/CV - Wendy Barsallo.pdf', '0000-00-00', '1', '1', '1', '1', '2017-07-26', '', '1142/1177', '001-1142 (50%) 001-1177 (50% RESTANTE)', b'0', 1, '40113534', '978751296', 'America Norte', 0, '1', '1'),
(144, 22, 'AYALA FLORES', 'CINTIA CRISTINA', '../cv/CV - Cintia Ayala Flores.pdf', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 0, '42654325', '960134903', 'America Norte', 0, '0', '0'),
(145, 20, 'OLIVARES PARRAVICINI ', 'MARIO', '../cv/', '0000-00-00', '1', '1', '1', '1', '2017-08-21', 'DOCS. COMPLETOS', '', 'VENDEDOR DE PLAYA, E/s LOS POSTES', b'0', 1, '70016034', '942984184', 'Postes', 0, '1', '1'),
(146, 20, 'SANTISTEBAN REYES ', 'JUAN', '../cv/', '0000-00-00', '1', '1', '1', '0', '0000-00-00', 'DOCS. COMPLETOS', '', 'VENDEDOR DE PLAYA, cubre E/S LA PERLA', b'0', 1, '70652180', '968313930', 'Perla', 0, '1', '0'),
(147, 20, 'ACARO SAUCEDO ', 'GERALD STEVE', '../cv/', '0000-00-00', '1', '1', '0', '0', '0000-00-00', 'DOCS. COMPLETOS', '', 'VENDEDOR DE PLAYA, E/s LOS POSTES', b'0', 1, '70664296', '943126186', 'Postes', 0, '0', '0'),
(148, 20, 'DURAND CUEVA', 'ANA', '../cv/', '0000-00-00', '1', '1', '1', '1', '2017-08-14', 'DOCS. COMPLETOS', '', 'ingresó 14-08-17', b'0', 1, '75395568', '989565933', 'America Norte', 0, '1', '1'),
(149, 20, 'VARGAS BENITES', 'JENNY', '../cv/', '0000-00-00', '1', '1', '0', '0', '0000-00-00', 'DOCS. COMPLETOS', '', 'VENDEDOR DE PLAYA, E/s LOS POSTES', b'0', 1, '45393686', '988662692', 'America Norte', 0, '0', '0'),
(150, 20, 'FLORES VALERA', 'YOBELI ORIANA', '../cv/', '0000-00-00', '1', '1', '0', '0', '0000-00-00', 'DOCS. COMPLETOS', '', 'VENDEDOR DE PLAYA, E/s LOS POSTES', b'0', 1, '74908256', '949300427', 'Postes', 0, '0', '0'),
(151, 12, 'AMAYA LUNA VICTORIA ', 'ALFREDO RAFAEL', '../cv/CV  Amaya Luna Victoria Alfredo Rafael.pdf', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 2, '47614690', '968745573', 'America Norte', 0, '0', '0'),
(152, 12, 'AMAYA LUNA VICTORIA ', 'ALFREDO RAFAEL', '../cv/CV  Amaya Luna Victoria Alfredo Rafael.pdf', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'Gerente de tienda retail', b'0', 2, '47614690', '968745573', 'America Norte', 0, '0', '0'),
(153, 12, 'CALDERON CENIZARIO ', 'EDWIN JHAROL', '../cv/CV  Calderon Cenizario Edwin Jharol.pdf', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'Asistente Administrativo', b'0', 2, '47381225', '941308180', 'America Norte', 0, '0', '0'),
(154, 12, 'LLERENA ALFARO', ' RUTH ADRIANA', '../cv/CV  Llerena Alfaro Ruth Adriana.pdf', '2017-08-23', '1', '1', '1', '1', '2017-08-23', '4510473843', 'E001-20', 'SELECCIONADA Y FACTURADA', b'0', 1, '47217099', '959377479', 'America Norte', 0, '1', '1'),
(155, 17, 'QUISPE HUAHUATICO ', 'JOVANA', '../cv/', NULL, '0', '0', '0', '0', NULL, NULL, NULL, NULL, b'1', 0, NULL, NULL, '', 0, '0', '0'),
(156, 17, 'QUISPE HUAHUATICO ', 'JOVANA', '../cv/', '2017-08-01', '0', '0', '0', '0', NULL, NULL, NULL, 'Desistió del puesto. Perfil para asesora comercial', b'0', 0, '46620444', '921-560589', 'America Norte', 0, '0', '0'),
(157, 21, 'TOSCANO ANGULO ', 'EDGAR CLAUDIO', '../cv/', '0000-00-00', '1', '1', '1', '1', '0000-00-00', '', '', 'ingresó 14-08-17', b'0', 1, '48286836', '979558275', 'America Norte', 0, '1', '0'),
(158, 26, 'PÉREZ TAPIA', 'BRENDA THALINA ', '../cv/CV Brenda Pérez Tapia.docx', NULL, '0', '0', '0', '0', NULL, NULL, NULL, NULL, b'1', 0, NULL, NULL, '', 0, '0', '0'),
(159, 26, 'PÉREZ TAPIA', 'BRENDA THALINA ', '../cv/CV Brenda Pérez Tapia.docx', NULL, '0', '0', '0', '0', NULL, NULL, NULL, NULL, b'1', 0, NULL, NULL, '', 0, '0', '0'),
(160, 26, 'PÉREZ TAPIA', 'BRENDA THALINA ', '../cv/CV Brenda Pérez Tapia.docx', '2017-08-07', '0', '0', '0', '0', NULL, NULL, NULL, 'Ps. Free Lance Chincha Alta', b'0', 0, '73780941', '979519604', 'America Norte', 0, '0', '0'),
(161, 25, 'LEÓN LOPEZ', 'MILAGROS', '../cv/CV Milagros León López.docx', '2017-08-07', '0', '0', '0', '0', NULL, NULL, NULL, 'Ps. Free Lance Chimbote', b'0', 0, '70931450', '948158301', 'America Norte', 0, '0', '0'),
(162, 13, 'LOPEZ BENITES ', 'RIGOBERTO EDUARDO ', '../cv/CV López Benites Rigoberto Eduardo..docx', NULL, '0', '0', '0', '0', NULL, NULL, NULL, NULL, b'1', 0, NULL, NULL, '', 0, '0', '0'),
(163, 13, 'LOPEZ BENITES ', 'RIGOBERTO EDUARDO ', '../cv/CV López Benites Rigoberto Eduardo..docx', '2017-08-07', '1', '1', '0', '0', '0000-00-00', '', '', 'Cumple con el perfil ', b'0', 2, '3898165', '942203479', 'America Norte', 0, '1', '0'),
(164, 13, ' CORDOVA FERNANDEZ ', 'WILMER ERICSON', '../cv/WILMER ERICSON CORDOVA FERNANDEZ.docx', '2017-08-07', '0', '0', '0', '0', '0000-00-00', '', '', 'Presentó una actitud inadecuada en entrevista pers', b'0', 2, '42591502', '989013892', 'America Norte', 0, '0', '0'),
(165, 12, 'DOMINGUEZ ACARO ', 'ELSA', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, '', b'1', 0, '', '', 'America Norte', 0, '0', '0'),
(166, 12, 'DOMINGUEZ ACARO', 'ELSA', '../cv/ELSA DOMINGUEZ ACARO.doc', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'Su perfil se ajusta a asistente de tienda o vended', b'0', 2, '45978010', '94-3923934', 'America Norte', 0, '0', '0'),
(167, 12, 'PASTOR RODRIGUEZ ', 'PAMELA GERALDINE', '../cv/PAMELA GERALDINE PASTOR RODRIGUEZ.docx', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'Su perfil se ajusta a asistente de tienda o vended', b'0', 2, '72409862', '956399902', 'America Norte', 0, '0', '0'),
(168, 12, 'SALDAÑA RODRIGUEZ ', 'EVELYN', '../cv/Evelyn Saldaña Rodríguez.docx', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'Su perfil se ajusta a administradora / oficina', b'0', 2, '41025358', '991942445', 'America Norte', 0, '0', '0'),
(169, 10, ' Olaya Santos', 'Diago Bruno', '../cv/CV  Olaya Santos Diago Bruno.pdf', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 0, '46999752', '073-410935 / 9606947', 'America Norte', 0, '0', '0'),
(170, 10, 'SULLON SEFERINO ', 'RONALD ALEXANDER', '../cv/CV  Sullon Seferino Ronald Alexander.pdf', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 0, '40934180', '995981432 / 94367601', 'America Norte', 0, '0', '0'),
(171, 24, ' Ortega Rojas', 'Victor Gabriel', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 0, '70753732', '960180430', 'America Norte', 0, '0', '0'),
(172, 24, ' GARCIA HUAROC', 'NATALIE KAREN', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 0, '47183450', '959353288', 'America Norte', 0, '0', '0'),
(173, 24, 'RODAS MOZOMBITE ', 'MARIA ROSA YDELSA', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 0, '46143660', '991958353', 'America Norte', 0, '0', '0'),
(174, 24, 'BUSTAMANTE ARCE', ' KATHLEEN LIZABET', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 0, '46728654', '987634190', 'America Norte', 0, '0', '0'),
(175, 24, 'RABINES OLIVARES', ' MILAGROS MARYTU', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 0, '46058714', '982092021', 'America Norte', 0, '0', '0'),
(176, 17, ' Romero Romero', 'Edison', '../cv/Cv Edison Romero Romero.pdf', '0000-00-00', '1', '1', '1', '0', '0000-00-00', '', '', 'Se envió cv a CLASEM.', b'0', 2, '46140255', '921247896', 'America Norte', 0, '1', '0'),
(177, 21, 'Vergara Carmona', 'Jhanira Eloysa', '../cv/', '2017-08-07', '1', '1', '1', '1', '2017-08-21', '', '', 'documentos completos.', b'0', 1, '73086246', '043-621556/ 97623329', 'America Norte', 0, '1', '1'),
(178, 12, 'LLERENA ALFARO ', 'RUTH ADRIANA', '../cv/CV  Llerena Alfaro Ruth Adriana.pdf', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'SELECCIONADA', b'0', 0, '47217099', '959377479', 'America Norte', 0, '0', '0'),
(179, 20, 'DIAZ SOTERO', 'LUZ ESPERANZA', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'BACK UP', b'1', 0, '73067591', '936122759', 'America Norte', 0, '0', '0'),
(180, 20, 'CERNA RASCO', 'ROBERTO ', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'TRAMITANDO DOCS. VENDEDOR DE PLAYA- E/s LOS POSTES', b'0', 0, '41084601', '969353656', 'America Norte', 0, '0', '0'),
(181, 20, 'VILLANUEVA RODRIGUEZ', ' LEIDY KELLY', '../cv/', '0000-00-00', '1', '1', '0', '0', '0000-00-00', 'DOCS. COMPLETOS', '', 'VENDEDOR DE PLAYA, E/s LA UNIÓN', b'0', 1, '45196718', '939344229', 'America Norte', 0, '0', '0'),
(182, 20, 'AGREDA JIMENEZ', 'JESSY LILIBETH', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'TRAMITANDO DOCS.', b'0', 0, '45120709', '950234274', 'America Norte', 0, '0', '0'),
(183, 20, 'MELO QUEZADA', 'JOSUÉ', '../cv/', '0000-00-00', '1', '1', '1', '0', '0000-00-00', 'DOCUS.COMPLETOS', '', 'VENDEDOR DE PLAYA- E/s AMÉRICA NO', b'0', 1, '48274930', '958103854', 'America Norte', 0, '0', '0'),
(184, 20, 'VARGAS BENITES', 'JENNY ISABEL', '../cv/', '0000-00-00', '1', '1', '0', '0', '0000-00-00', 'DOCS. COMPLETOS', '', 'VENDEDOR DE PLAYA, E/s LOS POSTES', b'0', 1, '45393686', '988662692', 'America Norte', 0, '0', '0'),
(185, 20, 'ALMEYDA MÉNDEZ', 'KRISTELL ', '../cv/', '0000-00-00', '1', '1', '1', '0', '0000-00-00', 'DOCS. COMPLETOS', '', 'VENDEDOR DE PLAYA- CUBRIR ESPAÑA', b'0', 1, '70473363', '960847487', 'America Norte', 0, '1', '0'),
(186, 21, 'VILCHEZ ROBLES', 'YESENIA', '../cv/', '0000-00-00', '1', '1', '1', '0', '0000-00-00', '', '', 'FALTA CARNET DE SANIDAD', b'0', 1, '74927610', '933616160', 'America Norte', 0, '1', '0'),
(187, 20, 'CARRANZA APARICIO', 'SOL DAIANA', '../cv/', '0000-00-00', '1', '1', '1', '1', '2017-08-21', 'DOCS. COMPLETOS', '', 'VENDEDOR DE TIENDA- E/s LISTO CALIFORNIA.', b'0', 1, '73056399', '968356003', 'America Norte', 0, '1', '1'),
(188, 20, 'ALVAREZ ESPEJO', 'MARCO', '../cv/', '0000-00-00', '1', '1', '1', '0', '0000-00-00', 'DOCS. COMPLETOS', '', 'VENDEDOR DE PLAYA, E/s ESPAÑA', b'0', 1, '45789594', '936141013', 'America Norte', 0, '1', '0'),
(189, 20, 'CRUZADO ESQUIVES', 'BETTY', '../cv/', '0000-00-00', '1', '1', '1', '0', '0000-00-00', 'DOCS. COMPLETOS', '', 'VENDEDOR DE PLAYA- E/S LOS POSTES', b'0', 1, '48732450', '971998327', 'America Norte', 0, '1', '0'),
(190, 13, 'SHIMOKAWA PAREDES ', 'JORGE LUIS ', '../cv/CV Jorge Luis Shimokawa Paredes.docx', '2017-08-14', '1', '1', '1', '1', '2017-09-04', '', '001-1211', 'Auxiliar Logístico', b'0', 1, '44642481', '990453238', 'America Norte', 0, '1', '1'),
(191, 13, 'CRUZ LUNA ', 'JOEL EDWIN ', '../cv/', '2017-08-14', '1', '0', '0', '0', '0000-00-00', '', '', 'Auxiliar Logistico ', b'0', 2, '44081253', '938162892', 'America Norte', 0, '1', '0'),
(192, 8, 'SANCHEZ AVALOS', 'EDWIN MARTIN', '../cv/', '2017-07-12', '0', '0', '0', '0', NULL, NULL, NULL, 'SELECCIONADO', b'1', 1, '18229127', '985556078', 'America Norte', 0, '0', '0'),
(193, 8, 'MORAN PEREZ', 'ALEX RENZO', '../cv/', '2017-07-12', '0', '0', '0', '0', NULL, NULL, NULL, 'Postulante tentativo', b'0', 2, '18167352', '976591555-945004785', 'America Norte', 0, '0', '0'),
(194, 8, 'ARAUJO ARROYO', 'CARMEN CITA', '../cv/', '2017-07-12', '0', '0', '0', '0', NULL, NULL, NULL, 'Postulante con malas referencias laborales (hurto)', b'0', 2, '44083028', '999074390 / 99230315', 'America Norte', 0, '0', '0'),
(195, 10, 'CRUZ REVOLLEDO', ' PABLO', '../cv/Formato de Curriculum Vitae T-Soluciona (1) (33).docx', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 0, '44529370', '968404285/931982719', 'America Norte', 0, '0', '0'),
(196, 27, 'CASTRO CHUNA ', 'GEANCARLOS ALONSO', '../cv/Formato de Curriculum Vitae T-Soluciona (1) (34).docx', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'Ing. con experiencia en SST', b'0', 2, ' 4154532', '934414398 - 92916103', 'America Norte', 0, '0', '0'),
(197, 27, 'RAMOS MENDOZA', ' ZOSIMO MANUEL', '../cv/Ficha T-soluciona Zósimo Ramos (1).pdf', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'Ind. Industrial con experiencia en mantenimiento y SST', b'0', 2, '43464952', '995541808', 'America Norte', 0, '0', '0'),
(198, 27, 'GAMARRA CARPENA ', 'JHONATAN TEODORO', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'Ing. Químico con experiencia en mantenimiento y SST', b'0', 2, '42460446', '977606770', 'America Norte', 0, '0', '0'),
(199, 29, 'GOICOCHEA PAREDES ', 'ALMY MILENI', '../cv/Formato de Curriculum Vitae T-Soluciona (1) (39).docx', '0000-00-00', '1', '1', '1', '1', '2017-09-04', '4510473843', 'E001-20', 'ASESOR DE VENTAS PART TIME', b'0', 1, '70598805', '982887435', 'America Norte', 0, '1', '1'),
(200, 29, 'ACOSTA CUBAS ', 'SAMANTHA LILIANA', '../cv/Formato de Curriculum Vitae T-Soluciona (1) (38).docx', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'ASESOR DE VENTAS RETAIL', b'0', 2, '73101855', '975479853', 'America Norte', 0, '0', '0'),
(201, 29, 'RODRIGUEZ CUEVA ', 'SAMUEL ISAIAS', '../cv/CV SAMUEL RODRIGUEZ.docx', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 2, '70160489', '983594144', 'America Norte', 0, '0', '0'),
(202, 8, 'SÁNCHEZ AVALOS', 'EDWIN MARTÍN ', '../cv/CV Edwin Martín Sanchez Avalos.pdf', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'SELECCIONADO', b'0', 0, '18229127', '985556078', 'America Norte', 0, '0', '0'),
(203, 9, ' ABANTO ASIAN', 'ALDO JOEL', '../cv/CV Aldo Joel Abanto Asian.pdf', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'SELECCIONADO', b'0', 0, '41188875', '986162001', 'America Norte', 0, '0', '0'),
(204, 10, ' MARCA IBÁÑEZ', 'VÍCTOR MANUEL', '../cv/CV  Marca Ibáñez Victor Manuel.pdf', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 0, '40297451', '942325718/921824490/', 'America Norte', 0, '0', '0'),
(205, 28, 'AREVALO LAZO', ' RAUL ESAU', '../cv/fichaVitae T-Soluciona.docx', '0000-00-00', '1', '1', '1', '1', '2017-10-04', '002300', '1180 Y 46', 'SOLO SE FACTURO EL 50%\r\n001 - 1180 Y EL 50% RESTANTE FACT E001 - 46', b'0', 1, '41387957', ' 982016934', 'America Norte', 0, '1', '1'),
(243, 28, 'LLENQUE TRELLES ', 'WALTER ANTONIO', '../cv/CV Llenque Trelles Walter Antonio.pdf', '0000-00-00', '1', '1', '1', '1', '0000-00-00', '', '', 'NO SELECCIONADO', b'0', 2, '45795270', '987041889', 'America Norte', 0, '1', '0'),
(206, 28, 'CAMPOS SUPO ', 'CARLOS ENRIQUE', '../cv/fichaVitae T-Soluciona.docx', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, '', b'1', 0, '41861037', '970004381', 'America Norte', 0, '0', '0'),
(207, 20, ' BURGOS CASSANO ', 'MARCOS RAFAEL', '../cv/', '2017-09-04', '1', '1', '1', '0', '0000-00-00', 'DOCS. COMPLETOS', '', 'VENDEDOR DE PLAYA- E/S LOS POSTES', b'0', 1, NULL, NULL, '', 0, '1', '0'),
(208, 20, ' RODRIGUEZ AREVALO ', 'JANNET PAMELA', '../cv/', '2017-09-04', '1', '1', '1', '0', '0000-00-00', 'DOCS. COMPLETOS', '', 'VENDEDOR DE TIENDA- E/S LISTO CALIFORNIA ', b'0', 1, NULL, NULL, '', 0, '1', '0'),
(209, 20, 'TOUZET VALVERDE', 'KAROLINA DE FATIMA ', '../cv/', '2017-09-04', '1', '1', '1', '0', '0000-00-00', 'DOCS. COMPLETOS', '', 'VENDEDOR DE TIENDA- E/S LISTO CALIFORNIA ', b'0', 1, NULL, NULL, '', 0, '1', '0'),
(210, 20, 'LOZANO AGUILAR ', 'MARK BRYAN ', '../cv/', '2017-09-04', '1', '1', '1', '0', '0000-00-00', 'DOCS. COMPLETOS', '', 'VENDEDOR DE TIENDA- E/S ESPAÑA', b'0', 1, NULL, NULL, '', 0, '1', '0'),
(211, 20, 'ALBA HUAREZ ', 'RUTH GIOVANA', '../cv/', '2017-09-12', '0', '0', '0', '0', NULL, NULL, NULL, 'DOCUMENTOS LISTO / E/S MOCHICA ', b'0', 0, '45861490', '980133243', 'Postes', 0, '0', '0'),
(212, 20, 'REVILLA GUILLERMO ', 'DIEGO ALFREDO ', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'FALTA TRAMITAR DOCUMENTOS ', b'0', 0, '74037210', '920357633 / 92733577', 'Pacasmayo', 0, '0', '0'),
(213, 20, 'CULQUE ARCE ', 'KATHERIN GISSETH ', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'DOCUMENTOS LISTOS - MINIMARKET - E/S LARCO ', b'0', 0, '73013909', '961917418', 'America Norte', 0, '0', '0'),
(214, 20, 'TEPE GIRALDO ', 'MABEL MARION  ', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'DOCUMENTOS LISTOS - MINIMARKET ', b'0', 0, '42182406', '942744007', 'Postes', 0, '0', '0'),
(215, 20, 'AVALOS CRUZ ', 'VICTOR EDUARDO ', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'DOCUMENTOS LISTO  - E/S POSTES ', b'0', 0, '71301541', '988701114/ 044 40594', 'America Norte', 0, '0', '0'),
(216, 20, 'AVALOS CRUZ ', 'VICTOR EDUARDO ', '../cv/', NULL, '0', '0', '0', '0', NULL, NULL, NULL, NULL, b'1', 0, NULL, NULL, '', 0, '0', '0'),
(217, 20, 'AMAYA CASTILLO ', 'JEN PAUL ', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'DOCUMENTOS LISTOS - e/s POSTES - MOCHICA ', b'0', 0, '48363702', '991784692', 'America Norte', 0, '0', '0'),
(218, 20, 'JARA APOLONIO ', 'JEAN ANDERSON ', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'DOCUMENTOS LISTOS E/S MOCHE ', b'0', 0, '4754680', '938916778', 'America Norte', 0, '0', '0'),
(219, 20, 'ACARO SAUCEDO ', 'GERALD STEVE ', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'DOCUMENTOS LISTOS - RENOVAR ANTECEDENTES - E/S POSTES o MOCHICA ', b'0', 0, '70664296', '943126186 / 044 4142', 'America Norte', 0, '0', '0'),
(220, 20, 'HIMBOMA CHACON ', 'ALEJANDRA DEL ROCIO ', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'DOCUMENTOS LISTOS - E/S ', b'0', 0, '74147511', '987665488 / 04441551', 'America Norte', 0, '0', '0'),
(221, 20, 'LOZA VILLAVICENCIO ', 'MIGUEL ANGEL ', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'DOCUMENTOS LISTOS - E/S UNION ', b'0', 0, '75879220', '960181762', 'America Norte', 0, '0', '0'),
(222, 20, 'GIL TORIBIO ', 'LISBETH FIORELLA ', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'DOCUMENTOS LISTOS - E/S UNION - MINIMARKET ', b'0', 0, '72047922', '976440547', 'America Norte', 0, '0', '0'),
(223, 20, 'CHACON CERNA ', 'FRANKLIN SMITH ', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'DOCUMENTOS LISTOS E/S LARCO, LA PERLA, UNION ', b'0', 0, '48434212', '925030885 / 97025309', 'America Norte', 0, '0', '0'),
(224, 20, 'CHAVEZ LUCIANO ', 'JUAN CARLOS ', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'DOCUMENTOS POR VENCER ANT. POLICIAL Y CARNET ', b'0', 0, '40841862', '977 172561', 'America Norte', 0, '0', '0'),
(225, 20, 'DIAZ GONZALES ', 'JOSEPH RAUL ', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'DOCUMENTOS POR ENTREGAR - E/S ESPAÑA ', b'0', 0, '76990066', '961417586', 'America Norte', 0, '0', '0'),
(226, 20, 'CALDERON NUNCAQUINT ', 'LEVID ALEXANDER ', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'DOCUMENTOS POR ENTREGAR - E/S POSTES ', b'0', 0, '70547555', '947192753 / 95053341', 'America Norte', 0, '0', '0'),
(227, 20, 'MELO QUEZADA ', 'JOSUE ', '../cv/', NULL, '0', '0', '0', '0', NULL, NULL, NULL, NULL, b'1', 0, NULL, NULL, '', 0, '0', '0'),
(228, 20, 'CHACON ENCO ', 'ROBERT PAUL ', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'DOCUMENTOS SIN TRAMITAR - LOS POSTES ', b'0', 0, '72657007', '964174350 / 04432508', 'America Norte', 0, '0', '0'),
(229, 20, 'ALVARADO AGUILAR ', 'CESAR STEWARD ', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'DOCUMENTOS INCOMPLETOS - CARNET DE SANIDAD - E/S UNION ', b'0', 0, '70277798', '990890728', 'America Norte', 0, '0', '0'),
(230, 20, 'RUBIO DAVALOS ', 'MERY ETEFANI ', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'FALTA ENTREGAR DOCUMENTOS - FALTA ENTREGAR CARNET DE SANIDAD ', b'0', 0, '47087742', '926205905', 'America Norte', 0, '0', '0'),
(231, 20, 'NIQUIN TORRES ', 'ANA MARIA ', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'DOCUMENTOS POR VENCER E/S POSTES MOCHICA ', b'0', 0, '46676857', '988548336', 'America Norte', 0, '0', '0'),
(232, 20, 'LINGAN LIVIAS ', 'MIGUEL ANGEL ', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'DOCUMENTOS LISTOS E/S MOCHICA ', b'0', 0, '70450111', '955074444', 'America Norte', 0, '0', '0'),
(233, 20, 'ESCALANTE MENOR ', 'JONATHAN ALEXIS ', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'FALTA SOLO CARNET DE SANIDAD - E/S UNION ', b'0', 0, '70542138', '948219672', 'America Norte', 0, '0', '0'),
(234, 30, 'ALVAREZ BARRIENTOS ', 'RICHARD ANTONIO', '../cv/CV  Alvarez Barrientos Richard Antonio.pdf', '0017-09-07', '1', '1', '1', '1', '0000-00-00', '', '', '', b'0', 1, '09870536', '988384451', 'America Norte', 0, '1', '0'),
(235, 30, 'GARCIA VENTOCILLA', ' IVAN HERNAN', '../cv/CV  Garcia Ventocilla Ivan.pdf', '0000-00-00', '1', '1', '1', '1', '0000-00-00', '', '', '', b'0', 1, NULL, NULL, '', 0, '1', '0'),
(236, 30, 'CAUSSO RAMOS ', 'MARINA ELEN ', '../cv/CV Causso Ramos Marina Elen.pdf', '0000-00-00', '1', '1', '1', '1', '0000-00-00', '', '', '', b'0', 1, NULL, NULL, '', 0, '1', '0'),
(237, 30, 'NANCY MARLENE ', 'CUTIPA CONDORI', '../cv/CV Cutipa Condori Nancy Marlene.pdf', '0000-00-00', '1', '1', '1', '1', '0000-00-00', '', '', '', b'0', 1, NULL, NULL, '', 0, '1', '0'),
(238, 27, 'VELASCO SANCHEZ ', 'JOSE LUIS ARNALDO', '../cv/CV  Velasco Sánchez José Luis Arnaldo.pdf', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'Ing. Electrónico con experiencia en mantenimiento y SST', b'0', 2, '45048534', '074- 265355/ 9789597', 'America Norte', 0, '0', '0'),
(239, 28, 'CALLE CELI', ' OSMER EDUARDO', '../cv/CV Calle Celi Osmer Eduardo.pdf', '0000-00-00', '1', '1', '1', '1', '0000-00-00', '', '', 'NO SELECCIONADO', b'0', 2, '40635477', '945549947', 'America Norte', 0, '1', '0'),
(240, 31, 'ALVAREZ GOMEZ', ' MILAGROS LILY ', '../cv/CV Álvarez Gómez Milagros Lily.docx', '2017-09-08', '1', '1', '1', '1', '0000-00-00', '', '', '', b'0', 2, NULL, NULL, '', 0, '1', '0'),
(241, 31, 'ARMIJOS GODOS', ' ELISBETT CORINNA ', '../cv/CV Armijos Godos Elisbett Corinna.docx', '2017-09-08', '1', '1', '1', '1', '0000-00-00', '', '', '', b'0', 2, NULL, NULL, '', 0, '1', '1'),
(242, 31, 'MONTOYA MANZANARES', ' MARIANA ', '../cv/CV Montoya Manzanares Mariana.docx', '0000-00-00', '1', '1', '1', '1', '0000-00-00', '52165', 'E001 -57', ' ASISTENTE DE ESTACION MOLLENDO OC 52165 PROCESO DE SELECCION DE PERSONAL AS-ES01', b'0', 1, NULL, NULL, '', 0, '1', '1'),
(244, 34, 'VILLANUEVA MALACAS', ' KATHERINE IVONNE', '../cv/Formato de Curriculum Vitae T-Soluciona (1) (53).docx', '0000-00-00', '1', '1', '1', '1', '0000-00-00', '', '', '', b'0', 2, '46953160', '970069163', 'America Norte', 0, '1', '0'),
(245, 34, ' Zevallos Palacios', 'Yuly Emperatriz', '../cv/Formato de Curriculum Vitae T-Soluciona (16).docx', '2017-09-22', '1', '1', '1', '1', '2017-11-06', '', '', 'Seleccionada para GMO, AUN NO ENVIA ORDEN DE COMPRA SE ENVIO EMAIL A JOSE EL 27.10', b'0', 1, '72746170', '949527614', 'America Norte', 0, '1', '1'),
(246, 34, ' Navarrete Alcalde', ' Alejandra Yumiko', '../cv/Formato de Curriculum Vitae T-Soluciona (16).docx', '0000-00-00', '0', '0', '0', '0', '0000-00-00', '', '', '', b'0', 1, '75847473', '944903195', 'America Norte', 0, '0', '0'),
(247, 35, 'GUERRERO GERVACCI ', 'CHISLAYNE DANELL', '../cv/Formato de Curriculum Vitae T-Soluciona (1) Danell Guerrero.docx', '2017-09-22', '1', '1', '1', '1', '2017-10-10', '', '', 'Salario: S/. 850 más bonos por meta individual y meta de tienda. ', b'0', 1, '72078153', '936995039', 'America Norte', 0, '1', '1'),
(248, 35, 'CASTILLO POPIC ', 'ANDREI MISAEL ', '../cv/Formato de Curriculum Vitae T-Soluciona (1) Danell Guerrero.docx', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 0, '7204017', '947962254/ 922308386', 'America Norte', 0, '0', '0'),
(249, 35, 'URBINA JARA ', 'JUNIOR OMAR ', '../cv/Formato de Curriculum Vitae T-Soluciona (13).docx', '2017-09-22', '1', '1', '1', '1', '2017-11-06', '', '', 'Se envío email el 27.10 y aun no envían ordene de compra', b'0', 1, '', '941406659', 'America Norte', 0, '1', '1'),
(250, 34, 'CUADRA ORTIZ ', 'MIGUEL EDUARDO', '../cv/CV  Cuadra Ortiz Miguel Eduardo.pdf', '0000-00-00', '1', '1', '1', '1', '0000-00-00', '', '', '', b'1', 1, '71224448', '973459499-946860239', 'America Norte', 0, '1', '1'),
(251, 34, 'DELGADO MANTILLA ', 'DIEGO ALBERTO', '../cv/CV  Delgado Mantilla Diego Alberto.pdf', '0000-00-00', '1', '1', '1', '1', '0000-00-00', '', '', '', b'0', 2, '75722836', '945545181', 'America Norte', 0, '1', '0'),
(252, 34, 'CUADRA ORTIZ ', 'MIGUEL EDUARDO', '../cv/CV  Cuadra Ortiz Miguel Eduardo.pdf', '0000-00-00', '1', '1', '1', '1', '0000-00-00', '', '', '', b'0', 2, NULL, NULL, '', 0, '1', '0'),
(253, 35, 'CHINGUEL MAURICIO ', 'RODRIGO ENRIQUE ', '../cv/CV Chinguel Mauricio Rodrigo Enrique.pdf', '0000-00-00', '1', '1', '1', '1', '0000-00-00', '', '', '', b'0', 2, '75518336', '948027836', 'America Norte', 0, '1', '0'),
(254, 35, 'CHINGUEL MAURICIO ', 'RODRIGO ENRIQUE ', '../cv/CV Chinguel Mauricio Rodrigo Enrique.pdf', NULL, '0', '0', '0', '0', NULL, NULL, NULL, NULL, b'1', 0, NULL, NULL, '', 0, '0', '0'),
(255, 27, 'FUSTAMANTE CARRANZA', ' LUIS FERNANDO', '../cv/CV Fustamante Carranza  Luis Fernando.pdf', '0000-00-00', '1', '1', '1', '1', '2017-11-13', '002300', 'E001- 1180 Y 46', '50 % RESTANTE FACT 1180\r\n50 % RESTANTE FACT 46', b'0', 1, '42201849', '944476505', 'America Norte', 0, '1', '1'),
(256, 27, 'CAPUÑAY SANTISTEBAN', ' CRISTHIAN ROBINSON', '../cv/CV Fustamante Carranza  Luis Fernando.pdf', '0000-00-00', '1', '1', '1', '1', '0000-00-00', '', '', '', b'0', 2, '46552626', '962849948', 'America Norte', 0, '1', '0'),
(257, 39, 'NOLASCO SALHUANA ', 'JOE LUIS JUNIOR ', '../cv/CV Junior Nolasco Sahuana.docx', '0000-00-00', '1', '1', '1', '1', '2017-10-09', '', '', 'seleccionado', b'0', 1, '43073152', '994999019 / (01) 331', 'America Norte', 0, '1', '0'),
(258, 39, 'INFANZON MAGALLANES ', 'ANGEL YEREMI ', '../cv/CV Angel Yeremi Infanzon Magallanes.docx', '0000-00-00', '1', '1', '1', '1', '0000-00-00', '', '', 'Expectativas salariales superiores ', b'0', 1, '43650572', '966-311398 / 482-908', 'America Norte', 0, '1', '0'),
(259, 40, 'LOZANO HURTADO ', 'ZINA ESTER ', '../cv/CV Zina Lozano Hurtado.docx', '0000-00-00', '1', '1', '1', '1', '2017-10-05', '', '', '', b'0', 1, '70019658', '971372540', 'America Norte', 0, '1', '0'),
(260, 42, 'Ponce Jimenez', 'Jeraldine', '../cv/CV Jeraldine Ponce Jimenez.docx', '0000-00-00', '1', '1', '1', '1', '2017-10-02', '', '', 'seleccionada. Empezó a laborar el 02-10-2017', b'0', 1, '47177744', '948563889/988839910', 'America Norte', 0, '1', '0'),
(261, 42, 'VALVERDE RUIZ ', 'GINNA YUVIKSA ', '../cv/CV Gina Valverde Ruíz.docx', '0000-00-00', '1', '1', '1', '1', '0000-00-00', '', '', 'perfil para puestos de atención al cliente.', b'0', 1, '70147360', '953767830', 'America Norte', 0, '1', '0'),
(262, 42, 'VALVERDE RUIZ ', 'GINNA YUVIKSA ', '../cv/CV Gina Valverde Ruíz.docx', NULL, '0', '0', '0', '0', NULL, NULL, NULL, NULL, b'1', 0, NULL, NULL, '', 0, '0', '0'),
(263, 42, ' QUEVEDO SIPIRAN ', 'ANGIE STEFFANY', '../cv/CV Angie Quevedo Sipiran.docx', '0000-00-00', '1', '1', '1', '1', '0000-00-00', '', '', 'perfil para recepcionista o para puestos relacionados atención al cliente.', b'0', 1, '74444706', '-978711401', 'America Norte', 0, '1', '0'),
(264, 42, 'PUESCAS FLORES ', 'MARITZA ISABEL ', '../cv/cv Maritza Puescas Flores.docx', '0000-00-00', '1', '1', '1', '1', '0000-00-00', '', '', 'perfil para puestos de atención al cliente.', b'0', 1, '70689900', '994646635', 'America Norte', 0, '1', '0');
INSERT INTO `ternas` (`codigo`, `proceso`, `apellidos`, `nombres`, `rutaCV`, `fechaEnvio`, `rrhhCliente`, `induccion`, `entrevista`, `informe`, `fechaIngreso`, `ordenServicio`, `factura`, `observacion`, `eliminado`, `terna`, `dni`, `telefono`, `estacion`, `porcentaje`, `verLaboral`, `elegido`) VALUES
(265, 17, 'SANTANDER FLOREZ ', 'HKRISS MELBA ESTRELLA', '../cv/CV Estrella Santander Florez.docx', '0000-00-00', '1', '1', '1', '1', '0000-00-00', '', '', 'Se envió cv a CLASEM. A la espera de resultados.', b'0', 2, '44195221', '987844139', 'America Norte', 0, '1', '1'),
(266, 17, '  ZURITA CHURA ', 'JOSE RICARDO ', '../cv/CV Jose Ricardo Zurita Chura.pdf', '0000-00-00', '1', '1', '1', '1', '0000-00-00', '', '', 'Se envió cv a CLASEM. A la espera de resultados.', b'0', 2, '44283600', '937047242', 'America Norte', 0, '1', '0'),
(267, 17, 'COVARRUBIAS POMPILLA ', 'LILIETTE SHEYLA', '../cv/CV Liliett Covarrubias pompilla.pdf', '0000-00-00', '1', '1', '1', '1', '0000-00-00', '', '', 'Se envió cv a CLASEM. A la espera de resultados.', b'0', 2, '', '', 'America Norte', 0, '1', '0'),
(268, 45, 'Rodriguez', 'Domiluz', '../cv/cv Domiluz Rodriguez.doc', '0000-00-00', '1', '1', '1', '1', '2017-10-09', '', '', 'seleccionada como psicóloga free lance.', b'0', 1, '70073836', '984756076', 'America Norte', 0, '1', '0'),
(269, 45, ' Montoya Copa', 'Jorge', '../cv/CV  Jorge montoya copa.docx', '0000-00-00', '1', '1', '1', '1', '0000-00-00', '', '', 'apto como psicólogo free lance. Tiene muy buenas referencias laborales.', b'0', 1, '44622767', '999004899', 'America Norte', 0, '1', '0'),
(270, 45, 'Morales novella', 'Christ stephanie ', '../cv/cv Christ Morales.docx', '0000-00-00', '1', '1', '1', '1', '0000-00-00', '', '', 'experiencia en procesos masivos de selección.', b'0', 1, '44179130', '-997183326', 'America Norte', 0, '1', '0'),
(271, 46, 'ESPINOZA LANZA', 'BLANCA', '../cv/CV  ESPINOZA LANZA BLANCA CATHERINE.pdf', '2017-09-21', '1', '1', '1', '1', '0000-00-00', '001-1199 y E001- 35', '001-1199 y E001- 35', '001-1199 y E001- 35', b'0', 1, NULL, NULL, '', 0, '1', '0'),
(272, 46, 'MARTINEZ DEL AGUILA', 'JURGENABEL', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, '', b'1', 0, '', '', 'America Norte', 0, '0', '0'),
(273, 46, 'CABRERA RODRIGUEZ', 'CARLOS', '../cv/CV CABRERA RODRIGUEZ CARLOS EDUARDO.pdf', '2017-09-21', '1', '1', '1', '1', '0000-00-00', '001-1199 y E001- 35', '001-1199 y E001- 35', '001-1199 y E001- 35', b'0', 1, NULL, NULL, '', 0, '1', '0'),
(274, 46, 'MARTINEZ DEL AGUILA', 'JURGENABEL', '../cv/CV  Martinez Del Aguila Jurgenabel Mathias.pdf', '2017-09-21', '1', '1', '1', '1', '0000-00-00', '001-1199 y E001- 35', '001-1199 y E001- 35', '001-1199 y E001- 35', b'0', 1, NULL, NULL, '', 0, '1', '0'),
(275, 36, 'GUEVARA PAUCAR ', 'ANGEL RUDIN ', '../cv/Formato de Curriculum Vitae T-Soluciona (25).docx', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 0, '71210218', '01-2592400/991922013', 'America Norte', 0, '0', '0'),
(276, 36, 'GUEVARA PAUCAR ', 'ANGEL RUDIN ', '../cv/Formato de Curriculum Vitae T-Soluciona (25).docx', NULL, '0', '0', '0', '0', NULL, NULL, NULL, NULL, b'1', 0, NULL, NULL, '', 0, '0', '0'),
(277, 36, 'SANDOVAL TORRES ', 'EDWIN', '../cv/Formato de Curriculum Vitae  - Edwin Sandoval  Torres.docx', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 0, '42953799', '989094633', 'America Norte', 0, '0', '0'),
(278, 36, 'CASTRO TORRES ', 'JUAN MIGUEL ', '../cv/Formato de Curriculum Vitae T-Soluciona (23).docx', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'Tiene antecedentes', b'0', 0, '', '', 'America Norte', 0, '0', '0'),
(279, 38, 'ROJAS CRISTOBAL ', 'ROSA DIANA ', '../cv/Formato de Curriculum Vitae T-Soluciona (24).docx', '2017-10-11', '1', '1', '1', '1', '0000-00-00', '', '', 'Es técnica egresada', b'0', 2, '43202081', '944246015', 'America Norte', 0, '1', '0'),
(280, 38, ' Alarcon Carrera', 'Luis Humberto Juniors', '../cv/Formato de Curriculum Vitae T-Soluciona (22).docx', '0000-00-00', '1', '1', '1', '1', '0000-00-00', '', '', 'PASÓ EXAMEN MÉDICO', b'0', 2, '44600754', '955756948', 'America Norte', 0, '1', '1'),
(281, 38, ' Tueros Zavaleta', 'Manuel Augusto', '../cv/Formato de Curriculum Vitae T-Soluciona (26).docx', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, 'Antecedentes positivos', b'1', 0, '43559438', '983982472', 'America Norte', 0, '0', '0'),
(282, 47, 'SAHUARAURA CONDORI ', 'GIAN MARCO ', '../cv/CV Gian Marco Sahuaraura.docx', '2017-10-16', '1', '1', '1', '1', '0000-00-00', '', '', 'no fue seleccionado.', b'0', 2, '47151375', '-992737879', 'America Norte', 0, '1', '0'),
(288, 49, 'CORNEJO GUTIERREZ', 'HÉCTOR IVAN', '../cv/Informe Héctor  Iván Cornejo Gutiérrez.pdf', '2017-10-17', '1', '1', '1', '1', '2017-10-19', 'E001', '37', 'APTO PERO CON OBSERVACIONES', b'0', 1, '42999888', '979777431 – 01 29227', 'America Norte', 0, '1', '1'),
(283, 36, 'SILVERA GOMEZ ', 'MAC ERBIN', '../cv/Mac Silvera Curriculum Vitae T-Soluciona (1).docx', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 0, '45628968', '971804402', 'America Norte', 0, '0', '0'),
(285, 36, 'SEGURA ARREDONDO ', 'ROSA AMELIA ', '../cv/Formato de Curriculum Vitae T-Soluciona (27).docx', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 0, '42032885', '991467848', 'America Norte', 0, '0', '0'),
(284, 38, 'QUISPE ROJAS', ' ELVER RUIZ', '../cv/CV Elver.docx', '2017-10-11', '0', '0', '0', '0', NULL, NULL, NULL, 'No cuenta con profesión a fin al puesto. Es ing. de materiales.', b'0', 0, '46029133', '966421635', 'America Norte', 0, '0', '0'),
(286, 36, 'GUTIERREZ QUISPITERA ', 'RUDDY ROXANA', '../cv/CV. RUDDY ROXANA GUTIERREZ.docx', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 0, '44849692', '977160468', 'America Norte', 0, '0', '0'),
(296, 47, 'MOLERO PEREZ RUIBAL ', 'CESAR ALEXANDER ', '../cv/CV CESAR ALEXANDER MOLERO PÉREZ RUIBAL.docx', '2017-10-16', '1', '1', '1', '1', '0000-00-00', '', '', 'Ingresa el 04-12 a laborar.', b'0', 1, '71277723', '956097040', 'America Norte', 0, '1', '1'),
(287, 47, 'VALDEZ ALFARO', 'LUCERO', '../cv/CV LUCERO VALDEZ ALFARO.docx', '2017-10-16', '1', '1', '1', '1', '0000-00-00', '', '', 'Ingresa a laborar el 04-12.', b'0', 1, '45270294', '931480501', 'America Norte', 0, '1', '1'),
(289, 49, 'CORNEJO GUTIERREZ', 'HÉCTOR IVAN', '../cv/Informe Héctor  Iván Cornejo Gutiérrez.pdf', NULL, '0', '0', '0', '0', NULL, NULL, NULL, NULL, b'1', 0, NULL, NULL, '', 0, '0', '0'),
(290, 49, 'ROBLES SANTILLÁN', 'VINCENT EDWARD', '../cv/Informe Vincent Edward Robles Santillan.pdf', '2017-10-17', '1', '1', '1', '1', '2017-10-19', 'E001', '37', 'SEGÚN EVALUACIÓN ES APTO', b'0', 1, '09304697', '941 620 016 - 435667', 'America Norte', 0, '1', '1'),
(291, 36, 'SOLORZANO LIZARRAGA ', 'SABRINA ', '../cv/CV Solorzano Lizarraga Sabrina.docx', NULL, '0', '0', '0', '0', NULL, NULL, NULL, NULL, b'0', 0, NULL, NULL, '', 0, '0', '0'),
(292, 36, 'SOLORZANO LIZARRAGA ', 'SABRINA ', '../cv/CV Solorzano Lizarraga Sabrina.docx', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 0, '42294489', '993225159', 'America Norte', 0, '0', '0'),
(293, 36, 'ROJAS CANORIO ', 'LIZBETH NOHELY', '../cv/CV  Rojas Canorio Lizbeth Nohely.docx', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 0, '44656056', '987971940', 'America Norte', 0, '0', '0'),
(294, 36, 'VICTORIO ROQUE ', 'MEIBY JACKELINE', '../cv/CV  Victorio Roque Meiby Jackeline.docx', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 0, '43124514', '955326919', 'America Norte', 0, '0', '0'),
(295, 36, 'HUAMAN SANCHEZ ', 'JUAN JOSE ', '../cv/Formato de Curriculum Vitae T-Soluciona[65].docx', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 0, '44814405', '994568492', 'America Norte', 0, '0', '0'),
(297, 38, 'VILCA HUAMANTUMBA ', 'CARLOS ALFREDO', '../cv/CV  Vilca Huamantumba Carlos Alfredo.pdf', '2017-10-11', '1', '1', '1', '1', '0000-00-00', '', '', '', b'0', 1, '70396645', '70396645', 'America Norte', 0, '1', '0'),
(298, 55, 'GUTIERREZ QUISPITERA', ' RUDDY ROXANA', '../cv/CV Gutierrez Quispitera Ruddy Roxana.docx', '2017-10-10', '1', '1', '1', '1', '0000-00-00', '', '', 'No pasó entrevista con Primax, puede ser considerada para otros puestos administrativos', b'0', 2, NULL, NULL, '', 0, '1', '0'),
(299, 55, 'SOLORZANO LIZARRAGA', ' SABRINA ', '../cv/CV Solorzano Lizarraga Sabrina.docx', '2017-10-10', '1', '1', '1', '1', '0000-00-00', '', '', 'Pasó examen médico', b'0', 1, NULL, NULL, '', 0, '1', '1'),
(300, 55, 'GUEVARA PAUCAR ', 'ANGEL RUDIN ', '../cv/CV  Guevara Paucar Angel Rudin.docx', '2017-10-10', '1', '1', '1', '1', '0000-00-00', '', '', 'Trabajo anteriormente en Primax ( 5 días). Puede considerarse para otros puestos administrativos', b'0', 2, NULL, NULL, '', 0, '1', '0'),
(301, 52, 'VELA CHAVEZ ', 'RODOLFO ', '../cv/CV RODOLFO VELA CHAVEZ.pdf', '2017-10-23', '1', '1', '1', '1', '0000-00-00', '', '', 'entrevista final con PRIMAX el 14-11', b'0', 2, '44414692', '945545690', 'America Norte', 0, '1', '0'),
(302, 52, 'VELA CANELO ', 'OSCAR ALBERTO', '../cv/CV OSCAR ALBERTO VELA CANELO.pdf', '2017-10-23', '1', '1', '1', '1', '0000-00-00', '', '', 'entrevista final con PRIMAX el 14-11', b'0', 2, '42400331', '956904997', 'America Norte', 0, '1', '0'),
(303, 52, 'PANDURO GOMEZ ', 'NELIDA MILUSKA', '../cv/CV NELIDA MILUSKA PANDURO GOMEZ.pdf', '2017-10-23', '1', '1', '1', '1', '0000-00-00', '', '', 'PASO EXAMEN MEDICO EN IQUITOS', b'0', 1, '', '959577860', 'America Norte', 0, '1', '0'),
(347, 60, 'RAMIREZ DIAZ ', 'MILAGROS CAROLINA', '../cv/CV MILAGROS CAROLINA RAMIREZ DIAZ.docx', NULL, '0', '0', '0', '0', NULL, NULL, NULL, NULL, b'1', 0, NULL, NULL, '', 0, '0', '0'),
(304, 53, 'SANDOVAL TORRES ', 'EDWIN', '../cv/CV Sandoval Torres Edwin.docx', '0000-00-00', '1', '1', '1', '1', '0000-00-00', '', '', 'Pasó entrevista, no fue seleccionado para Primax. Puede ser considerado para puestos administrativos', b'0', 2, NULL, NULL, '', 0, '1', '0'),
(305, 53, 'ROJAS CANORIO ', 'LIZBETH NOHELY', '../cv/CV  Rojas Canorio Lizbeth Nohely.docx', '0000-00-00', '1', '1', '1', '1', '0000-00-00', '', '', 'Pasó entrevista, no fue seleccionado para Primax. Puede ser considerado para puestos administrativos', b'0', 2, NULL, NULL, '', 0, '1', '0'),
(306, 53, 'VICTORIO ROQUE ', 'MEIBY JACKELINE', '../cv/CV  Victorio Roque Meiby Jackeline.pdf', '0000-00-00', '1', '1', '1', '1', '0000-00-00', '', '', 'Pasó entrevista, no fue seleccionado para Primax. Puede ser considerado para puestos administrativos', b'0', 2, NULL, NULL, '', 0, '1', '0'),
(307, 56, 'Medina Villar', 'Ramiro', '../cv/CV-Ramiro Medina.docx', '2017-11-09', '1', '1', '1', '1', '0000-00-00', '', '', 'Referencias empresa', b'0', 1, '45024532', '995420646', 'America Norte', 0, '1', '0'),
(308, 51, 'RODRIGUEZ REVILLA ', 'CESAR AUGUSTO', '../cv/CV  Rodríguez Revilla César Augusto.pdf', '0000-00-00', '1', '1', '1', '1', '0000-00-00', '', '', '', b'0', 2, NULL, NULL, '', 0, '1', '0'),
(309, 51, 'TORIBIO ZURITA ', 'CARLOS ALBERTO', '../cv/CV  Rodríguez Revilla César Augusto.pdf', '0000-00-00', '1', '1', '1', '1', '0000-00-00', '', '', '', b'0', 2, NULL, NULL, '', 0, '1', '0'),
(310, 51, 'CALVANAPON ESPINOLA', ' PAOLA MILAGROS ', '../cv/CV Calvanapon Espinola Paola Milagros.pdf', '2017-10-25', '1', '1', '1', '1', '0000-00-00', '', 'E001-65', 'Seleccionada, se envío a karen para facturación 19.11\r\nSe procedió con la facturación el día 20/11', b'0', 1, NULL, NULL, '', 0, '1', '1'),
(311, 51, 'FLORES BAZALAR ', 'MARIELLA ALEXANDRA', '../cv/CV Flores Bazalar Mariella Alexandra.pdf', '0000-00-00', '1', '1', '1', '1', '0000-00-00', '', '', '', b'0', 2, NULL, NULL, '', 0, '1', '0'),
(312, 48, 'SOSA GUAYGUA ', 'CESAR AUGUSTO ', '../cv/CV Sosa Guaygua Cesar Agusto.pdf', '2017-10-18', '1', '1', '1', '1', '0000-00-00', '', '', 'Aún no pasan entrevista con Gascop', b'0', 1, '46266419', '955750996', 'America Norte', 0, '1', '0'),
(313, 48, 'SANTOS MOGOLLON ', 'LEO LUIS', '../cv/CV Santos Mogollón Leo Luis.pdf', '2017-10-18', '1', '1', '1', '1', '0000-00-00', '', '', 'Aún no pasan entrevista con Gascop', b'0', 1, '76670016', '977849466', 'America Norte', 0, '1', '0'),
(314, 48, 'RIVAS ACURIO ', 'CARLOS AUGUSTO ', '../cv/', '2017-10-18', '0', '0', '0', '0', NULL, NULL, NULL, '´Tiene antecedentes', b'0', 0, '41902550', '969079641', 'America Norte', 0, '0', '0'),
(315, 48, 'CAMPOS SUPO ', 'CARLOS ENRIQUE ', '../cv/Formato de Curriculum Vitae T-Soluciona (37).docx', '2017-10-18', '1', '1', '1', '1', '0000-00-00', '', '', 'Aún no pasan entrevista con Gascop', b'0', 1, '', '', 'America Norte', 0, '1', '0'),
(316, 56, 'La Rosa Chumpitaz', 'Jose Carlos', '../cv/CV-JOSE CARLOS LA ROSA CHUMPITAZI.docx', '2017-11-06', '1', '1', '1', '1', '0000-00-00', '', '', 'Referencias', b'0', 1, '70435616', '991919262', 'America Norte', 0, '1', '0'),
(317, 56, 'Marcapiña Avendaño', 'Monica', '../cv/CV - MONICA MARCAPIÑA AVENDAÑO.docx', '2017-11-07', '0', '0', '0', '0', NULL, NULL, NULL, 'Carta de no adeudo', b'0', 0, '46739604', '999464782', 'America Norte', 0, '0', '0'),
(318, 53, 'GONZALES ARTEAGA ', 'YUSSIARA YANNET', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 0, '45113672', '953567132', 'America Norte', 0, '0', '0'),
(319, 53, 'CABRERA AQUIJE', ' KAREN PATRICIA', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 0, '45207580', '-961864758', 'America Norte', 0, '0', '0'),
(320, 53, 'TELLO HENRIQUEZ ', 'MARCIAL RICARDO', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 0, '41408936', '994-358-226 ', 'America Norte', 0, '0', '0'),
(321, 53, 'ATUNCAR GAMBOA', ' MIGUEL ANGEL', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 0, '42934018', '982770764', 'America Norte', 0, '0', '0'),
(322, 53, 'SANTA CRUZ VALLADARES ', 'DIANA VANESSA ', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 0, '41367243', '051-5434710 /  99166', 'America Norte', 0, '0', '0'),
(323, 53, 'BAUTISTA OLORTEGUI ', 'JONATAN ', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 0, '43105042', '-941489128', 'America Norte', 0, '0', '0'),
(324, 53, 'QUIJAITE MENDOZA ', 'PABLO JESUS', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 0, '43694420', '01-5237747/964133218', 'America Norte', 0, '0', '0'),
(325, 53, 'PAUCAR CHAPPA ', 'CARLOS AUGUSTO', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 0, '45329685', '949730075', 'America Norte', 0, '0', '0'),
(326, 53, 'MONTEVERDE PINEDO ', 'LUIGI GIANCARLO ', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 0, '45867123', '983418673-983418673', 'America Norte', 0, '0', '0'),
(327, 53, 'CHINCHAY MELENDEZ', 'ALEX JAIR ', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 0, '44858730', '', 'America Norte', 0, '0', '0'),
(328, 53, 'Blanco Edquen', 'Udilberto ', '../cv/', '0000-00-00', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 0, '47677968', '', 'America Norte', 0, '0', '0'),
(329, 53, 'JACOME PAZ ANTONIO RENATO', 'Udilberto ', '../cv/', NULL, '0', '0', '0', '0', NULL, NULL, NULL, NULL, b'0', 0, NULL, NULL, '', 0, '0', '0'),
(330, 38, ' ALARCON CARRERA', 'LUIS HUMBERTO JUNIORS', '../cv/CV Alarcon Carrera  Luis Humberto Juniors.docx', '2017-10-11', '1', '1', '1', '1', '0000-00-00', '', '', 'Se descubrió que tienes estudios superiores inconclusos.', b'0', 2, '44600754', '955756948', 'America Norte', 0, '1', '0'),
(331, 48, 'MONTALBAN CARRASCO ', 'YOEL EDMUNDO', '../cv/CV Montalvan Carrasco Yoel Edmundo.pdf', '2017-10-18', '1', '1', '1', '1', '0000-00-00', '', '', 'Aún no pasan entrevista con Gascop', b'0', 1, '', '', 'America Norte', 0, '1', '0'),
(332, 48, 'AGUILAR HIDALGO ', 'RICARDO MANUEL', '../cv/CV Aguilar Hidalgo Ricardo Manuel .pdf', '2017-10-18', '1', '1', '1', '1', '0000-00-00', '', '', 'Aún no pasan entrevista con Gascop', b'0', 1, '', '', 'America Norte', 0, '1', '0'),
(333, 58, 'GONZALES ARTEAGA', ' YUSSIARA YANNET', '../cv/CV Gonzáles Arteaga Yussiara Yannet.pdf', '2017-10-31', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 1, '45113672', '953567132', 'America Norte', 0, '0', '0'),
(334, 58, 'CABRERA AQUIJE ', 'KAREN PATRICIA', '../cv/CV Cabrera Aquije Karen Patricia.pdf', '2017-10-31', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 1, '45207580', '961864758', 'America Norte', 0, '0', '0'),
(335, 58, 'TELLO HENRIQUEZ ', 'MARCIAL RICARDO', '../cv/CV Tello Henriquez  Marcial Ricardo.pdf', '2017-10-31', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 1, '41408936', '994-358-226 ', 'America Norte', 0, '0', '0'),
(336, 58, 'ATUNCAR GAMBOA ', 'MIGUEL ANGEL', '../cv/CV Atuncar Gamboa Miguel Angel.pdf', '2017-10-31', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 1, '42934018', '982770764', 'America Norte', 0, '0', '0'),
(337, 58, 'SANTA CRUZ VALLADARES ', 'DIANA VANESSA ', '../cv/CV  Santa Cruz Valladares Diana Vanessa.pdf', '2017-10-31', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 1, '41367243', ' 991661231  ', 'America Norte', 0, '0', '0'),
(338, 58, '', '', '../cv/', NULL, '0', '0', '0', '0', NULL, NULL, NULL, NULL, b'1', 0, NULL, NULL, '', 0, '0', '0'),
(339, 58, 'BAUTISTA OLORTEGUI ', 'JONATAN ', '../cv/CV Bautista Olortegui Jonatan.pdf', '2017-10-31', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 1, '43105042', '941489128', 'America Norte', 0, '0', '0'),
(340, 58, 'QUIJAITE MENDOZA ', 'PABLO JESUS ', '../cv/CV Quijaite Mendoza  Pablo.pdf', '2017-10-31', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 1, '43694420', '964133218', 'America Norte', 0, '0', '0'),
(341, 58, 'PAUCAR CHAPPA ', 'CARLOS AUGUSTO', '../cv/CV  Paucar Chappa Carlos Augusto.pdf', '2017-10-31', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 1, '45329685', '949730075', 'America Norte', 0, '0', '0'),
(342, 58, ' CHINCHAY MELENDEZ', 'ALEX JAIR', '../cv/CV Chinchay Melendez Alex Jair.pdf', '2017-10-31', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 1, '44858730', '', 'America Norte', 0, '0', '0'),
(343, 58, ' Blanco Edquen', 'Udilberto', '../cv/CV Blanco Edquen Ubilberto.pdf', '2017-10-31', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 1, '47677968', '', 'America Norte', 0, '0', '0'),
(344, 58, 'JACOME PAZ', ' ANTONIO RENATO', '../cv/CV Jacome Paz Renato Antonio.pdf', '2017-10-31', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 1, '44927228', '', 'America Norte', 0, '0', '0'),
(345, 58, 'MANTILLA ARRUNATEGUI', ' MARTIN EDUARDO', '../cv/CV  Mantilla Arrunategui Martin Eduardo.pdf', '2017-10-31', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 1, '41377490', '', 'America Norte', 0, '0', '0'),
(346, 59, ' Olaechea', 'Maria Melissa', '../cv/Formato de Curriculum Vitae T-Soluciona (1) (57).docx', '2017-11-21', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 0, '41299919', '·      956-765856', 'America Norte', 0, '0', '0'),
(348, 60, 'RAMIREZ DIAZ ', 'MILAGROS CAROLINA', '../cv/CV MILAGROS CAROLINA RAMIREZ DIAZ.docx', '2017-11-17', '0', '0', '0', '0', NULL, NULL, NULL, 'pendiente entrevista presencial y verificaciones ', b'0', 0, '44613482', '985645449', '', 0, '0', '0'),
(349, 59, 'CABRERA CALDERON ', 'OSCAR MAXIMILIANO', '../cv/Formato de Curriculum Vitae T-Soluciona (49).docx', '2017-11-21', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 0, '42590524', '948195604', '', 0, '0', '0'),
(350, 59, ' Huallanca Masso', 'Maria Ines', '../cv/Formato de Curriculum Vitae T-Soluciona (49).docx', '2017-11-21', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 0, '42299490', '930188060', '', 0, '0', '0'),
(351, 38, 'MELENDEZ CANCHO ', 'PASCUAL CHRISTIAN', '../cv/Formato de Curriculum Vitae T-Soluciona (51).docx', '2017-11-17', '0', '0', '0', '0', NULL, NULL, NULL, '', b'0', 0, '44284047', '953292686', '', 0, '0', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `usuario` varchar(45) NOT NULL,
  `clave` varchar(45) NOT NULL,
  `admin` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`usuario`, `clave`, `admin`) VALUES
('admi', 'waacxmmg', 1),
('clientePrimax', '123', 0),
('gerente', 'rperez', 0),
('seleccion', '123', 0),
('vanessa', 'vruiz', 0),
('alessandra', 'avelasco', 0),
('marita', 'myglesias', 0),
('gianella', 'ghilario', 0),
('carmen', 'cpalacios', 0),
('joe', 'jnolasco', 0),
('karen', '123', 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`ID_Categoria`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`ruc`);

--
-- Indices de la tabla `datospersonales`
--
ALTER TABLE `datospersonales`
  ADD PRIMARY KEY (`usuario`);

--
-- Indices de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `grifos`
--
ALTER TABLE `grifos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idDepartamento` (`idDepartamento`);

--
-- Indices de la tabla `perfiles`
--
ALTER TABLE `perfiles`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `empresa` (`empresa`);

--
-- Indices de la tabla `permisos`
--
ALTER TABLE `permisos`
  ADD PRIMARY KEY (`usuario`,`ID_Categoria`),
  ADD KEY `ID_Categoria` (`ID_Categoria`);

--
-- Indices de la tabla `procesos`
--
ALTER TABLE `procesos`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `perfil` (`perfil`);

--
-- Indices de la tabla `ternas`
--
ALTER TABLE `ternas`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `proceso` (`proceso`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `ID_Categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `grifos`
--
ALTER TABLE `grifos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=172;
--
-- AUTO_INCREMENT de la tabla `perfiles`
--
ALTER TABLE `perfiles`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT de la tabla `procesos`
--
ALTER TABLE `procesos`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT de la tabla `ternas`
--
ALTER TABLE `ternas`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=352;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
