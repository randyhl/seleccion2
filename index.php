<!DOCTYPE html>
<html>


<!-- Mirrored from webapplayers.com/inspinia_admin-v2.7.1/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Sep 2017 15:12:14 GMT -->
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>T-SELECCIÓN | Login</title>
    <link rel="shortcut icon" type="image/x-icon" href="images/logo.ico" />
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>

                <h1 class="logo-name">T-S</h1>

            </div>
            <h3>T-SOLUCIONA|SELECCIÓN</h3>
            <p>Accede con tu cuenta de usuario</p>
            <form class="form-signin" action="controlador/iniciar-sesion.php" method="POST">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Usuario" required="" name="usuario">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Contraseña" required="" name="clave">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b"><strong>Login</strong></button>

                <a href="#"><small>Forgot password?</small></a>
            </form>
            <p class="m-t"> <small> <a href="#" title="Randy Huaccha 950980044">Área de Sistemas</a> &copy; 2018</small> </p>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

</body>


<!-- Mirrored from webapplayers.com/inspinia_admin-v2.7.1/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Sep 2017 15:12:14 GMT -->
</html>
