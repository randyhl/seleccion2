<?php  
	require 'funciones.php';

    $codigo         = $_GET['id'];
	$cargo          = $_POST['cargo'];
    $empresa        = $_POST['selectEmpresa'];
    $gradoInstruccion = $_POST['selectGradoInstruccion'];
    $carrera        = $_POST['carrera'];
    $estadoCarrera  = $_POST['selectEstadoCarrera'];
    $diplomado      = $_POST['diplomado'];
    $idioma         = $_POST['idiomas'];
    $nivelIdioma    = $_POST['selectNivelIdioma'];
    $postGrado      = $_POST['postGrado'];
    $programas      = $_POST['programas'];
    $nivelPrograma  = $_POST['selectNivelProgramas'];
    $sexo           = $_POST['selectSexo'];
    $experiencia    = $_POST['experiencia'];
    $tiempoExp      = $_POST['tiempoExperiencia'];
    $edadMinima     = $_POST['edadMinima'];
    $sentinel       = $_POST['sentinel'];
    $viaje          = $_POST['selectViajar'];
    $edadMaxima     = $_POST['edadMaxima'];
    $funciones      = $_POST['funciones'];
    $detalleRem     = $_POST['detalleRemuneracion'];
    $horaInicio     = $_POST['horaInicio'];
    $horaFin        = $_POST['horaFin'];
    $remuneracion   = $_POST['remuneracion'];
    $planilla       = $_POST['selectPlanilla'];
    $otros          = $_POST['otros'];


    
    $perfil = ejecutarQuery("UPDATE perfiles SET
                                    empresa             = '$empresa',
                                    cargo               = '$cargo',
                                    gradoInstruccion    = '$gradoInstruccion',
                                    estadoCarrera       = '$estadoCarrera',
                                    carrera             = '$carrera',
                                    diplomados          = '$diplomado',
                                    postgrado           = '$postGrado',
                                    idioma              = '$idioma',
                                    estadoIdioma        = '$nivelIdioma',
                                    programasInformaticos = '$programas',
                                    nivelProgramas        = '$nivelPrograma',
                                    experienciaRequerida  = '$experiencia',
                                    tiempoExperiencia     = '$tiempoExp',
                                    otrasCaracteristicas  = '$otros',
                                    funciones           = '$funciones',
                                    sexo                = '$sexo',
                                    edadMin             = '$edadMinima',
                                    edadMax             = '$edadMaxima',
                                    direccion           = '$sentinel',
                                    horarioInicio       = '$horaInicio',
                                    horarioFin          = '$horaFin',
                                    remuneracion        = '$remuneracion',
                                    planilla            = '$planilla',
                                    remuneracionDetalle = '$detalleRem',
                                    viajar              = '$viaje'
                                    WHERE codigo = '$codigo'
                            ");
?>

<script>
    alert("Perfil editado exitosamente");
    window.history.go(-1); 
</script>