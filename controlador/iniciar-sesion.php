<?php
include ('funciones.php');

// Pasados por formulario:
    $usuario = $_POST['usuario'];
    $clave = $_POST['clave'];
//conectamos a la bd    
    conectar();
//usamos funciones:
    if( validarLogin($usuario, $clave) ) {
        // Accedemos al sistema
            if( esAdmin() )
                header('Location: ../vistas/admin.php');
            else header('Location: ../vistas/home.php');
    } else {
        // Sino volvemos al formulario inicial
    ?>
        <script>
            alert('Los datos ingresados son incorrectos.')
            location.href = "../index.php";
        </script>
<?php
        desconectar();
    }
?>